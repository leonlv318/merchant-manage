webpackJsonp([10],{

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_555e0c7f_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_editGoodsOfAdd_vue__ = __webpack_require__(475);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(473)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-555e0c7f"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_editGoodsOfAdd_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_555e0c7f_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_editGoodsOfAdd_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\editGoodsOfAdd.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-555e0c7f", Component.options)
  } else {
    hotAPI.reload("data-v-555e0c7f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(95);

var _stringify2 = _interopRequireDefault(_stringify);

var _assign = __webpack_require__(96);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    },
    changeIsUp1: function changeIsUp1(value) {
      if (value == 1) {
        return "显示";
      } else {
        return "不显示";
      }
    }
  },
  data: function data() {
    return {
      channelGoodsId: "",
      saveFlag: false,
      val1: "",
      options1: [{ value: 0, label: "不显示" }, { value: 1, label: "显示" }],
      total: 0,
      queryOfContent: {
        pageNum: 1,
        pageSize: 20,
        channelGoodsId: "",
        isUp: ""
      },
      tableDataOfContent: [],
      showDialogOfAdd: false,
      formOfAdd: {
        channelGoodsId: "",
        content: ""
      },
      showDialogOfEdit: false,
      formOfEdit: {
        channelGoodsDocumentId: "",
        documentContent: "",
        sort: "",
        isUp: true
      },
      endTime: "",
      rowIndex: 0,
      setAllFlag: false,
      cl: 1, //等级数
      levelNames: [],
      commissionLevel: [],
      showDialog: false,
      goodsData: [],
      query: {
        pageNum: 1,
        pageSize: 10
      },
      attrName: [],
      tableData: [],
      ruleForm: {
        v1: "",
        v2: "",
        v3: "",
        v4: "",
        v5: "",
        v6: "",
        v7: "",
        v8: ""
      },
      FormDialog: { commissionRate: 0 },
      FormDialogRow: {},
      rules: {}
    };
  },

  methods: {
    /**@function 获取商品详情 */
    getGoodsDetail: function getGoodsDetail() {
      var _this = this;

      var channelGoodsId = this.$route.params.channelGoodsId;
      this.channelGoodsId = channelGoodsId;
      this.$http.get(this.$api.findChannelGoods + ("?channelGoodsId=" + channelGoodsId)).then(function (rsp) {
        console.log("获取商品详情");
        console.log(rsp.data);
        _this.goodsData = rsp.data.data;
        _this.attrName = rsp.data.data.goodsAttributeDtos;
        console.log("this.tableData========");
        console.log(_this.tableData);
        _this.tableData = rsp.data.data.channelGoodsSkuDetailDtos;
        _this.getContentData();
        _this.goodsId = rsp.data.data.goodsId;
      });
    },

    /**@function 获取佣金等级 */
    getCommissionLevel: function getCommissionLevel() {
      var _this2 = this;

      this.$http.get(this.$api.findChannelCommissionRateConfList).then(function (rsp) {
        console.log("获取佣金等级");
        console.log(rsp.data);
        _this2.commissionLevel = rsp.data.data;
        _this2.cl = _this2.levelNames.length;
      });
    },

    /**@function 保存 */
    save: function save() {
      var self = this;
      console.log(this.endTime);
      this.$confirm("确认保存后该商品的推荐信息需要重新审核，请确认是否修改？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        console.log("保存");
        var formData = {};

        formData.channelGoodsId = self.$route.params.channelGoodsId;
        // formData.goodsId = self.goodsData.goodsId;

        if (self.endTime) {
          formData.endUseTime = self.endTime.substring(0, 10) + " 23:59:59";
        } else {
          formData.endUseTime = "";
        }

        formData.commissionRate = self.ruleForm.v1;
        formData.channelGoodsSkuAddParams = [];
        formData.channelGoodsSkuAddParams.channelGoodsCommissionParams = [];
        console.log(self.tableData);
        for (var i in self.tableData) {
          var obj1 = {};
          obj1.goodsSkuId = self.tableData[i].goodsSkuId;
          var obj2 = {};
          obj2.commissionRate = self.tableData[i].commissionRate;
          formData.channelGoodsSkuAddParams[i] = (0, _assign2.default)(obj1, obj2);
        }
        console.log("formData");
        console.log(formData);

        // formData.channelGoodsCommissionParams = [];

        // let goodsSkuIds = [];
        // this.goodsData.channelGoodsDetailDto.channelGoodsSkuDetailDtos.forEach(
        //   (gv, gi) => {
        //     goodsSkuIds.push(gv.goodsSkuId);
        //   }
        // );
        // let channelGoodsSkuAddParams = []; //

        // formData.channelGoodsSkuAddParams = channelGoodsSkuAddParams;

        // console.log("formData====================>>");
        // let formDataToJson = JSON.stringify(formData);
        // console.log(formDataToJson);

        self.$http.post(self.$api.updateChannelGoods, formData).then(function (rsp) {
          if (rsp.data.flag) {
            console.log(rsp.data);
            self.$message.success("保存成功！");
            self.$router.go(-1);
          } else {
            self.$message.error(rsp.data.msg);
          }
        }).catch(function (err) {
          console.log("err-------------------");
          console.log(err);
          self.$message.error(err);
          self.addFlag = false;
        });
      }).catch(function () {
        self.$message({
          type: "info",
          message: "已取消 [保存] 操作"
        });
      });
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    },
    qqq: function qqq() {
      console.log(this.FormDialog);
      console.log(this.tableData);
    },

    // 统一设置
    setAll: function setAll() {
      if (this.ruleForm.v1) {
        console.log("统一设置佣金比例");
        this.setAllFlag = true;
        var tableData = this.tableData;
        for (var i in tableData) {
          tableData[i].commissionRate = this.ruleForm.v1 * 100;
        }
        this.FormDialog.commissionRate = this.ruleForm.v1;
      }
    },

    /**@function 编辑佣金*/
    edit: function edit(row, $index) {
      console.log("this.tableData   ----------- edit row");
      console.log(this.tableData);
      console.log("$index========" + $index);
      if (this.setAllFlag) {
        this.showDialog = true;
        this.rowIndex = $index;
        this.FormDialogRow = row;
        // this.FormDialog = row.commissionLevels;
        console.log(this.FormDialogRow);
        console.log(this.FormDialog);
      } else {
        this.$message.error("请在上一步设置佣金比例后再执行编辑佣金操作！");
      }
    },


    /**@function 取消保存*/
    cancelDialog: function cancelDialog() {
      this.showDialog = false;
    },

    //===================文案部分开始==========================

    /**@function 获取文案列表 */
    getContentData: function getContentData() {
      var _this3 = this;

      console.log("获取文案列表");
      this.queryOfContent.channelGoodsId = this.channelGoodsId;
      this.$http.get(this.$api.findChannelGoodsDocumentListPage, {
        params: this.queryOfContent
      }).then(function (rsp) {
        if (rsp.data.flag && rsp.data.data) {
          _this3.tableDataOfContent = rsp.data.data.list;
          _this3.total = rsp.data.data.total;
        } else {
          _this3.$message.error(rsp.data.msg);
          _this3.tableData = [];
          _this3.total = 0;
        }
      });
    },

    /**@function 新增文案 */
    addContent: function addContent() {
      console.log("新增文案");
      this.showDialogOfAdd = true;
    },

    /**@function 修改文案排序号 */
    editSort: function editSort(row) {
      var _this4 = this;

      console.log("修改文案排序号");
      var params = {};
      params.channelGoodsDocumentId = row.channelGoodsDocumentId;
      params.sort = row.sort;
      this.$http.get(this.$api.updateChannelGoodsDocumentSort, {
        params: params
      }).then(function (rsp) {
        if (rsp.data.flag) {
          _this4.getContentData();
          _this4.$message({
            type: "success",
            message: "修改排序号成功!"
          });
        } else {
          _this4.$message({
            type: "error",
            message: rsp.data.msg
          });
        }
      });
    },
    saveDialog: function saveDialog() {
      console.log("保存");
      this.tableData[this.rowIndex].commissionRate = this.FormDialog.commissionRate * 100;
      this.showDialog = false;
      console.log("tableData:");
      console.log(this.tableData);
    },

    /**@function 选择显示状态 */
    selectVal1: function selectVal1() {
      console.log("选择显示状态");
      this.queryOfContent.isUp = this.val1;
      this.getContentData();
    },

    /**@function 修改文案按钮 */
    change: function change(row, i) {
      console.log("修改文案按钮");
      this.showDialogOfEdit = true;
      var tempForm = row;
      if (tempForm.isUp == 1) {
        tempForm.isUp = true;
      } else {
        tempForm.isUp = false;
      }
      this.formOfEdit = tempForm;
      console.log(this.formOfEdit);
    },

    /**@function 新增文案_保存按钮 */
    saveDialogOfAdd: function saveDialogOfAdd() {
      var _this5 = this;

      console.log("新增文案_保存按钮");
      this.formOfAdd.channelGoodsId = this.channelGoodsId;
      console.log(this.formOfAdd);
      var formDataToJson = (0, _stringify2.default)(this.formOfAdd);
      this.$http.post(this.$api.addChannelGoodsDocument, formDataToJson).then(function (rsp) {
        _this5.showDialogOfAdd = false;
        if (rsp.data.flag) {
          _this5.$message.success("新增成功！");
          _this5.getContentData();
        } else {
          _this5.$message.error(rsp.data.msg);
        }
      });
    },

    /**@function 新增文案_取消按钮 */
    cancelDialogOfAdd: function cancelDialogOfAdd() {
      this.showDialogOfAdd = false;
    },

    /**@function 修改文案_保存按钮 */
    saveDialogOfEdit: function saveDialogOfEdit() {
      var _this6 = this;

      console.log("修改文案_保存按钮");
      var formData = this.formOfEdit;
      if (formData.isUp == true) {
        formData.isUp = formData.isUp = 1;
      } else {
        formData.isUp = formData.isUp = 0;
      }
      formData.content = formData.documentContent;
      delete formData.documentContent;
      var formDataToJson = (0, _stringify2.default)(formData);

      console.log(formDataToJson);
      this.$http.post(this.$api.updateChannelGoodsDocument, formDataToJson).then(function (rsp) {
        _this6.showDialogOfEdit = false;
        if (rsp.data.flag) {
          _this6.$message.success("修改成功！");
          _this6.getContentData();
        } else {
          _this6.$message.error(rsp.data.msg);
        }
      });
    },

    /**@function 修改文案_删除按钮 */
    delDialogOfEdit: function delDialogOfEdit() {
      console.log("修改文案_删除按钮");
    },

    /**@function 修改文案_取消按钮 */
    cancelDialogOfEdit: function cancelDialogOfEdit() {
      console.log("修改文案_取消按钮");
      this.showDialogOfEdit = false;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.queryOfContent.pageSize = pageSize;
      this.getContentData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.queryOfContent.pageNum = pageNum;
      this.getContentData();
    }
    //===================文案部分结束==========================

  },
  created: function created() {
    this.getGoodsDetail();
    this.getCommissionLevel();
  }
};

/***/ }),

/***/ 473:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(474);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("8efc0b98", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-555e0c7f\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editGoodsOfAdd.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-555e0c7f\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./editGoodsOfAdd.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 474:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-555e0c7f] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n.w120[data-v-555e0c7f] {\n  width: 120px;\n}\n", ""]);

// exports


/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品基本信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.goodsData.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品编号：" + _vm._s(_vm.goodsData.goodsCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品库存：" + _vm._s(_vm.goodsData.inventory))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品品牌：" + _vm._s(_vm.goodsData.goodsBrandName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品分类：" + _vm._s(_vm.goodsData.goodsCategoryName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "商品价格（元）：" + _vm._s(_vm.goodsData.sellingPrice)
                  )
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "总推荐数：" + _vm._s(_vm.goodsData.goodsRecommendCount)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("总点击数：" + _vm._s(_vm.goodsData.goodsClickCount))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("总结算数：" + _vm._s(_vm.goodsData.goodsSettleCount))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("总销售额：" + _vm._s(_vm.goodsData.grossDealAmount))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "总产生佣金（元）：" +
                      _vm._s(_vm.goodsData.grossCommisionAmount)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("新增时间：" + _vm._s(_vm.goodsData.createTime))
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("设置推荐截止时间")
          ]),
          _vm._v(" "),
          _c("span", [_vm._v("截止时间:")]),
          _vm._v(" "),
          _c("el-date-picker", {
            attrs: {
              type: "date",
              "value-format": "yyyy-MM-dd HH:mm:ss",
              placeholder: "选择截止时间"
            },
            model: {
              value: _vm.endTime,
              callback: function($$v) {
                _vm.endTime = $$v
              },
              expression: "endTime"
            }
          }),
          _vm._v(" "),
          _c("p", { staticStyle: { color: "#FFA45B", "margin-top": "20px" } }, [
            _vm._v("* 推荐选择时间持续三个月以上")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("设置佣金比例")]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "ruleForm",
              attrs: {
                model: _vm.ruleForm,
                rules: _vm.rules,
                "label-width": "80px"
              }
            },
            [
              _c(
                "el-row",
                { attrs: { gutter: 20 } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "佣金比例", prop: "v1" } },
                        [
                          _c("el-input", {
                            staticClass: "w120",
                            on: { change: _vm.setAll },
                            model: {
                              value: _vm.ruleForm.v1,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "v1", $$v)
                              },
                              expression: "ruleForm.v1"
                            }
                          }),
                          _vm._v(
                            "%(￥" +
                              _vm._s(
                                Math.floor(
                                  _vm.ruleForm.v1 /
                                    100 *
                                    _vm.goodsData.sellingPrice *
                                    100
                                ) / 100
                              ) +
                              ")\n           "
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品属性")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: {
                  prop: "specName1",
                  label: _vm.attrName[0].attributeName
                }
              }),
              _vm._v(" "),
              _vm.attrName[1]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName2",
                      label: _vm.attrName[1].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.attrName[2]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName3",
                      label: _vm.attrName[2].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "commissionRate", label: "*佣金比例" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("div", [
                          _vm._v(_vm._s(scope.row.commissionRate / 100) + " %")
                        ])
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "orange-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.edit(scope.row, scope.$index)
                              }
                            }
                          },
                          [_vm._v("编辑佣金")]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "purple-bg mt10", on: { click: _vm.save } },
        [_vm._v("保 存")]
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt10", on: { click: _vm.goBack } },
        [_vm._v("返 回")]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content mt20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐文案")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt10 mb10" },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  attrs: { icon: "el-icon-plus" },
                  on: { click: _vm.addContent }
                },
                [_vm._v("新增文案")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt10 mb10" },
            [
              _c(
                "el-select",
                {
                  staticClass: "mt5 mb5",
                  attrs: {
                    filterable: "",
                    clearable: "",
                    placeholder: "选择显示状态"
                  },
                  on: { change: _vm.selectVal1 },
                  model: {
                    value: _vm.val1,
                    callback: function($$v) {
                      _vm.val1 = $$v
                    },
                    expression: "val1"
                  }
                },
                _vm._l(_vm.options1, function(item) {
                  return _c("el-option", {
                    key: item.value,
                    attrs: { label: item.label, value: item.value }
                  })
                })
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableDataOfContent, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "documentContent", label: "文案内容" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "isUp", label: "显示状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n           " +
                            _vm._s(_vm._f("changeIsUp1")(scope.row.isUp)) +
                            "\n         "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sort", label: "排序号", width: "120" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("el-input", {
                          attrs: { placeholder: "999" },
                          on: {
                            blur: function($event) {
                              _vm.editSort(scope.row)
                            }
                          },
                          nativeOn: {
                            keyup: function($event) {
                              if (
                                !("button" in $event) &&
                                _vm._k($event.keyCode, "enter", 13, $event.key)
                              ) {
                                return null
                              }
                              _vm.editSort(scope.row)
                            }
                          },
                          model: {
                            value: scope.row.sort,
                            callback: function($$v) {
                              _vm.$set(scope.row, "sort", $$v)
                            },
                            expression: "scope.row.sort"
                          }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作", width: "120" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "orange-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.change(scope.row, scope.$index)
                              }
                            }
                          },
                          [_vm._v("修改")]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt20" },
            [
              _c("el-button", { attrs: { type: "text" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "fr" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.queryOfContent.pageNum,
                      "page-sizes": [20, 50, 100, 200],
                      "page-size": _vm.queryOfContent.pageSize,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total
                    },
                    on: {
                      "size-change": _vm.handleSizeChange,
                      "current-change": _vm.handleCurrentChange
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "编辑属性佣金", visible: _vm.showDialog },
          on: {
            "update:visible": function($event) {
              _vm.showDialog = $event
            }
          }
        },
        [
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.goodsData.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "销售价（元）：" + _vm._s(_vm.FormDialogRow.sellingPrice)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("库存量：" + _vm._s(_vm.FormDialogRow.inventory))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    _vm._s(_vm.attrName[0].attributeName) +
                      "：" +
                      _vm._s(_vm.FormDialogRow.specName1)
                  )
                ]),
                _vm._v(" "),
                _vm.attrName[1]
                  ? _c("div", { staticClass: "p10" }, [
                      _vm._v(
                        _vm._s(_vm.attrName[1].attributeName) +
                          "：" +
                          _vm._s(_vm.FormDialogRow.specName2)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.attrName[2]
                  ? _c("div", { staticClass: "p10" }, [
                      _vm._v(
                        _vm._s(_vm.attrName[2].attributeName) +
                          "：" +
                          _vm._s(_vm.FormDialogRow.specName3)
                      )
                    ])
                  : _vm._e()
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("hr", { staticStyle: { color: "#E4E7EC", margin: "20px" } }),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { "font-size": "18px !important", margin: "20px" } },
            [_vm._v("设置佣金比例")]
          ),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "FormDialog",
              attrs: {
                model: _vm.FormDialog,
                rules: _vm.rules,
                "label-width": "80px"
              }
            },
            [
              _c(
                "el-row",
                { attrs: { gutter: 20 } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 10 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "佣金比例" } },
                        [
                          _c("el-input", {
                            staticClass: "w120",
                            on: { input: _vm.qqq },
                            model: {
                              value: _vm.FormDialog.commissionRate,
                              callback: function($$v) {
                                _vm.$set(_vm.FormDialog, "commissionRate", $$v)
                              },
                              expression: "FormDialog.commissionRate"
                            }
                          }),
                          _vm._v(
                            "%(￥" +
                              _vm._s(
                                Math.floor(
                                  _vm.FormDialog.commissionRate /
                                    100 *
                                    _vm.FormDialogRow.sellingPrice *
                                    100
                                ) / 100
                              ) +
                              ")\n           "
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                { staticClass: "purple-bg", on: { click: _vm.saveDialog } },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                { staticClass: "orange-bg", on: { click: _vm.cancelDialog } },
                [_vm._v("取 消")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "新增文案", visible: _vm.showDialogOfAdd },
          on: {
            "update:visible": function($event) {
              _vm.showDialogOfAdd = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "content",
              attrs: {
                model: _vm.formOfAdd,
                "label-position": "left",
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "文案内容", prop: "name" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "textarea",
                              autosize: { minRows: 6, maxRows: 10 },
                              placeholder: "请输入内容"
                            },
                            model: {
                              value: _vm.formOfAdd.content,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfAdd, "content", $$v)
                              },
                              expression: "formOfAdd.content"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                { staticStyle: { display: "none" } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "" } },
                        [_c("el-input")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  on: { click: _vm.saveDialogOfAdd }
                },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                {
                  staticClass: "orange-bg",
                  on: { click: _vm.cancelDialogOfAdd }
                },
                [_vm._v("取 消")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "修改文案", visible: _vm.showDialogOfEdit },
          on: {
            "update:visible": function($event) {
              _vm.showDialogOfEdit = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "showDialogOfEdit",
              attrs: {
                model: _vm.formOfEdit,
                "label-position": "left",
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "文案内容", prop: "name" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "textarea",
                              autosize: { minRows: 6, maxRows: 10 }
                            },
                            model: {
                              value: _vm.formOfEdit.documentContent,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfEdit, "documentContent", $$v)
                              },
                              expression: "formOfEdit.documentContent"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.formOfEdit.sort,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfEdit, "sort", $$v)
                              },
                              expression: "formOfEdit.sort"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-form-item",
                    { attrs: { prop: "isShow", label: "显示状态" } },
                    [
                      _c("el-switch", {
                        model: {
                          value: _vm.formOfEdit.isUp,
                          callback: function($$v) {
                            _vm.$set(_vm.formOfEdit, "isUp", $$v)
                          },
                          expression: "formOfEdit.isUp"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  on: { click: _vm.saveDialogOfEdit }
                },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                {
                  staticClass: "orange-bg",
                  on: { click: _vm.cancelDialogOfEdit }
                },
                [_vm._v("返 回")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-555e0c7f", esExports)
  }
}

/***/ })

});