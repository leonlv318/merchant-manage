webpackJsonp([24],{

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_2cf201c0_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_asortGoodsDetail_vue__ = __webpack_require__(385);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(383)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-2cf201c0"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_asortGoodsDetail_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_2cf201c0_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_asortGoodsDetail_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\asortGoodsDetail.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2cf201c0", Component.options)
  } else {
    hotAPI.reload("data-v-2cf201c0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 308:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    }
  },
  data: function data() {
    return {
      allCommission: "",
      data: [],
      attrName: [],
      asortData: {
        asortName: "",
        sort: ""
      },
      storeCategoryName: "",
      sortNum: "",
      inverseStr: "",
      tableData: [],
      attrLength: 1,
      rules: {
        sort: [{ required: true, message: "必填", trigger: "blur" }]
      }
    };
  },

  methods: {
    /**@function 获取商品详情 */
    getChannelGoods: function getChannelGoods() {
      var _this = this;

      this.$http.get(this.$api.findGoods, {
        params: {
          goodsId: this.$route.params.goodsId
        }
      }).then(function (rsp) {
        console.log("获取商品详情");
        console.log(rsp);
        var data = rsp.data.data;
        _this.data = data;
        _this.GoodsInfo = rsp.data.data;

        // 获取goodsAttributeDtos 长度 渲染标题
        _this.attrLength = _this.GoodsInfo.goodsSkuDtos.length;
        _this.attrName = _this.GoodsInfo.goodsAttributeDtos;

        // 商品信息数据
        var skuDetail = _this.GoodsInfo.goodsSkuDtos;

        _this.tableData = skuDetail;
        // 拼接商品信息佣金
        var inverse = _this.GoodsInfo.channelGoodsDetailDto.channelGoodsCommissionDtos;
        var inverseStr = "";
        for (var index in inverse) {
          inverseStr = inverseStr + inverse[index].commissionRate + "%（" + inverse[index].levelName + "）；";
        }
        _this.inverseStr = inverseStr;
      });
    },

    /**@function 搜索名称/编号/品牌 */
    searchVal1: function searchVal1() {
      console.log(this.val1);
      this.query.searchName = this.val1;
      this.getGoodsDetail();
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    },

    // 修改排序号
    save: function save(formName) {
      var _this2 = this;

      this.$refs[formName].validate(function (valid) {
        if (valid) {
          _this2.$http.get(_this2.$api.updateStoreGoodsCategorySort, {
            params: {
              storeGoodsCategoryId: _this2.$route.params.storeGoodsCategoryId,
              sort: _this2.asortData.sort
            }
          }).then(function (rsp) {
            console.log("获取保存状态");
            console.log(rsp);
            if (rsp.data.flag == true) {
              _this2.$message({
                type: "success",
                message: "保存成功"
              });
            } else {
              _this2.$message({
                type: "error",
                message: "保存失败"
              });
            }
          });
        } else {
          _this2.$message({
            type: "error",
            message: "请按要求填写。"
          });
          return false;
        }
      });
    },
    add: function add() {},
    Delete: function Delete() {
      this.$router.push("/assort/goods");
    },
    objectSpanMethod: function objectSpanMethod(_ref) {
      var row = _ref.row,
          column = _ref.column,
          rowIndex = _ref.rowIndex,
          columnIndex = _ref.columnIndex;

      console.log(rowIndex);
      console.log(columnIndex);
      if (columnIndex === 0) {
        if (rowIndex % 2 === 0) {
          return {
            rowspan: 2,
            colspan: 1
          };
        } else {
          return {
            rowspan: 0,
            colspan: 0
          };
        }
      }
    }
  },
  created: function created() {
    this.getChannelGoods();
  }
};

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(384);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("9bb4983a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2cf201c0\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./asortGoodsDetail.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-2cf201c0\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./asortGoodsDetail.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 384:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-2cf201c0] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐商品信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.data.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品编号：" + _vm._s(_vm.data.goodsCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品货号：" + _vm._s(_vm.data.itemCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品品牌：" + _vm._s(_vm.data.goodsBrandName))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品分类：" + _vm._s(_vm.data.goodsCategoryName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品原价（元）：" + _vm._s(_vm.data.originalPrice))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品销售价（元）：" + _vm._s(_vm.data.sellingPrice))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("库存量（件）：" + _vm._s(_vm.data.inventory))
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品属性")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: {
                data: _vm.tableData,
                border: "",
                "tooltip-effect": "dark"
              }
            },
            [
              _c("el-table-column", {
                attrs: {
                  prop: "specName1",
                  label: _vm.attrName[0].attributeName
                }
              }),
              _vm._v(" "),
              _vm.attrLength == 2
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName2",
                      label: _vm.attrName[1].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.attrLength == 3
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName3",
                      label: _vm.attrName[2].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "originalPrice", label: "原价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "imageUrl", label: "*商家图片", width: "600px" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          staticStyle: { width: "60px", height: "60px" },
                          attrs: { src: scope.row.imageUrl }
                        })
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.goBack } },
        [_vm._v("返  回")]
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.Delete } },
        [_vm._v("删  除")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-2cf201c0", esExports)
  }
}

/***/ })

});