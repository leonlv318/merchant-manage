webpackJsonp([28],{

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_f0b9c024_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Goods_vue__ = __webpack_require__(342);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(340)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f0b9c024"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Goods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_f0b9c024_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Goods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\Goods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f0b9c024", Component.options)
  } else {
    hotAPI.reload("data-v-f0b9c024", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    },
    changeGoodsAudit: function changeGoodsAudit(value) {
      if (value == 2) {
        return "通过";
      } else if (value == 3) {
        return "驳回";
      } else {
        return "未审核";
      }
    }
  },
  data: function data() {
    return {
      val1: "",
      val2: "",
      val3: "",
      val4: [],
      val5: "",
      val6: "",
      val4Options: [], //分类下拉选项
      val5Options: [{
        value: "1",
        label: "上架"
      }, {
        value: "0",
        label: "下架"
      }],
      val6Options: [{
        value: "1",
        label: "未审核"
      }, {
        value: "2",
        label: "通过"
      }, {
        value: "3",
        label: "驳回"
      }],
      total: 0,
      pages: 0,
      multipleSelection: [],
      options: [{
        value: "0",
        label: "不可用"
      }],
      value8: "",

      tableData: []
    };
  },

  methods: {
    /**@function 获取商品列表 */
    getGoodsData: function getGoodsData() {
      var _this = this;

      this.$http.get(this.$api.findGoodsListPage, { params: { shop_id: this.shop_id } }).then(function (rsp) {
        console.log("获取商品列表");
        console.log(rsp);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
        if (_this.$route.params.query && _this.val4 == []) {
          _this.query.goodsCategoryId = "";
        }
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findGoodsCategoryList).then(function (rsp) {
        var orData = rsp.data.data;
        var toget = function toget(orData) {
          var data = orData;
          var arrl = orData.length;
          var arr = [];
          for (var i = 0; i < arrl; i++) {
            //一级
            var obj1 = {};
            obj1.value = data[i].goodsCategoryId;
            obj1.label = data[i].goodsCategoryName;
            if (data[i].childCategoryList) {
              //是否有2级
              obj1.childrentemp = data[i].childCategoryList;
              obj1.children = [];
              for (var j = 0; j < obj1.childrentemp.length; j++) {
                var obj2 = {};
                obj2.children = [];
                obj2.value = obj1.childrentemp[j].goodsCategoryId;
                obj2.label = obj1.childrentemp[j].goodsCategoryName;
                obj1.children.push(obj2);
                // obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                if (obj1.childrentemp[j].childCategoryList) {
                  //是否有3级
                  obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                  for (var k = 0; k < obj2.childrentemp.length; k++) {
                    var obj3 = {};
                    obj3.value = obj2.childrentemp[k].goodsCategoryId;
                    obj3.label = obj2.childrentemp[k].goodsCategoryName;
                    obj2.children.push(obj3);
                  }
                }
              }
              arr[i] = obj1;
            } else {
              arr[i] = obj1;
            }
          }
          return arr;
        };
        var arr = toget(orData);
        console.log("生成分类数据");
        console.log(arr);
        _this2.val4Options = arr;
      });
    },

    /**@function 搜索商品名称/编号/品牌 */
    searchVal1: function searchVal1() {
      this.query.name = this.val1;
      this.getGoodsData();
    },

    /**@function 选择分类 */
    selectVal4: function selectVal4() {
      var l = this.val4.length;
      this.query.goodsCategoryId = this.val4[l - 1];
      this.getGoodsData();
    },

    /**@function 选择销售状态 */
    selectVal5: function selectVal5() {
      this.query.isUp = this.val5;
      this.getGoodsData();
    },

    /**@function 选择审核状态 */
    selectVal6: function selectVal6() {
      this.query.goodsAudit = this.val6;
      this.getGoodsData();
    },

    /**@function 启用停用 */
    toggle: function toggle(row, e) {
      var _this3 = this;

      if (row.isUp == "1") {
        this.$confirm("是否下架该商品？(注意：下架后商品将无法销售!)", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          console.log("下架商品");
          params.goodsIds = row.goodsId;
          params.goodsStatus = 0;
          _this3.$http.get(_this3.$api.updateGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            _this3.$isot(rsp); //登录超时处理函数
            if (rsp.data.flag) {
              _this3.getGoodsData();
              _this3.$message({
                type: "success",
                message: "下架商品成功!"
              });
            }
          });
        }).catch(function () {
          _this3.$message({
            type: "info",
            message: "已取消 [下架] 操作"
          });
        });
      } else {
        var params = {};
        console.log("上架商品");
        params.goodsIds = row.goodsId;
        params.goodsStatus = 1;
        this.$http.get(this.$api.updateGoodsIsUp, {
          params: params
        }).then(function (rsp) {
          _this3.$isot(rsp); //登录超时处理函数
          if (rsp.data.flag) {
            _this3.getGoodsData();
            _this3.$message({
              type: "success",
              message: "上架商品成功!"
            });
          }
        });
      }
    },

    /**@function 批量上架 */
    upSelect: function upSelect() {
      var _this4 = this;

      var flag = true;
      this.multipleSelection.forEach(function (v, i) {
        if (v.goodsAudit != 2) {
          flag = false;
        }
      });
      if (this.multipleSelection.length && flag) {
        this.$confirm("是否上架选中商品？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          params.goodsIds = [];
          console.log("上架选中商品1");
          for (var i = 0; i < _this4.multipleSelection.length; i++) {
            params.goodsIds.push(_this4.multipleSelection[i].goodsId);
          }
          params.goodsIds = params.goodsIds.join(",");
          params.goodsStatus = 1;
          _this4.$http.get(_this4.$api.updateGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            if (rsp.data.flag) {
              _this4.getGoodsData();
              _this4.$message({
                type: "success",
                message: "上架选中商品成功!"
              });
            }
          });
        }).catch(function () {
          _this4.$message({
            type: "info",
            message: "已取消 [上架] 操作"
          });
        });
      } else {
        if (!flag) {
          this.$message({
            type: "error",
            message: "所选的商品审核状态必须为通过！"
          });
        } else {
          this.$message({
            type: "error",
            message: "当前未选中任何行！"
          });
        }
      }
    },

    /**@function 批量下架 */
    downSelect: function downSelect() {
      var _this5 = this;

      var flag = true;
      this.multipleSelection.forEach(function (v, i) {
        if (v.goodsAudit != 2) {
          flag = false;
        }
      });
      if (this.multipleSelection.length && flag) {
        this.$confirm("是否下架选中商品？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          params.goodsIds = [];
          for (var i = 0; i < _this5.multipleSelection.length; i++) {
            params.goodsIds.push(_this5.multipleSelection[i].goodsId);
          }
          params.goodsStatus = 0;
          params.goodsIds = params.goodsIds.join(",");
          console.log(params);
          _this5.$http.get(_this5.$api.updateGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            _this5.$isot(rsp); //登录超时处理函数
            if (rsp.data.flag) {
              _this5.getGoodsData();
              _this5.$message({
                type: "success",
                message: "下架选中商品成功!"
              });
            }
          });
        }).catch(function () {
          _this5.$message({
            type: "info",
            message: "已取消 [下架] 操作"
          });
        });
      } else {
        if (!flag) {
          this.$message({
            type: "error",
            message: "所选的商品审核状态必须为通过！"
          });
        } else {
          this.$message({
            type: "error",
            message: "当前未选中任何行！"
          });
        }
      }
    },

    /**@function 查看商品 */
    checkgoods: function checkgoods(row, $index) {
      //   console.log(row);
      console.log("跳转商品详情页，id ↓");
      console.log(row.goodsId);
      this.$router.push({
        name: "gc",
        params: { id: row.goodsId, query: this.query }
      });
    },

    /**@function 修改商品 */
    edit: function edit(row) {
      console.log("跳转商品详情页，id ↓");
      console.log(row.goodsId);
      this.$router.push({
        name: "ge",
        params: { id: row.goodsId, query: this.query }
      });
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    },

    /**@function 新增商品 */
    addGoods: function addGoods() {
      this.$router.push("/goods/addgoods");
    }
  },
  created: function created() {
    console.log('shop_id');
    console.log(window.sessionStorage.getItem('shop_id'));
    var shop_id = window.sessionStorage.getItem('shop_id');
    this.shop_id = shop_id;
    this.getGoodsData();
  }
};

/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(341);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("6a8fbf64", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f0b9c024\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Goods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f0b9c024\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Goods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-f0b9c024],\n.search-group[data-v-f0b9c024] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-f0b9c024] {\n  float: right;\n}\n.up-btn[data-v-f0b9c024] {\n  position: relative;\n}\n.file-upload[data-v-f0b9c024] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n.hover_show[data-v-f0b9c024] {\n  position: fixed;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n  display: block;\n  border: 4px solid rgba(77, 108, 243, 0.8);\n  border-radius: 4px;\n}\n.hover_hide[data-v-f0b9c024] {\n  display: none;\n}\n", ""]);

// exports


/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "btn-group" },
          [
            _c(
              "el-button",
              {
                staticClass: "purple-bg",
                attrs: { icon: "el-icon-plus" },
                on: { click: _vm.addGoods }
              },
              [_vm._v("新增商品")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-input", {
              staticClass: "search-input mt5 mb5",
              attrs: {
                placeholder: "搜索商品名称/编号/品牌",
                "prefix-icon": "el-icon-search",
                clearable: ""
              },
              on: { blur: _vm.searchVal1 },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal1($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c("el-cascader", {
              staticClass: "mt5 mb5",
              attrs: {
                clearable: "",
                placeholder: "选择分类",
                "expand-trigger": "click",
                options: _vm.val4Options,
                "change-on-select": ""
              },
              on: { change: _vm.selectVal4 },
              model: {
                value: _vm.val4,
                callback: function($$v) {
                  _vm.val4 = $$v
                },
                expression: "val4"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "mt5 mb5",
                attrs: {
                  filterable: "",
                  clearable: "",
                  placeholder: "选择销售状态"
                },
                on: { change: _vm.selectVal5 },
                model: {
                  value: _vm.val5,
                  callback: function($$v) {
                    _vm.val5 = $$v
                  },
                  expression: "val5"
                }
              },
              _vm._l(_vm.val5Options, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            ),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "mt5 mb5",
                attrs: {
                  filterable: "",
                  clearable: "",
                  placeholder: "选择审核状态"
                },
                on: { change: _vm.selectVal6 },
                model: {
                  value: _vm.val6,
                  callback: function($$v) {
                    _vm.val6 = $$v
                  },
                  expression: "val6"
                }
              },
              _vm._l(_vm.val6Options, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "multipleTable",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" },
            on: { "selection-change": _vm.handleSelectionChange }
          },
          [
            _c("el-table-column", {
              attrs: { type: "selection", width: "55" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsName", label: "名称" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "图片", width: "70" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("img", {
                        attrs: {
                          src: scope.row.imageUrl,
                          width: "52",
                          height: "52"
                        }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsCode", label: "编号" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "itemCode", label: "货号" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsBrandName", label: "品牌" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "categoryName", label: "分类" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "originalPrice", label: "原价(元)" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "sellingPrice", label: "销售价(元)" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "inventory", label: "库存量(件)" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "isUp", label: "销售状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsAudit", label: "审核状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(
                            _vm._f("changeGoodsAudit")(scope.row.goodsAudit)
                          ) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作", width: "140px" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.checkgoods(scope.row, scope.$index)
                            }
                          }
                        },
                        [_vm._v("查看\n          ")]
                      ),
                      _vm._v(" "),
                      scope.row.isUp == "0" && scope.row.goodsAudit == 2
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "purple-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("上架\n          ")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      scope.row.isUp == "1" && scope.row.goodsAudit == 2
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "orange-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("下架\n          ")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "el-button",
                        {
                          staticClass: "orange-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.edit(scope.row)
                            }
                          }
                        },
                        [_vm._v("修改")]
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f0b9c024", esExports)
  }
}

/***/ })

});