webpackJsonp([15],{

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_99084bc2_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Total_vue__ = __webpack_require__(388);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(386)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-99084bc2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Total_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_99084bc2_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Total_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\orders\\Total.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-99084bc2", Component.options)
  } else {
    hotAPI.reload("data-v-99084bc2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeOrderStatus: function changeOrderStatus(value) {
      if (value == 1) {
        return "待付款";
      } else if (value == 2) {
        return "待发货";
      } else if (value == 3) {
        return "待收货";
      } else if (value == 4) {
        return "已完成";
      } else if (value == 5) {
        return "已取消";
      }
    }
  },
  data: function data() {
    return {
      val1: "",
      val2: "",
      val3: "",
      options: [{
        value: "",
        label: "全部"
      }, {
        value: "2",
        label: "待发货"
      }, {
        value: "3",
        label: "待收货"
      }, {
        value: "4",
        label: "已完成"
      }, {
        value: "5",
        label: "已取消"
      }],
      value8: "",
      total: 0,
      pages: 0,
      multipleSelection: [],
      query: {
        name: "",
        orderStatus: "",
        beginDate: "",
        endDate: "",
        pageNum: 1,
        pageSize: 20
      },
      tableData: []
    };
  },

  methods: {
    /**@function 获取订单列表 */
    getOrderList: function getOrderList() {
      var _this = this;

      if (this.$route.params.query) {
        this.query = this.$route.params.query;
        this.val1 = this.query.name;
        this.val2 = this.query.orderStatus;
        this.val3.push(this.query.beginDate);
        this.val3.push(this.query.endDate);
      }
      this.$http.get(this.$api.findOrdersListPage, { params: this.query }).then(function (rsp) {
        console.log("获取订单列表");
        console.log(rsp.data);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 查看 */
    checkOrder: function checkOrder(row) {
      //   console.log(row);
      /** 根据传入的flag判断进入订单详情的状态
       * 1:待付款
       * 2:待发货
       * 3:待收货
       * 4:已完成
       * 5:已取消
       */
      var oderflag = 0;
      switch (row.orderStatus) {
        case 1:
          oderflag = 1;
          console.log("待付款");
          //   this.$router.push("/oders/orderdetail");
          this.$router.push({
            name: "oo",
            params: {
              no: row.orderId,
              flag: oderflag,
              type: "待付款",
              query: this.query
            }
          });
          break;
        case 2:
          oderflag = 2;
          console.log("待发货");
          this.$router.push({
            name: "oo",
            params: {
              no: row.orderId,
              flag: oderflag,
              type: "待发货",
              query: this.query
            }
          });
          break;
        case 3:
          oderflag = 3;
          console.log("待收货");
          this.$router.push({
            name: "oo",
            params: {
              no: row.orderId,
              flag: oderflag,
              type: "待收货",
              query: this.query
            }
          });
          break;
        case 4:
          oderflag = 4;
          console.log("已完成");
          this.$router.push({
            name: "oo",
            params: {
              no: row.orderId,
              flag: oderflag,
              type: "已完成",
              query: this.query
            }
          });
          break;
        case 5:
          oderflag = 5;
          console.log("已取消");
          this.$router.push({
            name: "oo",
            params: {
              no: row.orderId,
              flag: oderflag,
              type: "已取消",
              query: this.query
            }
          });
          break;
        default:
          break;
      }
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getOrderList();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getOrderList();
    },

    /**@function 搜索购买用户/订单号 */
    searchVal1: function searchVal1() {
      this.query.name = this.val1;
      this.getOrderList();
    },

    /**@function 选择订单状态 */
    selectVal2: function selectVal2() {
      this.query.orderStatus = this.val2;
      this.getOrderList();
    },

    /**@function 选择日期 */
    selectVal3: function selectVal3() {
      if (this.val3) {
        this.query.beginDate = this.val3[0];
        this.query.endDate = this.val3[1].substring(0, 10) + " 23:59:59";
      } else {
        this.query.beginDate = this.query.endDate = "";
      }
      this.getOrderList();
    },

    /**@function 导出订单 */
    exportOrder: function exportOrder() {
      console.log("this.router");
      console.log(this.$router);

      window.location.assign(this.$apiDomain + "/admin/orders/exportOrderList?name=" + this.query.name + "&orderStatus" + this.query.orderStatus + "=&beginDate" + this.query.beginDate + "=&endDate=" + this.query.endDate);
    }
  },
  created: function created() {
    this.getOrderList();
  }
};

/***/ }),

/***/ 386:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(387);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("a6d5f16a", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-99084bc2\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Total.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-99084bc2\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Total.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 387:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.pagination[data-v-99084bc2] {\n  float: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("订单管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content" },
        [
          _c(
            "div",
            {
              staticClass: "search-group",
              staticStyle: { "margin-bottom": "20px" }
            },
            [
              _c("el-input", {
                staticClass: "search-input",
                attrs: {
                  placeholder: "搜索购买用户/订单号",
                  "prefix-icon": "el-icon-search",
                  clearable: ""
                },
                on: { blur: _vm.searchVal1 },
                nativeOn: {
                  keyup: function($event) {
                    if (
                      !("button" in $event) &&
                      _vm._k($event.keyCode, "enter", 13, $event.key)
                    ) {
                      return null
                    }
                    _vm.searchVal1($event)
                  }
                },
                model: {
                  value: _vm.val1,
                  callback: function($$v) {
                    _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                  },
                  expression: "val1"
                }
              }),
              _vm._v(" "),
              _c(
                "el-select",
                {
                  attrs: {
                    clearable: "",
                    filterable: "",
                    placeholder: "选择订单状态"
                  },
                  on: { change: _vm.selectVal2 },
                  model: {
                    value: _vm.val2,
                    callback: function($$v) {
                      _vm.val2 = $$v
                    },
                    expression: "val2"
                  }
                },
                _vm._l(_vm.options, function(item) {
                  return _c("el-option", {
                    key: item.value,
                    staticClass: "search-select",
                    attrs: { label: item.label, value: item.value }
                  })
                })
              ),
              _vm._v(" "),
              _c("el-date-picker", {
                attrs: {
                  clearable: "",
                  type: "daterange",
                  "range-separator": "至",
                  "start-placeholder": "开始日期",
                  "end-placeholder": "结束日期",
                  "value-format": "yyyy-MM-dd HH:mm:ss"
                },
                on: { change: _vm.selectVal3 },
                model: {
                  value: _vm.val3,
                  callback: function($$v) {
                    _vm.val3 = $$v
                  },
                  expression: "val3"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "singleTable",
              staticStyle: { width: "100%" },
              attrs: {
                data: _vm.tableData,
                "tooltip-effect": "dark",
                "default-sort": { prop: "time", order: "descending" }
              }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "orderSn", label: "订单号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "nickName", label: "购买用户" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "payableAmount", label: "订单金额(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "orderStatus", label: "订单状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(
                              _vm._f("changeOrderStatus")(scope.row.orderStatus)
                            ) +
                            "\n        "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: {
                  prop: "createDate",
                  label: "下单时间",
                  sortable: "",
                  width: "200"
                }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "detail", label: "商品详情" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "purple-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.checkOrder(scope.row)
                              }
                            }
                          },
                          [_vm._v("查看")]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { "margin-top": "20px" } },
            [
              _c("el-button", { attrs: { type: "text", disabled: "" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "pagination" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.query.pageNum,
                      "page-sizes": [20, 50, 100, 200],
                      "page-size": _vm.query.pageSize,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total
                    },
                    on: {
                      "size-change": _vm.handleSizeChange,
                      "current-change": _vm.handleCurrentChange
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        {
          staticClass: "purple-bg",
          staticStyle: { "margin-top": "20px" },
          on: { click: _vm.exportOrder }
        },
        [_vm._v("导出订单")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-99084bc2", esExports)
  }
}

/***/ })

});