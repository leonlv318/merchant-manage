webpackJsonp([34],{

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_1914faba_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Carousel_vue__ = __webpack_require__(406);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(404)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1914faba"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Carousel_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_1914faba_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Carousel_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\carousel\\Carousel.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1914faba", Component.options)
  } else {
    hotAPI.reload("data-v-1914faba", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 315:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上线";
      } else {
        return "下线";
      }
    }
  },
  data: function data() {
    return {
      shop_id: "",
      total: 0,
      pages: 0,
      multipleSelection: [],
      val1: "",
      val2: "",
      val3: "",
      options: [{
        value: "1",
        label: "上线"
      }, {
        value: "0",
        label: "下线"
      }],
      value8: "",
      query: {
        pageNum: 1,
        pageSize: 20,
        title: "",
        isUp: "",
        beginUseTime: "",
        endUseTime: ""
      },
      tableData: []
    };
  },

  methods: {
    /**@function 获取广告列表 */
    getCarouselList: function getCarouselList() {
      var _this = this;

      this.$http.get(this.$api.findAdImageListPage, { params: { shop_id: this.shop_id } }).then(function (rsp) {
        _this.$isot(rsp); //登录超时处理函数
        console.log("获取广告列表");
        console.log(rsp.data);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 查看 */
    checkcarousel: function checkcarousel(row) {
      this.$router.push({
        name: "cck",
        params: { id: row.adImageId }
      });
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getCarouselList();
    },
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getCarouselList();
    },

    /**@function 新增商品 */
    addCarousel: function addCarousel() {
      this.$router.push("/carousel/addcarousel");
    },

    /**@function 搜索 */
    searchVal1: function searchVal1() {
      this.query.title = this.val1;
      this.getCarouselList();
    },

    /**@function 选择 */
    selectVal2: function selectVal2() {
      this.query.isUp = this.val2;
      this.getCarouselList();
    },
    selectVal3: function selectVal3() {
      if (this.val3) {
        this.query.beginUseTime = this.val3[0];
        this.query.endUseTime = this.val3[1].substring(0, 10) + " 23:59:59";
      } else {
        this.query.beginUseTime = this.query.endUseTime = "";
      }
      this.getCarouselList();
    },

    /**@function 修改排序号 */
    changeSort: function changeSort(row) {
      console.log("修改排序号");
      console.log(row);
    },

    /**@function 上线下线请求函数 */
    toggleIsUp: function toggleIsUp(adImageId, status) {
      var _this2 = this;

      this.$http(this.$api.updateStoreAdImageStauts + ("/?adImageId=" + adImageId + "&status=" + status)).then(function (rsp) {
        _this2.$isot(rsp); //登录超时处理函数
      });
    },

    /**@function 上线下线 */
    toggle: function toggle(row, e) {
      var _this3 = this;

      if (row.isUp == "1") {
        this.$confirm("是否下线广告？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          console.log("下线广告");
          params.adImageId = row.adImageId;
          params.status = 0;
          _this3.$http.get(_this3.$api.updateStoreAdImageStauts, {
            params: params
          }).then(function (rsp) {
            _this3.$isot(rsp); //登录超时处理函数
            _this3.getCarouselList();
          });
          _this3.$message({
            type: "warning",
            message: "下线广告成功!"
          });
        }).catch(function () {
          _this3.$message({
            type: "info",
            message: "已取消 [下线] 操作"
          });
        });
      } else {
        var params = {};
        params.adImageId = row.adImageId;
        params.status = 1;
        this.$http.get(this.$api.updateStoreAdImageStauts, { params: params }).then(function (rsp) {
          _this3.$isot(rsp); //登录超时处理函数
          _this3.getCarouselList();
        });
        this.$message({
          type: "success",
          message: "上线广告成功!"
        });
      }
    }
  },
  created: function created() {
    console.log("shop_id");
    console.log(window.sessionStorage.getItem("shop_id"));
    var shop_id = window.sessionStorage.getItem("shop_id");
    this.shop_id = shop_id;
    this.getCarouselList();
  }
};

/***/ }),

/***/ 404:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(405);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("21114405", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1914faba\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Carousel.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1914faba\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Carousel.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-1914faba],\n.search-group[data-v-1914faba] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-1914faba] {\n  float: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("轮播图管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "btn-group" },
          [
            _c(
              "el-button",
              {
                staticClass: "purple-bg",
                attrs: { icon: "el-icon-plus" },
                on: { click: _vm.addCarousel }
              },
              [_vm._v("新增轮播图")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c(
              "el-select",
              {
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择广告状态"
                },
                on: { change: _vm.selectVal2 },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              },
              _vm._l(_vm.options, function(item) {
                return _c("el-option", {
                  key: item.value,
                  staticClass: "search-select",
                  attrs: { label: item.label, value: item.value }
                })
              })
            ),
            _vm._v(" "),
            _c("el-date-picker", {
              attrs: {
                clearable: "",
                type: "daterange",
                "range-separator": "至",
                "start-placeholder": "选择开始日期",
                "end-placeholder": "选择结束日期"
              },
              on: { change: _vm.selectVal3 },
              model: {
                value: _vm.val3,
                callback: function($$v) {
                  _vm.val3 = $$v
                },
                expression: "val3"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "singleTable",
            staticStyle: { width: "100%" },
            attrs: {
              data: _vm.tableData,
              "tooltip-effect": "dark",
              "default-sort": { prop: "sort_no", order: "descending" }
            },
            on: { "selection-change": _vm.handleSelectionChange }
          },
          [
            _c("el-table-column", { attrs: { type: "index", label: "序号" } }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "title", label: "轮播图名称" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "图片", width: "260" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("img", {
                        attrs: { src: scope.row.imgUrl, width: "240" }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "轮播图持续时间" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(scope.row.beginUseTime) +
                          " 至 " +
                          _vm._s(scope.row.endUseTime) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "sort", label: "排序号", sortable: "" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("el-input", {
                        attrs: { disabled: true, placeholder: "999" },
                        nativeOn: {
                          keyup: function($event) {
                            if (
                              !("button" in $event) &&
                              _vm._k($event.keyCode, "enter", 13, $event.key)
                            ) {
                              return null
                            }
                            _vm.changeSort(scope.row)
                          }
                        },
                        model: {
                          value: scope.row.sort,
                          callback: function($$v) {
                            _vm.$set(scope.row, "sort", $$v)
                          },
                          expression: "scope.row.sort"
                        }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "isUp", label: "轮播图状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.checkcarousel(scope.row)
                            }
                          }
                        },
                        [_vm._v("查看")]
                      ),
                      _vm._v(" "),
                      scope.row.isUp != "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "purple-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("上线")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      scope.row.isUp == "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "orange-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("下线")]
                          )
                        : _vm._e()
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticStyle: { "margin-top": "20px" } },
          [
            _c("el-button", { attrs: { type: "text", disabled: "" } }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pagination" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.query.pageNum,
                    "page-sizes": [20, 50, 100, 200],
                    "page-size": _vm.query.pageSize,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1914faba", esExports)
  }
}

/***/ })

});