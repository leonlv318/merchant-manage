webpackJsonp([18],{

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c558374c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_OrderDetail_vue__ = __webpack_require__(403);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(401)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c558374c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OrderDetail_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c558374c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_OrderDetail_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\orders\\OrderDetail.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c558374c", Component.options)
  } else {
    hotAPI.reload("data-v-c558374c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(95);

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changePayStatus: function changePayStatus(value) {
      if (value == 1) {
        return "未付款";
      } else if (value == 2) {
        return "已付款";
      } else if (value == 3) {
        return "已取消";
      }
    },
    changeLD: function changeLD(value) {
      if (!value) {
        return "";
      } else {
        return value;
      }
    }
  },
  data: function data() {
    return {
      stepFlag: 1,
      orderData: [],
      userDeliveryAddress: {},
      currentRecord: [],
      address: "",
      options: [],
      formData: {
        val1: "",
        val2: ""
      },
      rules: {
        val1: [{ required: true, message: "请选择物流", trigger: "change"
          // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' }
        }],
        val2: [{ required: true, message: "请输入运单编号", trigger: "blur" }, {
          pattern: /^\d+(\.\d+)?$/,
          message: "请输入正确格式的运单编号",
          trigger: "blur"
        }]
      },
      logistics_flag: 0,
      co: 0,
      cp: 0,
      type: "",
      typeTitle: "",
      tableData: []
    };
  },

  // computed: {
  //   orderData: function () {
  //     return this.orderData
  //   }
  // },
  methods: {
    /**@function 获取物流选项数据 */
    getLogistics: function getLogistics() {
      var _this = this;

      this.$http.get(this.$api.findStoreLogisticsList).then(function (rsp) {
        console.log("获取物流选项数据");
        console.log(rsp.data);
        var logistics = rsp.data.data;
        for (var i = 0; i < logistics.length; i++) {
          var obj = {};
          obj.value = logistics[i].storeLogisticsId;
          obj.label = logistics[i].logisticsName;
          _this.options.push(obj);
        }
      });
    },

    /**@function 获取订单详情 */
    getOrderDetail: function getOrderDetail() {
      var _this2 = this;

      var id = this.$route.params.no;
      this.$http.get(this.$api.findOrders + ("?orderId=" + id)).then(function (rsp) {
        console.log("获取订单详情");
        console.log(rsp.data);
        _this2.orderData = rsp.data.data;
        _this2.userDeliveryAddress = _this2.orderData.userDeliveryAddress;

        _this2.stepFlag = _this2.orderData.orderStatus;
        var addressAll = _this2.orderData.userDeliveryAddress;
        _this2.address = addressAll.province + addressAll.city + addressAll.district + addressAll.addressDetail;
        _this2.tableData = rsp.data.data.ordersGoodsDetailDtoList;
        if (_this2.orderData.orderDetailsDeliveryDto.currentRecord.length != 0) {
          _this2.currentRecord = _this2.orderData.orderDetailsDeliveryDto.currentRecord[0];
        } else {
          _this2.currentRecord = {
            currentRecordDate: "",
            currentRecordAddress: ""
          };
        }
        //   this.total = rsp.data.data.total;
      });
    },
    handleClick: function handleClick(row) {
      console.log(row);
      this.$router.push("/goods/checkgoods");
    },
    edit: function edit(row) {
      console.log(row);
      this.$router.push("/goods/editgoods");
    },

    /**新增商品 */
    addCarousel: function addCarousel() {
      this.$router.push("/carousel/addcarousel");
    },

    /**@function 选择 */
    selectVal: function selectVal() {
      console.log(this.formData.val1);
    },

    /**@function 搜索 */
    searchVal: function searchVal() {
      console.log(this.formData.val2);
    },

    /**@function 发货 */
    delivery: function delivery() {
      var _this3 = this;

      console.log("发货");
      var params = {};
      params.orderId = this.orderData.orderId;
      params.storeLogisticsId = this.formData.val1;
      params.logisticsSn = this.formData.val2;
      console.log(params);
      var formDataToJson = (0, _stringify2.default)(params);
      this.$http.post(this.$api.deliverGoods, formDataToJson).then(function (rsp) {
        if (rsp.data.flag) {
          _this3.$message.success("发货成功！");
          _this3.$router.go(-1);
        } else {
          _this3.$message.error(rsp.data.msg);
        }
      });
    },

    /**@function 打印物流信息 */
    // printLogistics() {
    //   console.log("打印物流信息");
    // },
    /**@function 打印商品清单 */
    printGoods: function printGoods() {
      //   this.$message.warning("请在关闭打印的页面后再返回后台继续操作！");
      var newwindow = window.open("_blank");
      var trstr = "";
      for (var i = 0; i < this.tableData.length; i++) {
        trstr += "\n\t\t<tr>\n        <td>" + this.tableData[i].detail + "</td>\n        <td>" + this.tableData[i].goodsCode + "</td>\n        <td>" + this.tableData[i].skuSellingPrice + "</td>\n        <td>" + this.tableData[i].goodsNumber + "</td>\n        <td>" + this.tableData[i].totalAmount + "</td>\n\t\t</tr>\n\t\t";
      }
      var printStr = "\n\t  <h2>\u5546\u54C1\u6E05\u5355</h2>\n    <h4>\u4E70\u5BB6\uFF1A" + this.orderData.nickName + "</h4>\n    <table  border=\"1\" cellpadding=\"2\" cellspacing=\"0\">\n      <tr>\n        <th>\u5546\u54C1\u540D\u79F0</th>\n        <th>\u5546\u54C1\u7F16\u53F7</th>\n        <th>\u5355\u4EF7</th>\n        <th>\u6570\u91CF</th>\n        <th>\u5546\u54C1\u603B\u989D</th>\n      </tr>";
      printStr += trstr;
      printStr += "</table>\n    <div style='text-align:right'>\n      <p>\u5546\u54C1\u603B\u4EF7\uFF1A" + this.orderData.payableAmount + "</p>\n      <p>\u8FD0\u8D39\uFF1A" + this.orderData.logisticsAmount + "</p>\n      <p>\u5B9E\u4ED8\u6B3E\uFF1A" + this.orderData.cash + "</p>\n    </div>\n    ";
      newwindow.document.write(printStr);
      newwindow.print();
      newWindow.document.close();
      return true;
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    }
  },
  created: function created() {
    /** 根据传入的flag判断进入订单详情的状态
     * 1:待付款
     * 2:待发货
     * 3:待收货
     * 4:已完成
     * 5:已取消
     */
    // console.log(this.$route);
    // console.log(this.$route.params.type);
    this.typeTitle = this.$route.params.type;
    // console.log(this.typeTitle);

    // 判断 类型  的字体颜色
    if (this.$route.params.flag == 5) {
      this.co = 1; //橙色字体
    } else {
      this.cp = 1; //紫色字体
    }
    // 判断 是否有  物流信息
    if (this.$route.params.flag == 3 || this.$route.params.flag == 0) {
      this.logistics_flag = 1; //显示物流信息
    } else {
      this.logistics_flag = 0; //不显示物流信息
    }
    this.getOrderDetail();
    this.getLogistics();
  }
};

/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(402);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("85c9ef26", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c558374c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./OrderDetail.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c558374c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./OrderDetail.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.pagination[data-v-c558374c] {\n  float: right;\n}\n.content .content-title[data-v-c558374c] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 0 20px 0;\n}\n.type_title[data-v-c558374c] {\n  font-size: 34px;\n  font-family: MicrosoftYaHei-Bold;\n  font-weight: Bold;\n  margin-bottom: 30px;\n}\n.detail_gray[data-v-c558374c] {\n  font-size: 16px;\n  font-family: MicrosoftYaHei;\n  color: #606266;\n}\n.text_right[data-v-c558374c] {\n  text-align: right;\n  width: 150px;\n}\n", ""]);

// exports


/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("订单管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-row",
        { staticStyle: { "margin-bottom": "20px" }, attrs: { gutter: 20 } },
        [
          _c("el-col", { attrs: { span: 6 } }, [
            _c(
              "div",
              {
                staticClass: "content",
                staticStyle: { "text-align": "center" }
              },
              [
                _c(
                  "div",
                  {
                    class: {
                      type_title: 1,
                      "orange-txt": _vm.co,
                      "purple-txt": _vm.cp
                    }
                  },
                  [_vm._v(_vm._s(_vm.typeTitle))]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "detail_gray",
                    staticStyle: { "margin-bottom": "10px" }
                  },
                  [_vm._v("购买用户：" + _vm._s(_vm.orderData.nickName))]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "detail_gray" }, [
                  _vm._v("订单编号：" + _vm._s(_vm.orderData.orderSn))
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("el-col", { attrs: { span: 18 } }, [
            _c(
              "div",
              {
                staticClass: "content",
                staticStyle: { height: "115px" },
                attrs: { "finish-status": "success" }
              },
              [
                _vm.$route.params.flag == 5
                  ? _c(
                      "el-steps",
                      {
                        staticStyle: { "padding-top": "20px" },
                        attrs: {
                          "finish-status": "success",
                          active: 3,
                          "align-center": ""
                        }
                      },
                      [
                        _c("el-step", {
                          attrs: {
                            title: "提交订单",
                            description: _vm.orderData.createDate
                          }
                        }),
                        _vm._v(" "),
                        _c("el-step", {
                          attrs: {
                            title: "支付成功",
                            description: _vm._f("changeLD")(
                              _vm.orderData.payDate
                            )
                          }
                        }),
                        _vm._v(" "),
                        _c("el-step", {
                          attrs: {
                            title: "用户取消",
                            description: _vm._f("changeLD")(
                              _vm.orderData.refundTime
                            )
                          }
                        })
                      ],
                      1
                    )
                  : _c(
                      "el-steps",
                      {
                        staticStyle: { "padding-top": "20px" },
                        attrs: {
                          "finish-status": "success",
                          active: _vm.stepFlag,
                          "align-center": ""
                        }
                      },
                      [
                        _c("el-step", {
                          attrs: {
                            title: "提交订单",
                            description: _vm.orderData.createDate
                          }
                        }),
                        _vm._v(" "),
                        _c("el-step", {
                          attrs: {
                            title: "支付成功",
                            description: _vm._f("changeLD")(
                              _vm.orderData.payDate
                            )
                          }
                        }),
                        _vm._v(" "),
                        _c("el-step", {
                          attrs: {
                            title: "商家发货",
                            description: _vm._f("changeLD")(
                              _vm.orderData.deliveryDate
                            )
                          }
                        }),
                        _vm._v(" "),
                        _c("el-step", {
                          attrs: {
                            title: "用户收货",
                            description: _vm._f("changeLD")(
                              _vm.orderData.receiveDate
                            )
                          }
                        })
                      ],
                      1
                    )
              ],
              1
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-row",
        { staticStyle: { "margin-bottom": "20px" }, attrs: { gutter: 20 } },
        [
          _c("el-col", { attrs: { span: _vm.logistics_flag == 1 ? 8 : 12 } }, [
            _c("div", { staticClass: "content" }, [
              _c("div", { staticClass: "content-title" }, [
                _vm._v("收货人信息")
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "detail_gray",
                  staticStyle: { "margin-bottom": "20px" }
                },
                [
                  _vm._v(
                    "购买用户：" +
                      _vm._s(_vm._f("changeLD")(_vm.userDeliveryAddress.name))
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "detail_gray",
                  staticStyle: { "margin-bottom": "20px" }
                },
                [_vm._v("电话：" + _vm._s(_vm.userDeliveryAddress.tel))]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "detail_gray" }, [
                _vm._v("地址：" + _vm._s(_vm.address))
              ])
            ])
          ]),
          _vm._v(" "),
          _c("el-col", { attrs: { span: _vm.logistics_flag == 1 ? 8 : 12 } }, [
            _c("div", { staticClass: "content" }, [
              _c("div", { staticClass: "content-title" }, [_vm._v("付款信息")]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "detail_gray",
                  staticStyle: { "margin-bottom": "20px" }
                },
                [
                  _vm._v(
                    "支付状态：" +
                      _vm._s(_vm._f("changePayStatus")(_vm.orderData.payStatus))
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "detail_gray",
                  staticStyle: { "margin-bottom": "20px" }
                },
                [_vm._v("支付金额(元)：" + _vm._s(_vm.orderData.cash))]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "detail_gray" }, [
                _vm._v("支付流水号：" + _vm._s(_vm.orderData.transactionNo))
              ])
            ])
          ]),
          _vm._v(" "),
          _vm.logistics_flag
            ? _c("el-col", { attrs: { span: 8 } }, [
                _c("div", { staticClass: "content" }, [
                  _c("div", { staticClass: "content-title" }, [
                    _vm._v("物流信息")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "detail_gray",
                      staticStyle: { "margin-bottom": "20px" }
                    },
                    [
                      _vm._v(
                        "承运物流：" +
                          _vm._s(
                            _vm.orderData.orderDetailsDeliveryDto.deliveryName
                          )
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "detail_gray",
                      staticStyle: { "margin-bottom": "20px" }
                    },
                    [
                      _vm._v(
                        "运单编号：" +
                          _vm._s(
                            _vm.orderData.orderDetailsDeliveryDto.logisticsSn
                          )
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "detail_gray" }, [
                    _vm._v(
                      "状态：" +
                        _vm._s(
                          _vm._f("changeLD")(
                            _vm.currentRecord.currentRecordDate
                          )
                        ) +
                        "\n            "
                    ),
                    _c("span", { staticClass: "orange-txt" }, [
                      _vm._v(
                        "\n              " +
                          _vm._s(
                            _vm._f("changeLD")(
                              _vm.currentRecord.currentRecordAddress
                            )
                          ) +
                          "\n            "
                      )
                    ])
                  ])
                ])
              ])
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content", attrs: { id: "print_goods" } },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品信息")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "singleTable",
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "detail", label: "商品名称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsCode", label: "商品编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "商品图片" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          attrs: {
                            src: scope.row.skuImageUrl,
                            alt: "",
                            width: "52",
                            height: "52"
                          }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "skuSellingPrice", label: "商品价格(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsNumber", label: "商品数量" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "totalAmount", label: "商品总额(元)" }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { "margin-top": "20px", overflow: "hidden" } },
            [
              _c(
                "div",
                { staticClass: "pagination" },
                [
                  _c(
                    "el-row",
                    {
                      staticClass: "detail_gray",
                      staticStyle: { "margin-bottom": "20px" }
                    },
                    [
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v("商品总价(元)：")
                      ]),
                      _vm._v(" "),
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v(_vm._s(_vm.orderData.actualAmount))
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    {
                      staticClass: "detail_gray",
                      staticStyle: { "margin-bottom": "20px" }
                    },
                    [
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v("运费(元)：")
                      ]),
                      _vm._v(" "),
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v(_vm._s(_vm.orderData.logisticsAmount))
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    {
                      staticClass: "detail_gray",
                      staticStyle: {
                        "margin-bottom": "20px",
                        "font-weight": "700"
                      }
                    },
                    [
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v("订单总价(元)：")
                      ]),
                      _vm._v(" "),
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v(_vm._s(_vm.orderData.payableAmount))
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    {
                      staticClass: "detail_gray",
                      staticStyle: { "font-weight": "700" }
                    },
                    [
                      _c("el-col", { staticClass: "text_right" }, [
                        _vm._v("实付款(元)：")
                      ]),
                      _vm._v(" "),
                      _c("el-col", { staticClass: "text_right orange-txt" }, [
                        _vm._v(_vm._s(_vm.orderData.cash))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ]
          )
        ],
        1
      ),
      _vm._v(" "),
      this.$route.params.flag == 2
        ? _c(
            "div",
            { staticClass: "content mt20" },
            [
              _c("div", { staticClass: "content-title" }, [
                _vm._v("填写发货信息")
              ]),
              _vm._v(" "),
              _c(
                "el-form",
                {
                  ref: _vm.formData,
                  attrs: {
                    model: _vm.formData,
                    rules: _vm.rules,
                    "label-width": "100px"
                  }
                },
                [
                  _c(
                    "el-row",
                    [
                      _c(
                        "el-col",
                        { attrs: { span: 8 } },
                        [
                          _c(
                            "el-form-item",
                            { attrs: { label: "承运物流", prop: "val1" } },
                            [
                              _c(
                                "el-select",
                                {
                                  attrs: {
                                    filterable: "",
                                    placeholder: "请选择物流"
                                  },
                                  on: { change: _vm.selectVal },
                                  model: {
                                    value: _vm.formData.val1,
                                    callback: function($$v) {
                                      _vm.$set(_vm.formData, "val1", $$v)
                                    },
                                    expression: "formData.val1"
                                  }
                                },
                                _vm._l(_vm.options, function(item) {
                                  return _c("el-option", {
                                    key: item.value,
                                    attrs: {
                                      label: item.label,
                                      value: item.value
                                    }
                                  })
                                })
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-row",
                    [
                      _c(
                        "el-col",
                        { attrs: { span: 8 } },
                        [
                          _c(
                            "el-form-item",
                            { attrs: { label: "运单编号", prop: "val2" } },
                            [
                              _c("el-input", {
                                nativeOn: {
                                  keyup: function($event) {
                                    if (
                                      !("button" in $event) &&
                                      _vm._k(
                                        $event.keyCode,
                                        "enter",
                                        13,
                                        $event.key
                                      )
                                    ) {
                                      return null
                                    }
                                    _vm.searchVal($event)
                                  }
                                },
                                model: {
                                  value: _vm.formData.val2,
                                  callback: function($$v) {
                                    _vm.$set(_vm.formData, "val2", $$v)
                                  },
                                  expression: "formData.val2"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      this.$route.params.flag == 2
        ? _c(
            "el-button",
            { staticClass: "purple-bg mt20", on: { click: _vm.delivery } },
            [_vm._v("发货")]
          )
        : _vm._e(),
      _vm._v(" "),
      this.$route.params.flag == 2 || this.$route.params.flag == 3
        ? _c(
            "el-button",
            {
              staticClass: "mt20",
              staticStyle: { color: "#4d6cf3", border: "1px solid #4d6cf3" },
              on: { click: _vm.printGoods }
            },
            [_vm._v("打印商品清单")]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.goBack } },
        [_vm._v("返回")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c558374c", esExports)
  }
}

/***/ })

});