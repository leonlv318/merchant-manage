webpackJsonp([33],{

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6378b8c4_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_CheckCarousel_vue__ = __webpack_require__(412);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(410)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6378b8c4"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckCarousel_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6378b8c4_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_CheckCarousel_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\carousel\\CheckCarousel.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6378b8c4", Component.options)
  } else {
    hotAPI.reload("data-v-6378b8c4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(95);

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    }
  },
  data: function data() {
    return {
      imgApi: this.$imgApi, //图片上传api
      carouselDetail: {}, //获取广告的详情
      upImgs: [],
      keepTime: [],
      getGoodsIds: [],
      multipleSelection: [],
      val1: "",
      val2: "",
      val3: "",
      ruleForm: {
        title: "",
        imgUrl: "",
        jumpType: "",
        jumpTypeParam: "",
        sort: 999,
        beginUseTime: "",
        endUseTime: "",
        goodsIds: ""
      },
      rules: {
        title: [{ required: true, message: "请输入广告标题", trigger: "blur" }],
        sort: [{ required: true, message: "请输入排序号", trigger: "blur" }, {
          pattern: /^\d+$/,
          message: "请输入数字",
          trigger: "blur"
        }]
      },
      value8: "",
      tableData: [],
      query: {
        pageNum: 1,
        pageSize: 999,
        name: "",
        goodsCategoryId: "",
        isUp: "",
        goodsAudit: "",
        storeAdImageId: "",
        adImageId: ""
      }
    };
  },

  methods: {
    /**@function 获取广告详情 */
    getCarouselDetail: function getCarouselDetail() {
      var _this = this;

      var id = this.$route.params.id;
      this.$http.get(this.$api.findStoreAdImage, {
        params: { adImageId: this.$route.params.id }
      }).then(function (rsp) {
        console.log("获取广告详情");
        console.log(rsp.data);
        _this.carouselDetail = rsp.data.data;
        var upImgsObj = {};
        upImgsObj.name = upImgsObj.url = _this.carouselDetail.imgUrl;
        _this.upImgs.push(upImgsObj);
        _this.ruleForm.imgUrl = _this.carouselDetail.imgUrl; //图片存入ruleForm
        _this.ruleForm.title = _this.carouselDetail.title;
        _this.ruleForm.sort = _this.carouselDetail.sort;
        _this.keepTime.push(_this.carouselDetail.beginUseTime);
        _this.keepTime.push(_this.carouselDetail.endUseTime);
      });
    },

    /**@function 获取广告商品列表 */
    getGoodsList: function getGoodsList() {
      var _this2 = this;

      this.query.storeAdImageId = this.$route.params.id;
      this.$http.get(this.$api.findGoodsListPage, { params: this.query }).then(function (rsp) {
        console.log("获取广告商品列表");
        console.log(rsp.data);
        _this2.tableData = rsp.data.data.list;
        //   this.total = rsp.data.data.total;
      });
    },

    /**@function 查看 */
    // checkGoods(row) {
    //   console.log("查看商品");
    //   console.log(row);
    //   this.$router.push({
    //     name: "gc",
    //     params: { id: row.goodsId }
    //   });
    // },
    /**@function 删除当前行 */
    delGoods: function delGoods(row) {
      var _this3 = this;

      console.log(row.goodsId);
      this.$confirm("是否删除？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        var params = {};
        params.adImageId = _this3.$route.params.id;
        params.goodsIds = row.goodsId;
        _this3.$http.get(_this3.$api.delStoreAdImageGoodsBatch, { params: params }).then(function (rsp) {
          _this3.$isot(rsp); //登录超时处理函数
          if (rsp.data.flag) {
            _this3.$message({
              type: "warning",
              message: "删除成功!"
            });
            _this3.getGoodsList();
          }
        });
      }).catch(function () {
        _this3.$message({
          type: "info",
          message: "已取消删除操作"
        });
      });
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 图片 */
    handleRemove: function handleRemove(file, fileList) {
      console.log(file, fileList);
    },

    /**@function 控制上传文件规格 */
    beforeAvatarUpload: function beforeAvatarUpload(file) {
      console.log("控制上传文件规格");
      console.log(file);
      var isType = file.type === "image/jpeg" || "image/png";
      var isLt = file.size / 1024 / 1024 < 2;
      if (!isType) {
        this.$message.error("上传图片只能是 JPG 或者 PNG 格式!");
      }
      if (!isLt) {
        this.$message.error("上传图片大小不能超过 2MB!");
      }
      return isType && isLt;
    },

    /**@function 图片上传成功事件 */
    uploadImgHandler: function uploadImgHandler(data) {
      console.log("图片上传成功事件");
      // console.log(data);
      this.ruleForm.imgUrl = data.data[0].fileUrl;
      console.log(this.ruleForm.imgUrl);
    },

    /**@function 保存 */
    save: function save() {
      var _this4 = this;

      var params = {};

      params.adImageId = this.carouselDetail.adImageId;
      params.title = this.ruleForm.title;
      params.imgUrl = this.ruleForm.imgUrl;
      params.jumpType = 2;
      params.sort = this.ruleForm.sort;
      params.beginUseTime = this.keepTime[0];
      params.endUseTime = this.keepTime[1].substring(0, 10) + " 23:59:59";

      console.log("保存");
      console.log(params);
      var formDataToJson = (0, _stringify2.default)(params);
      this.$http.post(this.$api.updateStoreAdImage, formDataToJson).then(function (rsp) {
        _this4.$isot(rsp); //登录超时处理函数
        if (rsp.data.flag) {
          console.log(rsp.data);
          _this4.$message.success("保存成功！");
          //  保存成功后跳转
          _this4.$router.go(-1);
        } else {
          _this4.$message.error("数据未填写完毕或出错，请重新检查！");
        }
      });
    },

    /**@function 取消 */
    cancle: function cancle() {
      var _this5 = this;

      this.$confirm("取消后将直接返回上一页面，请确认是否取消？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        _this5.$message({
          type: "success",
          message: "取消成功!"
        });
        _this5.$router.go(-1);
      }).catch(function () {
        _this5.$message({
          type: "info",
          message: "放弃 [取消] 操作!"
        });
      });
    },

    /**@function 根据商品ID添加*/
    getGoodsById: function getGoodsById(id) {
      var _this6 = this;

      var params = {};
      params.storeAdImageId = this.$route.params.id;
      params.goodsId = id;
      this.$http.get(this.$api.addStoreAdImageOneGoods, { params: params }).then(function (rsp) {
        _this6.$isot(rsp); //登录超时处理函数
        if (rsp.data.flag) {
          _this6.$message({
            type: "success",
            message: "添加成功!"
          });
          _this6.getGoodsList();
        }
      });
    },

    /**@function 根据编号获取商品ID，再根据ID添加商品 */
    addGoods: function addGoods() {
      this.$router.push({
        name: "cmad",
        params: { adImageId: this.$route.params.id }
      });
      // console.log(this.val1);
      // this.$http(
      //   this.$api.findGoodsByGoodsCode + `?goodsCode=${this.val1}`
      // ).then(rsp => {
      //   this.$isot(rsp); //登录超时处理函数
      //   console.log("添加商品");
      //   console.log(rsp.data);
      //   // console.log(rsp.data.data[0]);
      //   if (!rsp.data.data[0]) {
      //     this.$message.error("商品编号错误！");
      //     return false;
      //   }
      //   let id = rsp.data.data[0];
      //   //判断商品是已添加过
      //   if (this.getGoodsIds.indexOf(id) != -1) {
      //     this.$message.error("该商品已在表中！");
      //   } else {
      //     this.getGoodsById(id);
      //     //   this.getGoodsIds.push(id);
      //   }
      // });
      // //   商品编号错误则$message提示
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    },

    /**@function 搜索val2 */
    searchVal2: function searchVal2() {
      console.log(this.val2);
    },

    /**@function 选择val3 */
    selectVal3: function selectVal3() {
      console.log(this.val3);
    },

    /**@function 批量删除 */
    delSelected: function delSelected() {
      var _this7 = this;

      if (this.multipleSelection.length != 0) {
        this.$confirm("是否批量删除？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          console.log("批量删除");
          console.log(_this7.multipleSelection);
          var goodsIds = "";
          _this7.multipleSelection.forEach(function (o, i) {
            goodsIds = o.goodsId + "," + goodsIds;
          });
          _this7.$http.get(_this7.$api.delStoreAdImageGoodsBatch, {
            params: {
              goodsIds: goodsIds,
              adImageId: _this7.$route.params.id
            }
          }).then(function (rsp) {
            _this7.$isot(rsp); //登录超时处理函数
            if (rsp.data.flag) {
              _this7.$message({
                type: "warning",
                message: "删除成功!"
              });
              _this7.getGoodsList();
            }
          });
        }).catch(function () {
          _this7.$message({
            type: "info",
            message: "已取消删除操作"
          });
        });
      } else {
        this.$message({
          type: "error",
          message: "当前未选中任何行！"
        });
      }
    }
  },
  created: function created() {
    this.getCarouselDetail();
    this.getGoodsList();
  }
};

/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(411);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("2741f832", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6378b8c4\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CheckCarousel.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6378b8c4\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CheckCarousel.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 411:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-6378b8c4],\n.search-group[data-v-6378b8c4] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-6378b8c4] {\n  float: right;\n}\n.content .content-title[data-v-6378b8c4] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("轮播图管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1 addcarousel" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("填写轮播图信息")
          ]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "ruleForm",
              attrs: {
                model: _vm.ruleForm,
                rules: _vm.rules,
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "轮播图标题", prop: "title" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.title,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.ruleForm,
                                  "title",
                                  typeof $$v === "string" ? $$v.trim() : $$v
                                )
                              },
                              expression: "ruleForm.title"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                {
                  staticClass: "addcarousel-pic-input",
                  staticStyle: { height: "200px" }
                },
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "轮播图图片" } },
                        [
                          _c(
                            "el-upload",
                            {
                              attrs: {
                                action: _vm.imgApi,
                                "list-type": "picture-card",
                                "before-upload": _vm.beforeAvatarUpload,
                                "on-success": _vm.uploadImgHandler,
                                "file-list": _vm.upImgs,
                                "on-remove": _vm.handleRemove,
                                limit: 1
                              }
                            },
                            [
                              _c("i", { staticClass: "el-icon-plus" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "el-upload__tip orange-txt",
                                  attrs: { slot: "tip" },
                                  slot: "tip"
                                },
                                [
                                  _vm._v(
                                    "**图片尺寸：750*320px；格式:jpg、png；大小：200KB以内"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-form-item",
                    { attrs: { label: "持续时间" } },
                    [
                      _c(
                        "el-col",
                        { attrs: { span: 8 } },
                        [
                          _c("el-date-picker", {
                            attrs: {
                              type: "daterange",
                              "range-separator": "至",
                              "start-placeholder": "轮播图开始日期",
                              "end-placeholder": "轮播图结束日期",
                              "value-format": "yyyy-MM-dd HH:mm:ss"
                            },
                            model: {
                              value: _vm.keepTime,
                              callback: function($$v) {
                                _vm.keepTime = $$v
                              },
                              expression: "keepTime"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号", prop: "sort" } },
                        [
                          _c("el-input", {
                            attrs: { type: "number" },
                            model: {
                              value: _vm.ruleForm.sort,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "sort", $$v)
                              },
                              expression: "ruleForm.sort"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-button",
            { staticClass: "purple-bg", on: { click: _vm.save } },
            [_vm._v("保存")]
          ),
          _vm._v(" "),
          _c(
            "el-button",
            { staticClass: "orange-bg", on: { click: _vm.cancle } },
            [_vm._v("取消")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("编辑轮播图跳转商品列表")
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "search-group" },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  attrs: { icon: "el-icon-plus" },
                  on: { click: _vm.addGoods }
                },
                [_vm._v("添加")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "multipleTable",
              staticStyle: { width: "100%" },
              attrs: {
                data: _vm.tableData,
                "tooltip-effect": "dark",
                "default-sort": { prop: "brand", order: "descending" }
              },
              on: { "selection-change": _vm.handleSelectionChange }
            },
            [
              _c("el-table-column", { attrs: { type: "selection" } }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsName", label: "名称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsCode", label: "编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "图片", width: "70" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          attrs: {
                            src: scope.row.imageUrl,
                            width: "52",
                            height: "52"
                          }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsBrandName", label: "品牌" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "categoryName", label: "分类" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "isUp", label: "销售状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n            " +
                            _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                            "\n          "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "orange-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.delGoods(scope.row)
                              }
                            }
                          },
                          [_vm._v("删除")]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticStyle: { "margin-top": "20px" } })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg", on: { click: _vm.delSelected } },
        [_vm._v("批量删除")]
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.goBack } }, [
        _vm._v("返回")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6378b8c4", esExports)
  }
}

/***/ })

});