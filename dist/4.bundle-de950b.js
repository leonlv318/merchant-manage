webpackJsonp([4],{

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_409524bf_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Logistics_vue__ = __webpack_require__(451);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(449)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-409524bf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Logistics_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_409524bf_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Logistics_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\shop\\Logistics.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-409524bf", Component.options)
  } else {
    hotAPI.reload("data-v-409524bf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeStatus: function changeStatus(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    }
  },
  data: function data() {
    return {
      options: [{
        value: "1",
        label: "启用"
      }, {
        value: "0",
        label: "停用"
      }],
      val1: "",
      val2: "",
      tableData: [],
      total: 0,
      pages: 0,
      multipleSelection: [],
      query: {
        pageNum: 1,
        pageSize: 20,
        logisticsName: "",
        logisticsStatus: ""
      }
    };
  },

  methods: {
    /**@function 获取物流列表 */
    getLogistics: function getLogistics() {
      var _this = this;

      this.$http.get(this.$api.findStoreLogisticsListPage, { params: this.query }).then(function (rsp) {
        _this.$isot(rsp); //登录超时处理函数
        console.log("获取物流列表");
        console.log(rsp.data);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 切换启用停用 */
    toggle: function toggle(row, e) {
      var _this2 = this;

      console.log(row);
      if (row.status == "1") {
        this.$confirm("是否停用物流？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          console.log("停用物流");
          params.logisticsConfId = row.logisticsConfId;
          _this2.$http.get(_this2.$api.delStoreLogistics, {
            params: params
          }).then(function (rsp) {
            _this2.$isot(rsp); //登录超时处理函数
            _this2.getLogistics();
          });
          // this.getLogistics();
          _this2.$message({
            type: "warning",
            message: "停用物流成功!"
          });
        }).catch(function () {
          _this2.$message({
            type: "info",
            message: "已取消 [停用] 操作"
          });
        });
      } else {
        var params = {};
        params.logisticsConfId = row.logisticsConfId;
        this.$http.get(this.$api.addStoreLogistics, { params: params }).then(function (rsp) {
          _this2.$isot(rsp); //登录超时处理函数
          _this2.getLogistics();
        });
        this.$message({
          type: "success",
          message: "启用物流成功!"
        });
      }
      //   if (row.status == "1") {
      //     this.$message({
      //       message: "停用成功！",
      //       type: "warning"
      //     });

      //     row.status = "停用";
      //   } else {
      //     this.$message({
      //       message: "启用成功。",
      //       type: "success"
      //     });
      //     row.status = "启用";
      //   }
    },

    /**@function 搜索 */
    searchVal1: function searchVal1() {
      this.query.logisticsName = this.val1;
      this.getLogistics();
    },

    /**@function 搜索 */
    selectVal2: function selectVal2() {
      this.query.logisticsStatus = this.val2;
      this.getLogistics();
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    }
  },
  created: function created() {
    this.getLogistics();
  }
};

/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(450);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("f9a5f9ba", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-409524bf\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Logistics.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-409524bf\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Logistics.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 450:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-409524bf],\n.search-group[data-v-409524bf] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-409524bf] {\n  float: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-input", {
              staticClass: "search-input",
              attrs: {
                placeholder: "搜索物流名称",
                "prefix-icon": "el-icon-search",
                clearable: ""
              },
              on: { blur: _vm.searchVal1 },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal1($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择物流状态"
                },
                on: { change: _vm.selectVal2 },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              },
              _vm._l(_vm.options, function(item) {
                return _c("el-option", {
                  key: item.value,
                  staticClass: "search-select",
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "singleTable",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" },
            on: { "selection-change": _vm.handleSelectionChange }
          },
          [
            _c("el-table-column", { attrs: { type: "index" } }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "logisticsName", label: "物流名称" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "tel", label: "查询电话" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "logisticsName", label: "官方网址" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "status", label: "物流状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(_vm._f("changeStatus")(scope.row.status)) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作", prop: "status" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      scope.row.status != "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "purple-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("启用")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      scope.row.status == "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "orange-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("停用")]
                          )
                        : _vm._e()
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticStyle: { "margin-top": "20px" } },
          [
            _c("el-button", { attrs: { type: "text", disabled: "" } }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pagination" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.query.pageNum,
                    "page-sizes": [20, 50, 100, 200],
                    "page-size": _vm.query.pageSize,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-409524bf", esExports)
  }
}

/***/ })

});