webpackJsonp([9],{

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0db0c725_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_goods_vue__ = __webpack_require__(463);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(461)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0db0c725"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0db0c725_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_goods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\goods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0db0c725", Component.options)
  } else {
    hotAPI.reload("data-v-0db0c725", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    },
    changeGoodsAudit: function changeGoodsAudit(value) {
      if (value == 2) {
        return "通过";
      } else if (value == 3) {
        return "驳回";
      } else {
        return "未审核";
      }
    }
  },
  data: function data() {
    return {
      val1: "", //搜索商品名称/编号/品牌
      val2: "", //选择店铺
      val3: [], //选择分类
      val4: "", //选择销售状态
      val5: "", //选择审核状态
      multipleSelection: [],
      options3: [
        //分类选项
      ],
      options4: [
      //销售状态选项
      {
        value: "0",
        label: "下架"
      }, {
        value: "1",
        label: "上架"
      }],
      options5: [
      //审核状态选项
      {
        value: "1",
        label: "未审核"
      }, {
        value: "2",
        label: "通过"
      }, {
        value: "3",
        label: "驳回"
      }],
      total: 0,
      pages: 0,
      query: {
        pageNum: 1,
        pageSize: 20,
        name: "",
        goodsCategoryId: "",
        isUp: "",
        goodsAudit: "",
        orderByRecommendCount: "",
        orderByClickCount: "",
        orderBySettleCount: "",
        orderByDealAmount: "",
        orderByCommissionAmount: ""
      },
      tableData: []
    };
  },

  methods: {
    /**@function 获取商品列表 */
    getGoodsData: function getGoodsData() {
      var _this = this;

      this.$http.get(this.$api.findChannelGoodsListPage, { params: this.query }).then(function (rsp) {
        console.log("获取商品列表");
        console.log(rsp.data);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findGoodsCategoryList).then(function (rsp) {
        var orData = rsp.data.data;
        var toget = function toget(orData) {
          var data = orData;
          var arrl = orData.length;
          var arr = [];
          for (var i = 0; i < arrl; i++) {
            //一级
            var obj1 = {};
            obj1.value = data[i].goodsCategoryId;
            obj1.label = data[i].goodsCategoryName;
            if (data[i].childCategoryList) {
              //是否有2级
              obj1.childrentemp = data[i].childCategoryList;
              obj1.children = [];
              for (var j = 0; j < obj1.childrentemp.length; j++) {
                var obj2 = {};
                obj2.children = [];
                obj2.value = obj1.childrentemp[j].goodsCategoryId;
                obj2.label = obj1.childrentemp[j].goodsCategoryName;
                obj1.children.push(obj2);
                // obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                if (obj1.childrentemp[j].childCategoryList) {
                  //是否有3级
                  obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                  for (var k = 0; k < obj2.childrentemp.length; k++) {
                    var obj3 = {};
                    obj3.value = obj2.childrentemp[k].goodsCategoryId;
                    obj3.label = obj2.childrentemp[k].goodsCategoryName;
                    obj2.children.push(obj3);
                  }
                }
              }
              arr[i] = obj1;
            } else {
              arr[i] = obj1;
            }
          }
          return arr;
        };
        var arr = toget(orData);
        console.log("生成分类数据");
        console.log(arr);
        _this2.options3 = arr;
      });
    },


    /**@function 新增商品 */
    addGoods: function addGoods() {
      this.$router.push({
        name: "raa",
        params: {}
      });
    },

    /**@function 搜索商品名称/编号/品牌 */
    searchVal1: function searchVal1() {
      this.query.name = this.val1;
      this.getGoodsData();
    },


    /**@function 选择分类 */
    selectVal3: function selectVal3() {
      console.log(this.val3);
      var l = this.val3.length;
      this.query.goodsCategoryId = this.val3[l - 1];
      this.getGoodsData();
    },

    /**@function 选择销售状态 */
    selectVal4: function selectVal4() {
      this.query.isUp = this.val4;
      this.getGoodsData();
    },

    /**@function 选择审核状态 */
    selectVal5: function selectVal5() {
      this.query.goodsAudit = this.val5;
      this.getGoodsData();
    },

    /**@function 上架下架 */
    toggleSalesStatus: function toggleSalesStatus(row, e) {
      var _this3 = this;

      console.log(row);
      if (row.isUp == "1") {
        this.$confirm("是否下架该商品？(注意：下架后商品将无法推荐!)", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          console.log("下架商品");
          params.channelGoodsIds = row.channelGoodsId;
          params.channelGoodsStatus = 0;
          _this3.$http.get(_this3.$api.updateChannelGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            if (rsp.data.flag) {
              _this3.getGoodsData();
              _this3.$message({
                type: "warning",
                message: "下架商品成功!"
              });
            }
          });
        }).catch(function () {
          _this3.$message({
            type: "info",
            message: "已取消 [下架] 操作"
          });
        });
      } else {
        var params = {};
        console.log("上架商品");
        params.channelGoodsIds = row.channelGoodsId;
        params.channelGoodsStatus = 1;
        this.$http.get(this.$api.updateChannelGoodsIsUp, {
          params: params
        }).then(function (rsp) {
          if (rsp.data.flag) {
            _this3.getGoodsData();
            _this3.$message({
              type: "warning",
              message: "上架商品成功!"
            });
          }
        });
      }
    },

    /**@function 批量上架 */
    upSelect: function upSelect() {
      var _this4 = this;

      console.log(this.multipleSelection);
      var flag = true;
      this.multipleSelection.forEach(function (v, i) {
        if (v.goodsAudit != 2) {
          flag = false;
        }
      });
      if (this.multipleSelection.length && flag) {
        this.$confirm("是否上架选中商品？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          params.channelGoodsIds = [];
          console.log("上架选中商品");
          for (var i = 0; i < _this4.multipleSelection.length; i++) {
            params.channelGoodsIds.push(_this4.multipleSelection[i].channelGoodsId);
          }
          params.channelGoodsIds = params.channelGoodsIds.join(",");
          params.channelGoodsStatus = 1;
          _this4.$http.get(_this4.$api.updateChannelGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            if (rsp.data.flag) {
              _this4.getGoodsData();
              _this4.$message({
                type: "success",
                message: "上架选中商品成功!"
              });
            }
          });
        }).catch(function () {
          _this4.$message({
            type: "info",
            message: "已取消 [上架] 操作"
          });
        });
      } else {
        if (!flag) {
          this.$message({
            type: "error",
            message: "所选的商品审核状态必须为通过！"
          });
        } else {
          this.$message({
            type: "error",
            message: "当前未选中任何行！"
          });
        }
      }
    },

    /**@function 批量下架 */
    downSelect: function downSelect() {
      var _this5 = this;

      console.log(this.multipleSelection);
      var flag = true;
      this.multipleSelection.forEach(function (v, i) {
        if (v.goodsAudit != 2) {
          flag = false;
        }
      });
      if (this.multipleSelection.length && flag) {
        this.$confirm("是否下架选中商品？(注意：下架后商品将无法推荐!)", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          params.channelGoodsIds = [];
          console.log("下架选中商品");
          for (var i = 0; i < _this5.multipleSelection.length; i++) {
            params.channelGoodsIds.push(_this5.multipleSelection[i].channelGoodsId);
          }
          params.channelGoodsIds = params.channelGoodsIds.join(",");
          params.channelGoodsStatus = 0;
          _this5.$http.get(_this5.$api.updateChannelGoodsIsUp, {
            params: params
          }).then(function (rsp) {
            if (rsp.data.flag) {
              _this5.getGoodsData();
              _this5.$message({
                type: "warning",
                message: "下架选中商品成功!"
              });
            }
          });
        }).catch(function () {
          _this5.$message({
            type: "info",
            message: "已取消 [下架]操作"
          });
        });
      } else {
        if (!flag) {
          this.$message({
            type: "error",
            message: "所选的商品审核状态必须为通过！"
          });
        } else {
          this.$message({
            type: "error",
            message: "当前未选中任何行！"
          });
        }
      }
    },

    /**@function 查看商品 */
    checkgoods: function checkgoods(row, $index) {
      this.$router.push({
        name: "rae",
        params: {
          channelGoodsId: row.channelGoodsId
        }
      });
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    }
  },
  created: function created() {
    this.getGoodsData();
    this.getCategoryData();
  }
};

/***/ }),

/***/ 461:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(462);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("fab106b6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0db0c725\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0db0c725\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 462:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-0db0c725],\n.search-group[data-v-0db0c725] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-0db0c725] {\n  float: right;\n}\n.up-btn[data-v-0db0c725] {\n  position: relative;\n}\n.file-upload[data-v-0db0c725] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n.select_width .el-cascader[data-v-0db0c725] {\n  width: 240px;\n}\n", ""]);

// exports


/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "el-button",
          {
            staticClass: "purple-bg mb20",
            attrs: { icon: "el-icon-plus" },
            on: { click: _vm.addGoods }
          },
          [_vm._v("新增商品")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group select_width" },
          [
            _c("el-input", {
              staticClass: "search-input mb10",
              attrs: {
                clearable: "",
                placeholder: "搜索商品名称/编号/品牌",
                "prefix-icon": "el-icon-search"
              },
              on: { blur: _vm.searchVal1 },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal1($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c("el-cascader", {
              staticClass: "mb10",
              attrs: {
                clearable: "",
                placeholder: "选择分类",
                "expand-trigger": "click",
                options: _vm.options3,
                "change-on-select": ""
              },
              on: { change: _vm.selectVal3 },
              model: {
                value: _vm.val3,
                callback: function($$v) {
                  _vm.val3 = $$v
                },
                expression: "val3"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择推荐状态"
                },
                on: { change: _vm.selectVal4 },
                model: {
                  value: _vm.val4,
                  callback: function($$v) {
                    _vm.val4 = $$v
                  },
                  expression: "val4"
                }
              },
              _vm._l(_vm.options4, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            ),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择审核状态"
                },
                on: { change: _vm.selectVal5 },
                model: {
                  value: _vm.val5,
                  callback: function($$v) {
                    _vm.val5 = $$v
                  },
                  expression: "val5"
                }
              },
              _vm._l(_vm.options5, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "multipleTable",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" },
            on: { "selection-change": _vm.handleSelectionChange }
          },
          [
            _c("el-table-column", {
              attrs: { type: "selection", width: "55" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsName", label: "商品名称" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsCode", label: "商品编码" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "sellingPrice", label: "商品价格(元)" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "goodsRecommendCount",
                label: "总推荐数",
                sortable: ""
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "goodsClickCount",
                label: "总点击数",
                sortable: ""
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "goodsSettleCount",
                label: "总结算数",
                sortable: ""
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "grossDealAmount",
                label: "总销售额(元)",
                sortable: ""
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "grossCommisionAmount",
                label: "总产生佣金(元)",
                sortable: ""
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "isUp", label: "推荐状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsAudit", label: "审核状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(
                            _vm._f("changeGoodsAudit")(scope.row.goodsAudit)
                          ) +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作", width: "140" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.checkgoods(scope.row, scope.$index)
                            }
                          }
                        },
                        [_vm._v("查看")]
                      ),
                      _vm._v(" "),
                      scope.row.isUp == "0" && scope.row.goodsAudit == "2"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "purple-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggleSalesStatus(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("上架")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      scope.row.isUp == "1" && scope.row.goodsAudit == "2"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "orange-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggleSalesStatus(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("下架")]
                          )
                        : _vm._e()
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "mt20" },
          [
            _c(
              "el-button",
              { staticClass: "purple-bg", on: { click: _vm.upSelect } },
              [_vm._v("批量上架")]
            ),
            _vm._v(" "),
            _c(
              "el-button",
              { staticClass: "orange-bg", on: { click: _vm.downSelect } },
              [_vm._v("批量下架")]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "fr" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.query.pageNum,
                    "page-sizes": [20, 50, 100, 200],
                    "page-size": _vm.query.pageSize,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0db0c725", esExports)
  }
}

/***/ })

});