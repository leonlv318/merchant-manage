webpackJsonp([8],{

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3282b90c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_recommendation_vue__ = __webpack_require__(454);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(452)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3282b90c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendation_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3282b90c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_recommendation_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\recommendation.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3282b90c", Component.options)
  } else {
    hotAPI.reload("data-v-3282b90c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeOrganization: function changeOrganization(value) {
      if (value == null) {
        return "无";
      } else {
        return value;
      }
    },
    changeOrganizationType: function changeOrganizationType(value) {
      if (value == 1) {
        return "用户";
      } else if (value == 2) {
        return "群";
      } else if (value == 3) {
        return "二维码";
      } else {
        return "未获取";
      }
    }
  },
  data: function data() {
    return {
      val1: "", //搜索用户/用户编号/商品/商品编号
      val2: "", //选择机构
      val4: "", //选择佣金等级
      val5: "", //选择日期
      options2: [
        //机构选项
      ],
      options4: [
        //佣金等级选项
      ],
      total: 0,
      pages: 0,
      query: {
        pageNum: 1,
        pageSize: 20,
        commissionRateConfId: "",
        searchName: "",
        channelOrganizationId: "",
        beginTime: "",
        endTime: "",
        orderByGoodsClickCount: "",
        orderByGrossDealCount: "",
        orderByPreDealAmount: "",
        orderByPreSettleAmount: "",
        orderByCommisionRate: "",
        orderByCreateTime: ""
      },
      tableData: []
    };
  },

  methods: {
    /**@function 获取导购列表 */
    getData: function getData() {
      var _this = this;

      this.$http.get(this.$api.findChannelRecordList, { params: this.query }).then(function (rsp) {
        console.log("获取商品列表");
        console.log(rsp.data);
        if (rsp.data.data) {
          _this.tableData = rsp.data.data.list;
          _this.total = rsp.data.data.total;
        } else {
          _this.tableData = [];
          _this.total = 0;
        }
      });
    },

    /**@function 获取机构下拉列表 */
    getOrganization: function getOrganization() {
      var _this2 = this;

      this.$http.get(this.$api.findAllChannelOrganization).then(function (rsp) {
        console.log("获取机构下拉列表");

        var data = rsp.data.data;
        var keys = [];
        var values = [];
        data.forEach(function (v, k) {
          values.push(v.channelOrganizationId);
          keys.push(v.channelOrganizationName);
        });
        for (var i = 0; i < keys.length; i++) {
          var obj = {};
          obj.value = values[i];
          obj.label = keys[i];
          _this2.options2.push(obj);
        }
        console.log(_this2.options2);
      });
    },

    /**@function 获取佣金等级下拉列表 */
    getCommissionLevel: function getCommissionLevel() {
      var _this3 = this;

      this.$http.get(this.$api.findChannelCommissionRateConfList).then(function (rsp) {
        console.log("获取佣金等级下拉列表");
        console.log(rsp.data);
        var data = rsp.data.data;
        var keys = [];
        var values = [];
        data.forEach(function (v, k) {
          keys.push(v.levelName);
          values.push(v.commissionRateConfId);
        });
        for (var i = 0; i < keys.length; i++) {
          var obj = {};
          obj.value = values[i];
          obj.label = keys[i];
          _this3.options4.push(obj);
        }
        console.log(_this3.options4);
      });
    },

    /**@function 搜索用户昵称/编号 */
    searchVal1: function searchVal1() {
      this.query.searchName = this.val1;
      this.getData();
    },

    /**@function 选择机构 */
    selectVal2: function selectVal2() {
      console.log("channelOrganizationId==============" + this.val2);
      this.query.channelOrganizationId = this.val2;
      this.getData();
    },

    /**@function 选择佣金等级 */
    selectVal4: function selectVal4() {
      this.query.commissionRateConfId = this.val4;
      this.getData();
    },

    /**@function 选择日期 */
    selectVal5: function selectVal5() {
      if (this.val5) {
        this.query.beginDate = this.val5[0];
        this.query.endDate = this.val5[1].substring(0, 10) + " 23:59:59";
      } else {
        this.query.beginDate = this.query.endDate = "";
      }
      this.getData();
    },

    /**@function 查看导购*/
    check: function check(row, $index) {
      console.log("row==============");
      console.log(row);
      this.$router.push({
        name: "rrd",
        params: {
          channelUserId: row.channelUserId,
          channelGoodsId: row.channelGoodsId,
          channelRecordModeStatisticsId: row.channelRecordModeStatisticsDto.channelRecordModeStatisticsId,
          userId: row.userId,
          channelRecordId: row.channelRecordId
        }
      });
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getData();
    }
  },
  created: function created() {
    this.getData();
    this.getOrganization();
    this.getCommissionLevel();
  }
};

/***/ }),

/***/ 452:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(453);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("521a96ca", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3282b90c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./recommendation.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3282b90c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./recommendation.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-3282b90c],\n.search-group[data-v-3282b90c] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-3282b90c] {\n  float: right;\n}\n.table-gray-text[data-v-3282b90c] {\n  color: #999999;\n  font-size: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐列表")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-input", {
              staticClass: "search-input mb10",
              attrs: {
                clearable: "",
                placeholder: "用户/商品/及其编号",
                "prefix-icon": "el-icon-search"
              },
              on: { blur: _vm.searchVal1 },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal1($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "search-select mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择机构"
                },
                on: { change: _vm.selectVal2 },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              },
              _vm._l(_vm.options2, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            ),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "search-select mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择佣金等级"
                },
                on: { change: _vm.selectVal4 },
                model: {
                  value: _vm.val4,
                  callback: function($$v) {
                    _vm.val4 = $$v
                  },
                  expression: "val4"
                }
              },
              _vm._l(_vm.options4, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            ),
            _vm._v(" "),
            _c("el-date-picker", {
              staticClass: "mb10",
              attrs: {
                clearable: "",
                type: "daterange",
                "range-separator": "至",
                "start-placeholder": "开始日期",
                "end-placeholder": "结束日期",
                "value-format": "yyyy-MM-dd HH:mm:ss"
              },
              on: { change: _vm.selectVal5 },
              model: {
                value: _vm.val5,
                callback: function($$v) {
                  _vm.val5 = $$v
                },
                expression: "val5"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
          },
          [
            _c("el-table-column", {
              attrs: { type: "index", label: "序号", width: "55" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "nickName", label: "推荐用户" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("div", [_vm._v(" " + _vm._s(scope.row.nickName))]),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-gray-text" }, [
                        _vm._v(
                          _vm._s(
                            _vm._f("changeOrganization")(
                              scope.row.channelUserCode
                            )
                          )
                        )
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "channelOrganizationName", label: "所属机构" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("div", [
                        _vm._v(
                          _vm._s(
                            _vm._f("changeOrganization")(
                              scope.row.channelOrganizationName
                            )
                          )
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-gray-text" }, [
                        _vm._v(
                          _vm._s(
                            _vm._f("changeOrganization")(
                              scope.row.channelOrganizationCode
                            )
                          )
                        )
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsName", label: "推荐商品" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("div", [_vm._v(_vm._s(scope.row.goodsName))]),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-gray-text" }, [
                        _vm._v(
                          _vm._s(
                            _vm._f("changeOrganization")(scope.row.goodsCode)
                          )
                        )
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "sellingPrice", label: "商品价格(元)" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "佣金比例" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("div", [
                        _vm._v(_vm._s(scope.row.commisionRate) + "%")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-gray-text" }, [
                        _vm._v(_vm._s(scope.row.levelName))
                      ])
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "modeValue", label: "推荐渠道" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n          " +
                          _vm._s(scope.row.modeValue || "未知") +
                          "\n        "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "channelRecordModeStatisticsDto.goodsClickCount",
                label: "点击数"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "channelRecordModeStatisticsDto.grossDealCount",
                label: "付款笔数"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "channelRecordModeStatisticsDto.preDealAmount",
                label: "成交预估(元)"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "channelRecordModeStatisticsDto.preSettleAmount",
                label: "结算预估(元)"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "createTime", label: "推荐时间" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.check(scope.row, scope.$index)
                            }
                          }
                        },
                        [_vm._v("查看")]
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "mt20" },
          [
            _c("el-button", { attrs: { type: "text" } }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "fr" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.query.pageNum,
                    "page-sizes": [20, 50, 100, 200],
                    "page-size": _vm.query.pageSize,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3282b90c", esExports)
  }
}

/***/ })

});