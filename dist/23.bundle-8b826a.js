webpackJsonp([23],{

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_d95de57a_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_goodsOfSort_vue__ = __webpack_require__(376);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(374)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-d95de57a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_goodsOfSort_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_d95de57a_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_goodsOfSort_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\goodsOfSort.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d95de57a", Component.options)
  } else {
    hotAPI.reload("data-v-d95de57a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _assign = __webpack_require__(96);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      query: {
        pageNum: 1,
        pageSize: 10
      },
      total: 0,
      val1: "",
      val2: [],
      val3: [],
      val4: [],
      val4Options: [],
      options2: [],
      options3: [],
      currentPage: 1,
      tableData: [],
      deletestr: ""
    };
  },

  methods: {
    // 获取分类商品
    getAsortGoods: function getAsortGoods() {
      var _this = this;

      this.$http.get(this.$api.findStoreGoodsListByChannelCategory, {
        params: this.query
      }).then(function (rsp) {
        console.log("获取分类商品");
        console.log(rsp);
        var list = [];
        for (var i in rsp.data.data.list) {
          var obj = {};
          obj = (0, _assign2.default)(rsp.data.data.list[i], rsp.data.data.list[i].storeGoodsInfo);
          list.push(obj);
        }
        _this.total = rsp.data.data.total;
        _this.tableData = list;
      });
    },

    /**@function 获取分类下拉列表 */
    getCateChanelgoryData: function getCateChanelgoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findGoodsCategoryList).then(function (rsp) {
        var orData = rsp.data.data;
        var toget = function toget(orData) {
          var data = orData;
          var arrl = orData.length;
          var arr = [];
          for (var i = 0; i < arrl; i++) {
            //一级
            var obj1 = {};
            obj1.value = data[i].goodsCategoryId;
            obj1.label = data[i].goodsCategoryName;
            if (data[i].childCategoryList) {
              //是否有2级
              obj1.childrentemp = data[i].childCategoryList;
              obj1.children = [];
              for (var j = 0; j < obj1.childrentemp.length; j++) {
                var obj2 = {};
                obj2.children = [];
                obj2.value = obj1.childrentemp[j].goodsCategoryId;
                obj2.label = obj1.childrentemp[j].goodsCategoryName;
                obj1.children.push(obj2);
                // obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                if (obj1.childrentemp[j].childCategoryList) {
                  //是否有3级
                  obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                  for (var k = 0; k < obj2.childrentemp.length; k++) {
                    var obj3 = {};
                    obj3.value = obj2.childrentemp[k].goodsCategoryId;
                    obj3.label = obj2.childrentemp[k].goodsCategoryName;
                    obj2.children.push(obj3);
                  }
                }
              }
              arr[i] = obj1;
            } else {
              arr[i] = obj1;
            }
          }
          return arr;
        };
        var arr = toget(orData);
        console.log("生成分类数据");
        console.log(arr);
        _this2.val4Options = arr;
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this3 = this;

      this.$http.get(this.$api.findStoreCategoryList, {
        params: this.query
      }).then(function (rsp) {
        console.log("获取分类下拉列表");
        console.log(rsp);
        console.log(rsp.data.data);
        var data = rsp.data.data;
        var list = [];
        for (var i in data) {
          list.push({
            label: data[i].storeCategoryName,
            value: data[i].storeCategoryId
          });
        }
        _this3.options3 = list;
      });
    },

    /**@function 搜索名称/编号/品牌 */
    searchVal1: function searchVal1() {
      console.log(this.val1);
      this.query.searchName = this.val1;
      this.getAsortGoods();
    },

    /**@function 选择分类 */
    selectVal2: function selectVal2() {
      var l = this.val4.length;
      this.query.goodsCategoryId = this.val4[l - 1];
      this.getAsortGoods();
    },

    /**@function 选择店铺 */
    selectVal3: function selectVal3() {
      this.query.storeCategoryId = this.val3;
      this.getAsortGoods();
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
      console.log(this.multipleSelection);
      var str = "";
      for (var i in this.multipleSelection) {
        str = str + this.multipleSelection[i].storeGoodsCategoryId + ",";
      }
      str = str.substring(0, str.length - 1);
      console.log(str);
      this.deletestr = str;
    },
    handleSizeChange: function handleSizeChange(val) {
      console.log("\u6BCF\u9875 " + val + " \u6761");
      this.query.pageSize = val;
      this.getAsortGoods();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      console.log("\u5F53\u524D\u9875: " + val);
      this.query.pageNum = val;
      this.getAsortGoods();
    },

    // 添加商品
    addGoods: function addGoods() {
      this.$router.push("/assort/addAssortGoods");
    },

    // 查看详情
    checkOrganization: function checkOrganization(row) {
      this.$router.push({
        name: "agd",
        params: {
          goodsId: row.goodsId,
          storeCategoryId: row.storeCategoryId,
          storeGoodsCategoryId: row.storeGoodsCategoryId
        }
      });
    },

    // 修改排序号
    changeSort: function changeSort(row) {
      var _this4 = this;

      this.$http.get(this.$api.updateStoreGoodsCategorySort, {
        params: {
          storeGoodsCategoryId: row.storeGoodsCategoryId,
          sort: row.sort
        }
      }).then(function (rsp) {
        console.log("获取保存状态");
        console.log(rsp);
        if (rsp.data.flag == true) {
          _this4.$message({
            type: "success",
            message: "修改成功"
          });
        } else {
          _this4.$message({
            type: "error",
            message: "修改失败"
          });
        }
      });
    },

    // 删除
    cancel: function cancel(row, index) {
      var _this5 = this;

      this.$confirm("确认删除?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        console.log(row.channelGoodsCategoryId);
        _this5.$http.get(_this5.$api.deleteStoreGoodsCategoryGoods, {
          params: {
            storeGoodsCategoryIds: row.storeGoodsCategoryId
          }
        }).then(function (rsp) {
          console.log("获取删除状态");
          console.log(rsp);
          if (rsp.data.flag == true) {
            _this5.$message({
              type: "success",
              message: "删除成功。"
            });
            _this5.getAsortGoods();
          } else {
            _this5.$message({
              type: "error",
              message: "删除失败。"
            });
          }
        });
      }).catch(function () {
        _this5.$message({
          type: "info",
          message: "已取消删除"
        });
      });
    },

    // 批量删除
    deleteSelect: function deleteSelect() {
      var _this6 = this;

      this.$confirm("确认删除多行?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        _this6.$http.get(_this6.$api.deleteStoreGoodsCategoryGoods, {
          params: {
            storeGoodsCategoryIds: _this6.deletestr
          }
        }).then(function (rsp) {
          console.log("获取删除状态");
          console.log(rsp);
          if (rsp.data.flag == true) {
            _this6.$message({
              type: "success",
              message: "删除成功。"
            });
            _this6.getAsortGoods();
          } else {
            _this6.$message({
              type: "error",
              message: "删除失败。"
            });
          }
        });
      }).catch(function () {
        _this6.$message({
          type: "info",
          message: "已取消删除"
        });
      });
    }
  },
  created: function created() {
    this.getCategoryData();
    this.getAsortGoods();
    this.getCateChanelgoryData();
    console.log("val4Options");
    console.log(this.val4Options);
  }
};

/***/ }),

/***/ 374:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(375);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("1a9f0f4b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d95de57a\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goodsOfSort.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d95de57a\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./goodsOfSort.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 375:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-d95de57a],\n.search-group[data-v-d95de57a] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-d95de57a] {\n  float: right;\n}\n.content .content-title[data-v-d95de57a] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content content1 addcarousel" },
      [
        _c(
          "div",
          { staticClass: "btn-group" },
          [
            _c(
              "el-button",
              {
                staticClass: "purple-bg",
                attrs: { icon: "el-icon-plus" },
                on: { click: _vm.addGoods }
              },
              [_vm._v("添加商品")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-input", {
              staticClass: "search-input",
              attrs: {
                placeholder: "搜索商品名称 / 编号 / 品牌",
                "prefix-icon": "el-icon-search",
                clearable: ""
              },
              on: { blur: _vm.searchVal1 },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal1($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c("el-cascader", {
              staticClass: "mt5 mb5",
              attrs: {
                clearable: "",
                placeholder: "选择平台分类",
                "expand-trigger": "click",
                options: _vm.val4Options,
                "change-on-select": ""
              },
              on: { change: _vm.selectVal2 },
              model: {
                value: _vm.val4,
                callback: function($$v) {
                  _vm.val4 = $$v
                },
                expression: "val4"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                staticClass: "search-select mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择店铺分类"
                },
                on: { change: _vm.selectVal3 },
                model: {
                  value: _vm.val3,
                  callback: function($$v) {
                    _vm.val3 = $$v
                  },
                  expression: "val3"
                }
              },
              _vm._l(_vm.options3, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "singleTable",
            staticStyle: { width: "100%" },
            attrs: {
              data: _vm.tableData,
              "tooltip-effect": "dark",
              "default-sort": { prop: "sort_no", order: "descending" }
            },
            on: { "selection-change": _vm.handleSelectionChange }
          },
          [
            _c("el-table-column", {
              attrs: { type: "selection", width: "55" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsName", label: "名称", width: "100px" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "img", label: "图片", width: "150px;" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("img", {
                        staticStyle: { width: "60px", height: "60px" },
                        attrs: { src: "scope.row.goodsImageUrl", alt: "" }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsCode", label: "编号", width: "150px;" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "storeCategoryName",
                label: "店铺分类",
                width: "150px;"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "goodsBrandCnName",
                label: "品牌",
                width: "150px;"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "storeName", label: "分类", width: "150px;" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "originalPrice",
                label: "原价（元）",
                width: "150px"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "sellingPrice",
                label: "销售价（元）",
                width: "150px"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "inventory",
                label: "库存量（件）",
                width: "150px"
              }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "sort",
                label: "排序号",
                sortable: "",
                width: "150px"
              },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("el-input", {
                        staticStyle: { width: "80px" },
                        attrs: { placeholder: "999" },
                        on: {
                          blur: function($event) {
                            _vm.changeSort(scope.row)
                          }
                        },
                        nativeOn: {
                          keyup: function($event) {
                            if (
                              !("button" in $event) &&
                              _vm._k($event.keyCode, "enter", 13, $event.key)
                            ) {
                              return null
                            }
                            _vm.changeSort(scope.row)
                          }
                        },
                        model: {
                          value: scope.row.sort,
                          callback: function($$v) {
                            _vm.$set(scope.row, "sort", $$v)
                          },
                          expression: "scope.row.sort"
                        }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.checkOrganization(scope.row)
                            }
                          }
                        },
                        [_vm._v("查看")]
                      ),
                      _vm._v(" "),
                      _c(
                        "el-button",
                        {
                          staticClass: "orange-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.cancel(scope.row, scope.$index)
                            }
                          }
                        },
                        [_vm._v("删除")]
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticStyle: { "margin-top": "20px" } },
          [
            _c(
              "el-button",
              { staticClass: "orange-bg", on: { click: _vm.deleteSelect } },
              [_vm._v("批量删除")]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pagination" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.currentPage,
                    "page-sizes": [10, 20, 50, 100],
                    "page-size": 10,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d95de57a", esExports)
  }
}

/***/ })

});