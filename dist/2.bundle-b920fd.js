webpackJsonp([2],{

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue__ = __webpack_require__(328);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4e4dc90c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Staff_vue__ = __webpack_require__(445);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(443)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4e4dc90c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_Staff_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4e4dc90c_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_Staff_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\shop\\Staff.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4e4dc90c", Component.options)
  } else {
    hotAPI.reload("data-v-4e4dc90c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeUserStatus: function changeUserStatus(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    }
  },
  data: function data() {
    return {
      options: [{
        value: "1",
        label: "启用"
      }, {
        value: "0",
        label: "停用"
      }],
      val1: "",
      val2: "",
      total: 0,
      pages: 0,
      tableData: [],
      query: {
        pageNum: 1,
        pageSize: 20,
        userName: "",
        userStatus: ""
      }
    };
  },

  methods: {
    /**@function 获取职员列表 */
    getStaffList: function getStaffList() {
      var _this = this;

      this.$http.get(this.$api.findStoreEmployeeListPage, { params: this.query }).then(function (rsp) {
        _this.$isot(rsp); //登录超时处理函数
        console.log("获取职员列表");
        console.log(rsp.data);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 查看职员详情 */
    checkStaff: function checkStaff(row) {
      console.log(row);
      //   this.$router.push("/goods/checkgoods");
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    },

    /**@function 新增 */
    addStaff: function addStaff() {
      this.$router.push("/shop/addstaff");
    },

    /**@function 搜索 */
    searchVal: function searchVal() {
      this.query.userName = this.val1;
      this.getStaffList();
    },

    /**@function 选择 */
    selectVal: function selectVal() {
      this.query.userStatus = this.val2;
      this.getStaffList();
    },

    /**@function 启用停用 */
    toggle: function toggle(row, e) {
      var _this2 = this;

      console.log(row);
      if (row.userStatus == "1") {
        this.$confirm("是否停用职员？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var params = {};
          console.log("停用职员");
          params.storeEmployeeId = row.storeEmployeeId;
          params.employeeStatus = 0;
          _this2.$http.get(_this2.$api.updateStoreEmployeeStatus, {
            params: params
          }).then(function (rsp) {
            _this2.$isot(rsp); //登录超时处理函数
            _this2.getStaffList();
          });
          // this.getLogistics();
          _this2.$message({
            type: "warning",
            message: "停用职员成功!"
          });
        }).catch(function () {
          _this2.$message({
            type: "info",
            message: "已取消 [停用] 操作"
          });
        });
      } else {
        var params = {};
        params.storeEmployeeId = row.storeEmployeeId;
        params.employeeStatus = 1;
        this.$http.get(this.$api.updateStoreEmployeeStatus, { params: params }).then(function (rsp) {
          _this2.$isot(rsp); //登录超时处理函数
          _this2.getStaffList();
        });
        this.$message({
          type: "success",
          message: "启用职员成功!"
        });
      }
    }
  },
  created: function created() {
    this.getStaffList();
  }
};

/***/ }),

/***/ 443:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(444);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("22d287ec", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4e4dc90c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Staff.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4e4dc90c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Staff.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 444:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-4e4dc90c],\n.search-group[data-v-4e4dc90c] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-4e4dc90c] {\n  float: right;\n}\n", ""]);

// exports


/***/ }),

/***/ 445:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "btn-group" },
          [
            _c(
              "el-button",
              {
                staticClass: "purple-bg",
                attrs: { icon: "el-icon-plus" },
                on: { click: _vm.addStaff }
              },
              [_vm._v("新增职员")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-input", {
              staticClass: "search-input",
              attrs: {
                placeholder: "搜索职员姓名/账号",
                clearable: "",
                "prefix-icon": "el-icon-search"
              },
              on: { blur: _vm.searchVal },
              nativeOn: {
                keyup: function($event) {
                  if (
                    !("button" in $event) &&
                    _vm._k($event.keyCode, "enter", 13, $event.key)
                  ) {
                    return null
                  }
                  _vm.searchVal($event)
                }
              },
              model: {
                value: _vm.val1,
                callback: function($$v) {
                  _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                },
                expression: "val1"
              }
            }),
            _vm._v(" "),
            _c(
              "el-select",
              {
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择账号状态"
                },
                on: { change: _vm.selectVal },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              },
              _vm._l(_vm.options, function(item) {
                return _c("el-option", {
                  key: item.value,
                  staticClass: "search-select",
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "singleTable",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
          },
          [
            _c("el-table-column", { attrs: { type: "index", label: "序号" } }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "employeeName", label: "职员姓名" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "userName", label: "职员账号" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "position", label: "职位" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "phone", label: "联系方式" }
            }),
            _vm._v(" "),
            _c("el-table-column", { attrs: { prop: "remark", label: "备注" } }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "userStatus", label: "账号状态" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _vm._v(
                        "\n                    " +
                          _vm._s(
                            _vm._f("changeUserStatus")(scope.row.userStatus)
                          ) +
                          "\n                "
                      )
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作", prop: "status" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      scope.row.userStatus != "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "purple-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("启用")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      scope.row.userStatus == "1"
                        ? _c(
                            "el-button",
                            {
                              ref: "",
                              staticClass: "orange-txt",
                              attrs: { type: "text", size: "small" },
                              on: {
                                click: function($event) {
                                  _vm.toggle(scope.row, $event)
                                }
                              }
                            },
                            [_vm._v("停用")]
                          )
                        : _vm._e()
                    ]
                  }
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticStyle: { "margin-top": "20px" } },
          [
            _c("el-button", { attrs: { type: "text", disabled: "" } }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pagination" },
              [
                _c("el-pagination", {
                  attrs: {
                    "current-page": _vm.query.pageNum,
                    "page-sizes": [20, 50, 100, 200],
                    "page-size": _vm.query.pageSize,
                    layout: "total, sizes, prev, pager, next, jumper",
                    total: _vm.total
                  },
                  on: {
                    "size-change": _vm.handleSizeChange,
                    "current-change": _vm.handleCurrentChange
                  }
                })
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4e4dc90c", esExports)
  }
}

/***/ })

});