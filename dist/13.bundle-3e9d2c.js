webpackJsonp([13],{

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_d8000338_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addGoodsOfAdd_vue__ = __webpack_require__(478);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(476)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-d8000338"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoodsOfAdd_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_d8000338_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addGoodsOfAdd_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\addGoodsOfAdd.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d8000338", Component.options)
  } else {
    hotAPI.reload("data-v-d8000338", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(95);

var _stringify2 = _interopRequireDefault(_stringify);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "显示";
      } else {
        return "不显示";
      }
    }
  },
  data: function data() {
    return {
      channelGoodsId: "",
      saveFlag: false,
      val1: "",
      options1: [{ value: 0, label: "不显示" }, { value: 1, label: "显示" }],
      total: 0,
      queryOfContent: {
        pageNum: 1,
        pageSize: 20,
        channelGoodsId: "",
        isUp: ""
      },
      tableDataOfContent: [],
      endTime: "",
      rowIndex: 0,
      setAllFlag: false,
      cl: 1, //等级数
      levelNames: [],
      commissionLevel: [],
      showDialog: false,
      showDialogOfAdd: false,
      formOfAdd: {
        channelGoodsId: "",
        content: ""
      },
      showDialogOfEdit: false,
      formOfEdit: {
        channelGoodsDocumentId: "",
        documentContent: "",
        sort: "",
        isUp: true
      },
      goodsData: [],
      query: {
        pageNum: 1,
        pageSize: 10
      },
      attrName: [],
      tableData: [],
      ruleForm: {
        v1: "",
        v2: "",
        v3: "",
        v4: "",
        v5: "",
        v6: "",
        v7: "",
        v8: ""
      },
      FormDialog: {},
      FormDialogRow: {},
      rules: {}
    };
  },

  methods: {
    /**@function 获取商品详情 */
    getGoodsDetail: function getGoodsDetail() {
      var _this = this;

      var goodsId = this.$route.params.id;
      this.$http.get(this.$api.findGoods + ("?goodsId=" + goodsId)).then(function (rsp) {
        console.log("获取商品详情");
        console.log(rsp.data);
        _this.goodsData = rsp.data.data;
        _this.attrName = _this.goodsData.goodsAttributeDtos;
        _this.tableData = _this.goodsData.goodsSkuDtos;
        _this.tableData.forEach(function (v, i) {
          v.commissionLevelsTemp = "";
          v.commissionLevels = {};
        });
        _this.setAllFlag = true;
      });
    },

    /**@function 获取佣金等级 */
    getCommissionLevel: function getCommissionLevel() {
      var _this2 = this;

      this.$http.get(this.$api.findChannelCommissionRateConfList).then(function (rsp) {
        console.log("获取佣金等级");
        console.log(rsp.data);
        _this2.commissionLevel = rsp.data.data;
        _this2.commissionLevel.forEach(function (v, i) {
          _this2.levelNames.push(v.levelName);
        });

        _this2.cl = _this2.levelNames.length;
        console.log("this.cl==========" + _this2.cl);
        // this.attrName = this.goodsData.goodsAttributeDtos;
        // this.tableData = this.goodsData.goodsSkuDtos;
      });
    },

    //===================文案部分开始==========================

    /**@function 获取文案列表 */
    getContentData: function getContentData() {
      var _this3 = this;

      console.log("获取文案列表");
      this.queryOfContent.channelGoodsId = this.channelGoodsId;
      this.$http.get(this.$api.findChannelGoodsDocumentListPage, {
        params: this.queryOfContent
      }).then(function (rsp) {
        if (rsp.data.flag && rsp.data.data) {
          _this3.tableDataOfContent = rsp.data.data.list;
          _this3.total = rsp.data.data.total;
        } else {
          _this3.$message.error(rsp.data.msg);
          _this3.tableData = [];
          _this3.total = 0;
        }
      });
    },

    /**@function 新增文案 */
    addContent: function addContent() {
      console.log("新增文案");
      this.showDialogOfAdd = true;
    },

    /**@function 修改文案排序号 */
    editSort: function editSort(row) {
      var _this4 = this;

      console.log("修改文案排序号");
      var params = {};
      params.channelGoodsDocumentId = row.channelGoodsDocumentId;
      params.sort = row.sort;
      this.$http.get(this.$api.updateChannelGoodsDocumentSort, {
        params: params
      }).then(function (rsp) {
        if (rsp.data.flag) {
          _this4.getContentData();
          _this4.$message({
            type: "success",
            message: "修改排序号成功!"
          });
        } else {
          _this4.$message({
            type: "error",
            message: rsp.data.msg
          });
        }
      });
    },

    /**@function 选择显示状态 */
    selectVal1: function selectVal1() {
      console.log("选择显示状态");
      this.queryOfContent.isUp = this.val1;
      this.getContentData();
    },

    /**@function 修改文案按钮 */
    change: function change(row, i) {
      console.log("修改文案按钮");
      this.showDialogOfEdit = true;
      var tempForm = row;
      if (tempForm.isUp == 1) {
        tempForm.isUp = true;
      } else {
        tempForm.isUp = false;
      }
      this.formOfEdit = tempForm;
      console.log(this.formOfEdit);
    },

    /**@function 新增文案_保存按钮 */
    saveDialogOfAdd: function saveDialogOfAdd() {
      var _this5 = this;

      console.log("新增文案_保存按钮");
      this.formOfAdd.channelGoodsId = this.channelGoodsId;
      console.log(this.formOfAdd);
      var formDataToJson = (0, _stringify2.default)(this.formOfAdd);
      this.$http.post(this.$api.addChannelGoodsDocument, formDataToJson).then(function (rsp) {
        _this5.showDialogOfAdd = false;
        if (rsp.data.flag) {
          _this5.$message.success("新增成功！");
          _this5.getContentData();
        } else {
          _this5.$message.error(rsp.data.msg);
        }
      });
    },

    /**@function 新增文案_取消按钮 */
    cancelDialogOfAdd: function cancelDialogOfAdd() {
      this.showDialogOfAdd = false;
    },

    /**@function 修改文案_保存按钮 */
    saveDialogOfEdit: function saveDialogOfEdit() {
      var _this6 = this;

      console.log("修改文案_保存按钮");
      var formData = this.formOfEdit;
      if (formData.isUp == true) {
        formData.isUp = formData.isUp = 1;
      } else {
        formData.isUp = formData.isUp = 0;
      }
      formData.content = formData.documentContent;
      delete formData.documentContent;
      var formDataToJson = (0, _stringify2.default)(formData);

      console.log(formDataToJson);
      this.$http.post(this.$api.updateChannelGoodsDocument, formDataToJson).then(function (rsp) {
        _this6.showDialogOfEdit = false;
        if (rsp.data.flag) {
          _this6.$message.success("修改成功！");
          _this6.getContentData();
        } else {
          _this6.$message.error(rsp.data.msg);
        }
      });
    },

    /**@function 修改文案_删除按钮 */
    delDialogOfEdit: function delDialogOfEdit() {
      console.log("修改文案_删除按钮");
    },

    /**@function 修改文案_取消按钮 */
    cancelDialogOfEdit: function cancelDialogOfEdit() {
      console.log("修改文案_取消按钮");
      this.showDialogOfEdit = false;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.queryOfContent.pageSize = pageSize;
      this.getContentData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.queryOfContent.pageNum = pageNum;
      this.getContentData();
    },

    //===================文案部分结束==========================
    /**@function 统一设置佣金比例 */
    setAll: function setAll() {
      var _this7 = this;

      this.setAllFlag = true;
      var commissionLevels = {};
      for (var i = 0; i < this.cl; i++) {
        // obj.commissionRateConfId = this.commissionLevel[i].commissionRateConfId;
        // obj.level = this.commissionLevel[i].level;
        // obj.levelName = this.commissionLevel[i].levelName;
        switch (i) {
          case 0:
            commissionLevels.commissionRate1 = this.ruleForm.v1;
            commissionLevels.level1 = this.commissionLevel[i].level;
            commissionLevels.levelName1 = this.commissionLevel[i].levelName;
            break;
          case 1:
            commissionLevels.commissionRate2 = this.ruleForm.v2;
            commissionLevels.level2 = this.commissionLevel[i].level;
            commissionLevels.levelName2 = this.commissionLevel[i].levelName;
            break;
          case 2:
            commissionLevels.commissionRate3 = this.ruleForm.v3;
            commissionLevels.level3 = this.commissionLevel[i].level;
            commissionLevels.levelName3 = this.commissionLevel[i].levelName;
            break;
          case 3:
            commissionLevels.commissionRate4 = this.ruleForm.v4;
            commissionLevels.level4 = this.commissionLevel[i].level;
            commissionLevels.levelName4 = this.commissionLevel[i].levelName;
            break;
          case 4:
            ocommissionLevelsbj.commissionRate5 = this.ruleForm.v5;
            ocommissionLevelsbj.level5 = this.commissionLevel[i].level;
            commissionLevels.levelName5 = this.commissionLevel[i].levelName;
            break;
          case 5:
            commissionLevels.commissionRate6 = this.ruleForm.v6;
            commissionLevels.level6 = this.commissionLevel[i].level;
            commissionLevels.levelName6 = this.commissionLevel[i].levelName;
            break;
          case 6:
            commissionLevels.commissionRate7 = this.ruleForm.v7;
            commissionLevels.level7 = this.commissionLevel[i].level;
            commissionLevels.levelName7 = this.commissionLevel[i].levelName;
            break;
          case 7:
            commissionLevels.commissionRate8 = this.ruleForm.v8;
            commissionLevels.level8 = this.commissionLevel[i].level;
            commissionLevels.levelName8 = this.commissionLevel[i].levelName;
            break;
        }
      }
      console.log(commissionLevels);
      //=======================

      var tableDataTemp = this.tableData;
      this.tableData = [];
      for (var j = 0; j < tableDataTemp.length; j++) {
        tableDataTemp[j].commissionLevels = commissionLevels;
        var commissionLevelsTemp = "";

        if (commissionLevels.level1) {
          commissionLevelsTemp += commissionLevels.levelName1 + ":" + commissionLevels.commissionRate1 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate1 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level2) {
          commissionLevelsTemp += commissionLevels.levelName2 + ":" + commissionLevels.commissionRate2 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate2 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level3) {
          commissionLevelsTemp += commissionLevels.levelName3 + ":" + commissionLevels.commissionRate3 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate3 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level4) {
          commissionLevelsTemp += commissionLevels.levelName4 + ":" + commissionLevels.commissionRate4 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate4 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level5) {
          commissionLevelsTemp += commissionLevels.levelName5 + ":" + commissionLevels.commissionRate5 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate5 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level6) {
          commissionLevelsTemp += commissionLevels.levelName6 + ":" + commissionLevels.commissionRate6 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate6 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level7) {
          commissionLevelsTemp += commissionLevels.levelName7 + ":" + commissionLevels.commissionRate7 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate7 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }
        if (commissionLevels.level8) {
          commissionLevelsTemp += commissionLevels.levelName8 + ":" + commissionLevels.commissionRate8 + "%(\uFFE5" + Math.floor(commissionLevels.commissionRate8 / 100 * tableDataTemp[j].sellingPrice * 100) / 100 + "); ";
        }

        // commissionLevels.forEach((vv, ii) => {
        //   commissionLevelsTemp +=
        //     vv.levelName +
        //     `:` +
        //     vv.commissionRate +
        //     `%(￥` +
        //     Math.floor(
        //       vv.commissionRate / 100 * tableDataTemp[j].sellingPrice * 100
        //     ) /
        //       100 +
        //     "); ";
        // });
        tableDataTemp[j].commissionLevelsTemp = commissionLevelsTemp;
      }
      tableDataTemp.forEach(function (vvv, iii) {
        _this7.tableData.push(vvv);
      });
      console.log(this.tableData);
    },


    /**@function 保存 */
    save: function save() {
      var _this8 = this;

      console.log("保存");
      console.log(this.tableData);
      var formData = {};
      formData.goodsId = this.$route.params.id;

      if (this.endTime) {
        formData.endUseTime = this.endTime.substring(0, 10) + " 23:59:59";
      } else {
        formData.endUseTime = "";
      }

      formData.channelGoodsCommissionParams = [];
      this.commissionLevel.forEach(function (v, i) {
        var channelGoodsCommissionParams = {};
        channelGoodsCommissionParams.commissionRateConfId = v.commissionRateConfId;
        switch (i) {
          case 0:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v1;
            break;
          case 1:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v2;
            break;
          case 2:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v3;
            break;
          case 3:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v4;
            break;
          case 4:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v5;
            break;
          case 5:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v6;
            break;
          case 6:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v7;
            break;
          case 7:
            channelGoodsCommissionParams.commissionRate = _this8.ruleForm.v8;
            break;
        }
        formData.channelGoodsCommissionParams.push(channelGoodsCommissionParams);
      });

      var goodsSkuIds = [];
      this.goodsData.goodsSkuDtos.forEach(function (gv, gi) {
        goodsSkuIds.push(gv.goodsSkuId);
      });
      var channelGoodsSkuAddParams = []; //
      this.tableData.forEach(function (tv, ti) {
        var obj = {};
        obj.goodsSkuId = goodsSkuIds[ti];
        obj.channelGoodsCommissionParams = [];

        _this8.commissionLevel.forEach(function (v, i) {
          var channelGoodsCommissionParams = {};
          channelGoodsCommissionParams.commissionRateConfId = v.commissionRateConfId;
          switch (i) {
            case 0:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate1;
              break;
            case 1:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate2;
              break;
            case 2:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate3;
              break;
            case 3:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate4;
              break;
            case 4:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate5;
              break;
            case 5:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate6;
              break;
            case 6:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate7;
              break;
            case 7:
              channelGoodsCommissionParams.commissionRate = _this8.tableData[ti].commissionLevels.commissionRate8;
              break;
          }
          obj.channelGoodsCommissionParams.push(channelGoodsCommissionParams);
        });
        channelGoodsSkuAddParams.push(obj);
      });
      formData.channelGoodsSkuAddParams = channelGoodsSkuAddParams;

      console.log("formData====================>>");
      var formDataToJson = (0, _stringify2.default)(formData);
      console.log(formDataToJson);

      this.$http.post(this.$api.addChannelGoods, formDataToJson).then(function (rsp) {
        if (rsp.data.flag) {
          console.log(rsp.data);
          _this8.$message.success("保存成功！");
          _this8.channelGoodsId = "";
          _this8.$confirm("保存成功！可选择继续为该商品自定义推荐文案，或选择采用唯柚预设推荐文案，并返回上一页", "提示", {
            confirmButtonText: "自定义文案",
            cancelButtonText: "返回上一页",
            type: "warning"
          }).then(function () {
            _this8.$message({
              type: "success",
              message: "接下来您可以选择为该商品添加推荐文案"
            });
            _this8.saveFlag = true;
            _this8.channelGoodsId = rsp.data.data;
          }).catch(function () {
            _this8.$message({
              type: "info",
              message: "返回"
            });
            _this8.$router.go(-1);
          });
        } else {
          _this8.$message.error("数据未填写完毕或出错，请重新检查！");
        }
      }).catch(function (err) {
        console.log("err-------------------");
        console.log(err);
        _this8.$message.error(err);
        _this8.addFlag = false;
      });
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    },

    /**@function 编辑佣金*/
    edit: function edit(row, $index) {
      // console.log(row);
      if (this.setAllFlag) {
        this.showDialog = true;
        this.rowIndex = $index;
        this.FormDialogRow = row;
        this.FormDialog = row.commissionLevels;
        console.log(this.FormDialogRow);
        console.log(this.tableData);
      } else {
        this.$message.error("请在上一步设置佣金比例后再执行编辑佣金操作！");
      }
    },

    /**@function 保存弹出框*/
    saveDialog: function saveDialog() {
      var _this9 = this;

      var commissionLevelsTemp = "";
      var commissionLevels = {};
      if (this.FormDialog.level1) {
        commissionLevels.commissionRate1 = this.FormDialog.commissionRate1;
        commissionLevelsTemp += this.FormDialog.levelName1 + ":" + this.FormDialog.commissionRate1 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate1 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level2) {
        commissionLevels.commissionRate2 = this.FormDialog.commissionRate2;
        commissionLevelsTemp += this.FormDialog.levelName2 + ":" + this.FormDialog.commissionRate2 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate2 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level3) {
        commissionLevels.commissionRate3 = this.FormDialog.commissionRate3;
        commissionLevelsTemp += this.FormDialog.levelName3 + ":" + this.FormDialog.commissionRate3 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate3 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level4) {
        commissionLevels.commissionRate4 = this.FormDialog.commissionRate4;
        commissionLevelsTemp += this.FormDialog.levelName4 + ":" + this.FormDialog.commissionRate4 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate4 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level5) {
        commissionLevels.commissionRate5 = this.FormDialog.commissionRate5;
        commissionLevelsTemp += this.FormDialog.levelName5 + ":" + this.FormDialog.commissionRate5 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate5 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level6) {
        commissionLevels.commissionRate6 = this.FormDialog.commissionRate6;
        commissionLevelsTemp += this.FormDialog.levelName6 + ":" + this.FormDialog.commissionRate6 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate6 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level7) {
        commissionLevels.commissionRate7 = this.FormDialog.commissionRate7;
        commissionLevelsTemp += this.FormDialog.levelName7 + ":" + this.FormDialog.commissionRate7 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate7 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }
      if (this.FormDialog.level8) {
        commissionLevels.commissionRate8 = this.FormDialog.commissionRate8;
        commissionLevelsTemp += this.FormDialog.levelName8 + ":" + this.FormDialog.commissionRate8 + "%(\uFFE5" + Math.floor(this.FormDialog.commissionRate8 / 100 * this.tableData[this.rowIndex].sellingPrice * 100) / 100 + "); ";
      }

      this.tableData[this.rowIndex].commissionLevelsTemp = commissionLevelsTemp;
      this.tableData[this.rowIndex].commissionLevels = commissionLevels;
      var tableDataTemp = this.tableData;
      this.tableData = [];
      tableDataTemp.forEach(function (vvv, iii) {
        _this9.tableData.push(vvv);
      });
      this.showDialog = false;

      console.log(this.tableData);
    },

    /**@function 取消保存*/
    cancelDialog: function cancelDialog() {
      this.showDialog = false;
    }
  },
  created: function created() {
    this.getCommissionLevel();
    this.getGoodsDetail();
  }
};

/***/ }),

/***/ 476:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(477);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("2ed5e3ba", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d8000338\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addGoodsOfAdd.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d8000338\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addGoodsOfAdd.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 477:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-d8000338] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n.w120[data-v-d8000338] {\n  width: 120px;\n}\n", ""]);

// exports


/***/ }),

/***/ 478:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品基本信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.goodsData.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品编号：" + _vm._s(_vm.goodsData.goodsCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品库存：" + _vm._s(_vm.goodsData.inventory))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品品牌：" + _vm._s(_vm.goodsData.goodsBrandName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品分类：" + _vm._s(_vm.goodsData.goodsCategoryName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "商品价格（元）：" + _vm._s(_vm.goodsData.sellingPrice)
                  )
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("设置推荐截止时间")
          ]),
          _vm._v(" "),
          _c("span", [_vm._v("截止时间:")]),
          _vm._v(" "),
          _c("el-date-picker", {
            attrs: {
              type: "date",
              "value-format": "yyyy-MM-dd HH:mm:ss",
              placeholder: "选择截止时间"
            },
            model: {
              value: _vm.endTime,
              callback: function($$v) {
                _vm.endTime = $$v
              },
              expression: "endTime"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("设置佣金比例")]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "ruleForm",
              attrs: {
                model: _vm.ruleForm,
                rules: _vm.rules,
                "label-width": "60px"
              }
            },
            [
              _c(
                "el-row",
                { attrs: { gutter: 20 } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _vm.levelNames[0]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[0], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v1,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v1", $$v)
                                  },
                                  expression: "ruleForm.v1"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v1 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[2]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[2], prop: "v3" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v3,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v3", $$v)
                                  },
                                  expression: "ruleForm.v3"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v3 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[4]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[4], prop: "v5" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v5,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v5", $$v)
                                  },
                                  expression: "ruleForm.v5"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v5 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[6]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[6], prop: "v7" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v7,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v7", $$v)
                                  },
                                  expression: "ruleForm.v7"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v7 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 6, offset: 5 } },
                    [
                      _vm.levelNames[1]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[1], prop: "v2" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v2,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v2", $$v)
                                  },
                                  expression: "ruleForm.v2"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v2 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[3]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[3], prop: "v4" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v4,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v4", $$v)
                                  },
                                  expression: "ruleForm.v4"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v4 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[5]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[5], prop: "v6" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v6,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v6", $$v)
                                  },
                                  expression: "ruleForm.v6"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v6 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[7]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[7], prop: "v8" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                on: { input: _vm.setAll },
                                model: {
                                  value: _vm.ruleForm.v8,
                                  callback: function($$v) {
                                    _vm.$set(_vm.ruleForm, "v8", $$v)
                                  },
                                  expression: "ruleForm.v8"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.ruleForm.v8 /
                                        100 *
                                        _vm.goodsData.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品属性")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: {
                  prop: "specName1",
                  label: _vm.attrName[0].attributeName
                }
              }),
              _vm._v(" "),
              _vm.attrName[1]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName2",
                      label: _vm.attrName[1].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.attrName[2]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName3",
                      label: _vm.attrName[2].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "commissionLevelsTemp", label: "*佣金比例" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "orange-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.edit(scope.row, scope.$index)
                              }
                            }
                          },
                          [_vm._v("编辑佣金")]
                        )
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.saveFlag == true
        ? _c(
            "div",
            { staticClass: "content mt20" },
            [
              _c("div", { staticClass: "content-title" }, [_vm._v("推荐文案")]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mt10 mb10" },
                [
                  _c(
                    "el-button",
                    {
                      staticClass: "purple-bg",
                      attrs: { icon: "el-icon-plus" },
                      on: { click: _vm.addContent }
                    },
                    [_vm._v("新增文案")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mt10 mb10" },
                [
                  _c(
                    "el-select",
                    {
                      staticClass: "mt5 mb5",
                      attrs: {
                        filterable: "",
                        clearable: "",
                        placeholder: "选择显示状态"
                      },
                      on: { change: _vm.selectVal1 },
                      model: {
                        value: _vm.val1,
                        callback: function($$v) {
                          _vm.val1 = $$v
                        },
                        expression: "val1"
                      }
                    },
                    _vm._l(_vm.options1, function(item) {
                      return _c("el-option", {
                        key: item.value,
                        attrs: { label: item.label, value: item.value }
                      })
                    })
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-table",
                {
                  staticStyle: { width: "100%" },
                  attrs: {
                    data: _vm.tableDataOfContent,
                    "tooltip-effect": "dark"
                  }
                },
                [
                  _c("el-table-column", {
                    attrs: { type: "index", label: "序号" }
                  }),
                  _vm._v(" "),
                  _c("el-table-column", {
                    attrs: { prop: "documentContent", label: "文案内容" }
                  }),
                  _vm._v(" "),
                  _c("el-table-column", {
                    attrs: { prop: "isUp", label: "显示状态" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function(scope) {
                          return [
                            _vm._v(
                              "\n          " +
                                _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                                "\n        "
                            )
                          ]
                        }
                      }
                    ])
                  }),
                  _vm._v(" "),
                  _c("el-table-column", {
                    attrs: { prop: "sort", label: "排序号", width: "120" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function(scope) {
                          return [
                            _c("el-input", {
                              attrs: { placeholder: "999" },
                              on: {
                                blur: function($event) {
                                  _vm.editSort(scope.row)
                                }
                              },
                              nativeOn: {
                                keyup: function($event) {
                                  if (
                                    !("button" in $event) &&
                                    _vm._k(
                                      $event.keyCode,
                                      "enter",
                                      13,
                                      $event.key
                                    )
                                  ) {
                                    return null
                                  }
                                  _vm.editSort(scope.row)
                                }
                              },
                              model: {
                                value: scope.row.sort,
                                callback: function($$v) {
                                  _vm.$set(scope.row, "sort", $$v)
                                },
                                expression: "scope.row.sort"
                              }
                            })
                          ]
                        }
                      }
                    ])
                  }),
                  _vm._v(" "),
                  _c("el-table-column", {
                    attrs: { label: "操作", width: "120" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function(scope) {
                          return [
                            _c(
                              "el-button",
                              {
                                staticClass: "orange-txt",
                                attrs: { type: "text", size: "small" },
                                on: {
                                  click: function($event) {
                                    _vm.change(scope.row, scope.$index)
                                  }
                                }
                              },
                              [_vm._v("修改")]
                            )
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mt20" },
                [
                  _c("el-button", { attrs: { type: "text" } }),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "fr" },
                    [
                      _c("el-pagination", {
                        attrs: {
                          "current-page": _vm.queryOfContent.pageNum,
                          "page-sizes": [20, 50, 100, 200],
                          "page-size": _vm.queryOfContent.pageSize,
                          layout: "total, sizes, prev, pager, next, jumper",
                          total: _vm.total
                        },
                        on: {
                          "size-change": _vm.handleSizeChange,
                          "current-change": _vm.handleCurrentChange
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      !_vm.saveFlag
        ? _c(
            "el-button",
            { staticClass: "purple-bg mt10", on: { click: _vm.save } },
            [_vm._v("保 存")]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt10", on: { click: _vm.goBack } },
        [_vm._v("返 回")]
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "编辑属性佣金", visible: _vm.showDialog },
          on: {
            "update:visible": function($event) {
              _vm.showDialog = $event
            }
          }
        },
        [
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.goodsData.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "销售价（元）：" + _vm._s(_vm.FormDialogRow.sellingPrice)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("库存量：" + _vm._s(_vm.FormDialogRow.inventory))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    _vm._s(_vm.attrName[0].attributeName) +
                      "：" +
                      _vm._s(_vm.FormDialogRow.specName1)
                  )
                ]),
                _vm._v(" "),
                _vm.attrName[1]
                  ? _c("div", { staticClass: "p10" }, [
                      _vm._v(
                        _vm._s(_vm.attrName[1].attributeName) +
                          "：" +
                          _vm._s(_vm.FormDialogRow.specName2)
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.attrName[2]
                  ? _c("div", { staticClass: "p10" }, [
                      _vm._v(
                        _vm._s(_vm.attrName[2].attributeName) +
                          "：" +
                          _vm._s(_vm.FormDialogRow.specName3)
                      )
                    ])
                  : _vm._e()
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c(
            "div",
            {
              staticStyle: {
                "font-size": "18px !important",
                "padding-bottom": "30px"
              }
            },
            [_vm._v("设置佣金比例")]
          ),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "FormDialog",
              attrs: {
                model: _vm.FormDialog,
                rules: _vm.rules,
                "label-width": "60px"
              }
            },
            [
              _c(
                "el-row",
                { attrs: { gutter: 20 } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 10 } },
                    [
                      _vm.levelNames[0]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[0], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate1,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate1",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate1"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate1 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[2]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[2], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate3,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate3",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate3"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate3 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[4]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[4], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate5,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate5",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate5"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate5 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[6]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[6], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate7,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate7",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate7"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate7 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 10, offset: 4 } },
                    [
                      _vm.levelNames[1]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[1], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate2,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate2",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate2"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate2 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[3]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[3], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate4,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate4",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate4"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate4 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[5]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[5], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate6,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate6",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate6"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate6 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.levelNames[7]
                        ? _c(
                            "el-form-item",
                            { attrs: { label: _vm.levelNames[7], prop: "v1" } },
                            [
                              _c("el-input", {
                                staticClass: "p10 w120",
                                model: {
                                  value: _vm.FormDialog.commissionRate8,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.FormDialog,
                                      "commissionRate8",
                                      $$v
                                    )
                                  },
                                  expression: "FormDialog.commissionRate8"
                                }
                              }),
                              _vm._v(
                                "%(￥" +
                                  _vm._s(
                                    Math.floor(
                                      _vm.FormDialog.commissionRate8 /
                                        100 *
                                        _vm.FormDialogRow.sellingPrice *
                                        100
                                    ) / 100
                                  ) +
                                  ")\n          "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                { staticClass: "purple-bg", on: { click: _vm.saveDialog } },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                { staticClass: "orange-bg", on: { click: _vm.cancelDialog } },
                [_vm._v("取 消")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "新增文案", visible: _vm.showDialogOfAdd },
          on: {
            "update:visible": function($event) {
              _vm.showDialogOfAdd = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "content",
              attrs: {
                model: _vm.formOfAdd,
                "label-position": "left",
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "文案内容", prop: "name" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "textarea",
                              autosize: { minRows: 6, maxRows: 10 },
                              placeholder: "请输入内容"
                            },
                            model: {
                              value: _vm.formOfAdd.content,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfAdd, "content", $$v)
                              },
                              expression: "formOfAdd.content"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                { staticStyle: { display: "none" } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "" } },
                        [_c("el-input")],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  on: { click: _vm.saveDialogOfAdd }
                },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                {
                  staticClass: "orange-bg",
                  on: { click: _vm.cancelDialogOfAdd }
                },
                [_vm._v("取 消")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { title: "修改文案", visible: _vm.showDialogOfEdit },
          on: {
            "update:visible": function($event) {
              _vm.showDialogOfEdit = $event
            }
          }
        },
        [
          _c(
            "el-form",
            {
              ref: "showDialogOfEdit",
              attrs: {
                model: _vm.formOfEdit,
                "label-position": "left",
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "文案内容", prop: "name" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "textarea",
                              autosize: { minRows: 6, maxRows: 10 }
                            },
                            model: {
                              value: _vm.formOfEdit.documentContent,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfEdit, "documentContent", $$v)
                              },
                              expression: "formOfEdit.documentContent"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.formOfEdit.sort,
                              callback: function($$v) {
                                _vm.$set(_vm.formOfEdit, "sort", $$v)
                              },
                              expression: "formOfEdit.sort"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-form-item",
                    { attrs: { prop: "isShow", label: "显示状态" } },
                    [
                      _c("el-switch", {
                        model: {
                          value: _vm.formOfEdit.isUp,
                          callback: function($$v) {
                            _vm.$set(_vm.formOfEdit, "isUp", $$v)
                          },
                          expression: "formOfEdit.isUp"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "dialog-footer",
              attrs: { slot: "footer" },
              slot: "footer"
            },
            [
              _c(
                "el-button",
                {
                  staticClass: "purple-bg",
                  on: { click: _vm.saveDialogOfEdit }
                },
                [_vm._v("保 存")]
              ),
              _vm._v(" "),
              _c(
                "el-button",
                {
                  staticClass: "orange-bg",
                  on: { click: _vm.cancelDialogOfEdit }
                },
                [_vm._v("返 回")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d8000338", esExports)
  }
}

/***/ })

});