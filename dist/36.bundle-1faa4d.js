webpackJsonp([36],{

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_647a2137_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddCarousel_vue__ = __webpack_require__(409);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(407)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-647a2137"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddCarousel_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_647a2137_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddCarousel_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\carousel\\AddCarousel.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-647a2137", Component.options)
  } else {
    hotAPI.reload("data-v-647a2137", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 316:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    }
  },
  data: function data() {
    return {
      imgApi: this.$imgApi, //图片上传api
      upImgs: [],
      keepTime: "",
      getGoodsIds: [],
      multipleSelection: [],
      val1: "",
      val2: "",
      val3: "",
      val4: "",
      options2: [],
      options3: [{
        value: "1",
        label: "上线"
      }, {
        value: "0",
        label: "下线"
      }],
      options4: [{
        value: "1",
        label: "已添加"
      }, {
        value: "0",
        label: "未添加"
      }],
      ruleForm: {
        title: "",
        imgUrl: "",
        jumpType: "",
        goods_ids: "",
        index: "",
        beginUseTime: "",
        endUseTime: "",
        goodsIds: ""
      },
      rules: {
        title: [{ required: true, message: "请输入广告标题", trigger: "blur" }],
        sort: [{ required: true, message: "请输入排序号", trigger: "blur" }, {
          pattern: /^\d+$/,
          message: "请输入数字",
          trigger: "blur"
        }]
      },
      value8: "",
      tableData: []
    };
  },

  methods: {
    /**@function 查看 */
    checkGoods: function checkGoods(row) {
      console.log("查看商品");
      console.log(row);
      this.$router.push({
        name: "gc",
        params: { id: row.goodsId }
      });
    },

    /**@function 删除当前行 */
    delGoods: function delGoods(row) {
      var _this = this;

      console.log(row.goodsId);
      this.$confirm("是否删除？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        var i = _this.getGoodsIds.indexOf(row.goodsId);
        _this.getGoodsIds.splice(i, 1);
        var index = _this.tableData.indexOf(row);
        if (index > -1) {
          _this.tableData.splice(index, 1);
          _this.$message({
            type: "warning",
            message: "删除成功!"
          });
        }
      }).catch(function () {
        _this.$message({
          type: "info",
          message: "已取消删除操作"
        });
      });
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 图片 */
    handleRemove: function handleRemove(file, fileList) {
      console.log(file, fileList);
    },

    /**@function 控制上传文件规格 */
    beforeAvatarUpload: function beforeAvatarUpload(file) {
      console.log("控制上传文件规格");
      console.log(file);
      var isType = file.type == "image/jpeg" || "image/png";
      var isLt = file.size / 1024 / 1024 < 2;
      if (!isType) {
        this.$message.error("上传图片只能是 JPG 或者 PNG 格式!");
      }
      if (!isLt) {
        this.$message.error("上传图片大小不能超过 2MB!");
      }
      return isType && isLt;
    },

    /**@function 图片上传成功事件 */
    uploadImgHandler: function uploadImgHandler(data) {
      console.log("图片上传成功事件");
      // console.log(data);
      this.ruleForm.imgUrl = data.data[0].fileUrl;
      console.log(this.ruleForm.imgUrl);
    },

    /**@function 保存 */
    save: function save() {
      var _this2 = this;

      var params = this.ruleForm;
      params.beginUseTime = this.keepTime[0];
      params.endUseTime = this.keepTime[1].substring(0, 10) + " 23:59:59";
      params.goodsIds = this.getGoodsIds.join(",");
      params.jumpType = 2;
      console.log("保存");
      console.log(params);
      this.$http.post(this.$api.addStoreAdImage, params).then(function (rsp) {
        // this.$isot(rsp); //登录超时处理函数
        if (rsp.data.flag) {
          console.log(rsp.data);
          _this2.$message.success("保存成功！");
          //  保存成功后跳转
          console.log(rsp.data.adImageId);
          _this2.$router.push({
            name: "cck",
            params: { id: rsp.data.data.adImageId }
          });
        } else {
          _this2.$message.error("数据未填写完毕或出错，请重新检查！");
        }
      });
    },

    // /**@function 保存并新增 */
    // saveAndAdd() {
    //   console.log(this.ruleForm);
    //   this.save();
    //   console.log("跳转");
    //   //  保存成功后刷新本页。。。
    //   this.$router.go(0);
    // },
    /**@function 取消 */
    cancel: function cancel() {
      var _this3 = this;

      this.$confirm("取消后将直接返回上一页面，请确认是否取消？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        _this3.$message({
          type: "success",
          message: "取消成功!"
        });
        _this3.$router.go(-1);
      }).catch(function () {
        _this3.$message({
          type: "info",
          message: "放弃 [取消] 操作!"
        });
      });
    },

    /**@function 根据商品ID获取商品详情并添加到table中 */
    getGoodsById: function getGoodsById(id) {
      var _this4 = this;

      this.$http(this.$api.findGoods + ("?goodsId=" + id)).then(function (rsp) {
        _this4.$isot(rsp); //登录超时处理函数
        _this4.tableData.push(rsp.data.data);
        console.log("根据商品ID查询商品");
        //  console.log(this.tableData)
        console.log(rsp.data);
      });
    },

    /**@function 根据编号获取商品ID，再根据ID添加商品 */
    addGoods: function addGoods() {
      var _this5 = this;

      console.log(this.val1);
      this.$http(this.$api.findGoodsByGoodsCode + ("?goodsCode=" + this.val1)).then(function (rsp) {
        _this5.$isot(rsp); //登录超时处理函数
        console.log("添加商品");
        console.log(rsp.data);
        // console.log(rsp.data.data[0]);
        if (!rsp.data.data[0]) {
          _this5.$message.error("商品编号错误！");
          return false;
        }
        var id = rsp.data.data[0];
        //判断商品是已添加过
        if (_this5.getGoodsIds.indexOf(id) != -1) {
          _this5.$message.error("该商品已在表中！");
        } else {
          _this5.getGoodsById(id);
          _this5.getGoodsIds.push(id);
        }
      });
      //   商品编号错误则$message提示
    },

    /**@function 搜索val1 */
    searchVal1: function searchVal1() {
      console.log(this.val1);
    },

    /**@function 选择val2 */
    selectVal2: function selectVal2() {
      console.log(this.val2);
    },

    /**@function 选择val3 */
    selectVal3: function selectVal3() {
      console.log(this.val3);
    },
    selectVal4: function selectVal4() {
      console.log(this.val3);
    },

    /**@function 批量删除 */
    delSelected: function delSelected() {
      var _this6 = this;

      if (this.multipleSelection.length != 0) {
        this.$confirm("是否批量删除？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          // this.$message({
          //   type: "warning",
          //   message: "删除成功!"
          // });
          console.log("批量删除");
          console.log(_this6.multipleSelection);

          for (var i = 0; i < _this6.multipleSelection.length; i++) {
            // let i = this.getGoodsIds.indexOf(
            //   this.multipleSelection[i].goodsId
            // );
            _this6.getGoodsIds.splice(i, 1);
            var index = _this6.tableData.indexOf(_this6.multipleSelection[i]);
            if (index > -1) {
              _this6.tableData.splice(index, 1);
            }
          }
          _this6.$message({
            type: "warning",
            message: "删除成功!"
          });
        }).catch(function () {
          _this6.$message({
            type: "info",
            message: "已取消删除操作"
          });
        });
      } else {
        this.$message({
          type: "error",
          message: "当前未选中任何行！"
        });
      }
    }
  }
};

/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(408);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("6bfa43e8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-647a2137\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddCarousel.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-647a2137\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddCarousel.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-647a2137],\n.search-group[data-v-647a2137] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-647a2137] {\n  float: right;\n}\n.content .content-title[data-v-647a2137] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("轮播图管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1 addcarousel" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("填写轮播图信息")
          ]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "ruleForm",
              attrs: {
                model: _vm.ruleForm,
                rules: _vm.rules,
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "轮播图标题", prop: "title" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.title,
                              callback: function($$v) {
                                _vm.$set(
                                  _vm.ruleForm,
                                  "title",
                                  typeof $$v === "string" ? $$v.trim() : $$v
                                )
                              },
                              expression: "ruleForm.title"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                {
                  staticClass: "addcarousel-pic-input",
                  staticStyle: { height: "200px" }
                },
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "轮播图图片" } },
                        [
                          _c(
                            "el-upload",
                            {
                              attrs: {
                                action: _vm.imgApi,
                                "list-type": "picture-card",
                                "before-upload": _vm.beforeAvatarUpload,
                                "on-success": _vm.uploadImgHandler,
                                "file-list": _vm.upImgs,
                                "on-remove": _vm.handleRemove,
                                limit: 1
                              }
                            },
                            [
                              _c("i", { staticClass: "el-icon-plus" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "el-upload__tip orange-txt",
                                  attrs: { slot: "tip" },
                                  slot: "tip"
                                },
                                [
                                  _vm._v(
                                    "**图片尺寸：750*320px；格式:jpg、png；大小：200KB以内"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-form-item",
                    { attrs: { label: "持续时间" } },
                    [
                      _c(
                        "el-col",
                        { attrs: { span: 8 } },
                        [
                          _c("el-date-picker", {
                            attrs: {
                              type: "daterange",
                              "range-separator": "至",
                              "start-placeholder": "轮播图开始日期",
                              "end-placeholder": "轮播图结束日期",
                              "value-format": "yyyy-MM-dd HH:mm:ss"
                            },
                            model: {
                              value: _vm.keepTime,
                              callback: function($$v) {
                                _vm.keepTime = $$v
                              },
                              expression: "keepTime"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号", prop: "sort" } },
                        [
                          _c("el-input", {
                            attrs: { type: "number" },
                            model: {
                              value: _vm.ruleForm.index,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "index", $$v)
                              },
                              expression: "ruleForm.index"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "purple-bg", on: { click: _vm.save } }, [
        _vm._v("保存")
      ]),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.cancel } }, [
        _vm._v("取消")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-647a2137", esExports)
  }
}

/***/ })

});