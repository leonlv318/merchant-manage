webpackJsonp([29],{

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue__ = __webpack_require__(301);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_1c4441c6_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_CheckGoods_vue__ = __webpack_require__(364);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(362)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1c4441c6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_CheckGoods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_1c4441c6_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_CheckGoods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\CheckGoods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1c4441c6", Component.options)
  } else {
    hotAPI.reload("data-v-1c4441c6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 301:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      fromActivity: 0, //从活动页来的标志
      goodsDetail: "",
      carouselItem: [],
      attrName: [],
      tableData: []
    };
  },

  methods: {
    /**@function 获取商品详情 */
    getGoodsDetail: function getGoodsDetail() {
      var _this = this;

      var goodsId = this.$route.params.id;
      this.$http.get(this.$api.findGoods + ("?goodsId=" + goodsId)).then(function (rsp) {
        console.log("获取商品详情");
        console.log(rsp.data);
        _this.goodsDetail = rsp.data.data;
        _this.carouselItem = _this.goodsDetail.goodsCarouselImages;
        _this.attrName = _this.goodsDetail.goodsAttributeDtos;
        _this.tableData = _this.goodsDetail.goodsSkuDtos;
      });
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.push({
        name: "gg",
        params: {
          query: this.$route.params.query
        }
      });
    }
  },
  created: function created() {
    if (this.$route.params.fromActivity) {
      this.fromActivity = 1;
    } else {
      this.fromActivity = 0;
    }
    this.getGoodsDetail();
  }
};

/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(363);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("4f334cf4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c4441c6\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CheckGoods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1c4441c6\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CheckGoods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-1c4441c6] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n.content1[data-v-1c4441c6] {\n  margin-bottom: 20px;\n}\n.box-border[data-v-1c4441c6] {\n  border: 1px solid #dcdfe6;\n  padding: 30px;\n  border-radius: 4px;\n}\n.carousel[data-v-1c4441c6] {\n  width: 380px;\n}\n.content-detail .content-info[data-v-1c4441c6] {\n  font-size: 16px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #303133;\n  font-weight: bold;\n  padding-bottom: 30px;\n}\n", ""]);

// exports


/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20, type: "flex", justify: "space-between" } },
            [
              _c("el-col", { attrs: { span: 10 } }, [
                _c("div", { staticClass: "content-content" }, [
                  _c(
                    "div",
                    { staticClass: "check_goods carousel" },
                    [
                      [
                        _c(
                          "el-carousel",
                          { attrs: { "indicator-position": "outside" } },
                          _vm._l(_vm.carouselItem, function(item) {
                            return _c("el-carousel-item", { key: item }, [
                              _c("img", {
                                attrs: {
                                  src: item,
                                  width: "380",
                                  height: "380"
                                }
                              })
                            ])
                          })
                        )
                      ]
                    ],
                    2
                  )
                ])
              ]),
              _vm._v(" "),
              _c("el-col", [
                _c("div", { staticClass: "content-detail" }, [
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("商品名称：" + _vm._s(_vm.goodsDetail.goodsName))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("商品编号：" + _vm._s(_vm.goodsDetail.goodsCode))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("商品货号：" + _vm._s(_vm.goodsDetail.itemCode))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v(
                      "商品分类：" + _vm._s(_vm.goodsDetail.goodsCategoryName)
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v(
                      "商品品牌：" + _vm._s(_vm.goodsDetail.goodsBrandName)
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("原价(元)：\n                        "),
                    _c("span", { staticClass: "orange-txt" }, [
                      _vm._v(_vm._s(_vm.goodsDetail.originalPrice))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("销售价(元)：\n                        "),
                    _c("span", { staticClass: "orange-txt" }, [
                      _vm._v(_vm._s(_vm.goodsDetail.sellingPrice))
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "content-info" }, [
                    _vm._v("库存(件)：\n                        "),
                    _c("span", { staticClass: "orange-txt" }, [
                      _vm._v(_vm._s(_vm.goodsDetail.inventory))
                    ])
                  ])
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("商品属性")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, border: "" }
            },
            [
              _c("el-table-column", {
                attrs: {
                  prop: "specName1",
                  label: _vm.attrName[0].attributeName
                }
              }),
              _vm._v(" "),
              _vm.attrName[1]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName2",
                      label: _vm.attrName[1].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _vm.attrName[2]
                ? _c("el-table-column", {
                    attrs: {
                      prop: "specName3",
                      label: _vm.attrName[2].attributeName
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "originalPrice", label: "原价" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "商品图片" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          attrs: {
                            src: scope.row.imageUrl,
                            width: "50",
                            height: "50"
                          }
                        })
                      ]
                    }
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "content" }, [
        _c("div", { staticClass: "content-title" }, [_vm._v("商品详情")]),
        _vm._v(" "),
        _c("div", {
          staticClass: "box-border",
          domProps: { innerHTML: _vm._s(_vm.goodsDetail.detail) }
        })
      ]),
      _vm._v(" "),
      this.fromActivity != 1
        ? _c(
            "el-button",
            {
              staticClass: "orange-bg mt20",
              staticStyle: { "margin-top": "20px" },
              on: { click: _vm.goBack }
            },
            [_vm._v("返回")]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1c4441c6", esExports)
  }
}

/***/ })

});