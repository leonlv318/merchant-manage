webpackJsonp([7],{

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5d9f542a_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_recommendationDetail_vue__ = __webpack_require__(460);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(458)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-5d9f542a"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_recommendationDetail_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_5d9f542a_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_recommendationDetail_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\recommendationDetail.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d9f542a", Component.options)
  } else {
    hotAPI.reload("data-v-5d9f542a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    },
    changeSex: function changeSex(value) {
      if (value == 1) {
        return "男";
      } else {
        return "女";
      }
    }
  },
  data: function data() {
    return {
      userData: {},
      goodsData: {},
      recommendationData: {},
      total1: 0,
      pages1: 0,
      total2: 0,
      pages2: 0,
      query1: {
        pageNum: 1,
        pageSize: 20,
        channelRecordId: ""
      },
      query2: {
        pageNum: 1,
        pageSize: 20,
        channelRecordId: "",
        orderStatus: 1,
        settleStatus: 2
      },
      tableData1: [],
      attrName: [],
      tableData2: []
    };
  },

  methods: {
    /**@function 获取详情 */
    getInfo: function getInfo() {
      var _this = this;

      // if (this.$route.params.channelRecordModeStatisticsId) {
      this.$http.get(this.$api.findChannelRecordDto, { params: this.$route.params }).then(function (rsp) {
        console.log("获取详情");
        console.log(rsp.data);
        _this.userData = rsp.data.data.channelUserDto;
        _this.goodsData = rsp.data.data.channelGoodsInfo;
        _this.recommendationData = rsp.data.data.channelRecordModeStatisticsDto;
      });
      // } else {
      //   this.$message.error("无法获取该用户的详情！");
      // }
    },

    /**@function 获取点击用户 */
    getTableData1: function getTableData1() {
      var _this2 = this;

      if (!this.$route.params.channelRecordId) {
        return;
      }
      this.query1.channelRecordId = this.$route.params.channelRecordId;
      this.$http.get(this.$api.findChannelUserRelationByChannelRecordId, {
        params: this.query1
      }).then(function (rsp) {
        console.log("获取点击用户");
        console.log(rsp.data);
        _this2.tableData1 = rsp.data.data.list;
        _this2.total1 = rsp.data.data.total;
      });
    },

    /**@function 获取付款用户 */
    getTableData2: function getTableData2() {
      var _this3 = this;

      this.query2.channelRecordId = this.$route.params.channelRecordId;
      this.$http.get(this.$api.findChannelRecordOrdersRelationList, {
        params: this.query2
      }).then(function (rsp) {
        console.log("获取付款用户");
        console.log(rsp.data);
        _this3.tableData2 = rsp.data.data.list;
        _this3.total2 = rsp.data.data.total;
        var data = rsp.data.data.list;
        for (var i = 0; i < data.length; i++) {
          var buyInfo = "";
          if (data[i].spec3 != null) {
            buyInfo = data[i].spec3 + "/" + data[i].spec2 + "/" + data[i].spec1 + "(" + data[i].goodsNumber + ")";
            _this3.tableData2[i].push(buyInfo);
          }
        }
      });
    },

    /**@function 获取导购详情 */
    getRecommendationDetaill: function getRecommendationDetaill() {
      this.getInfo();
      this.getTableData1();
      this.getTableData2();
    },

    /**@function 查看订单 */
    // check(row) {
    //   console.log("查看订单-----------");
    // },
    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    },

    /**@function 分页1 */
    handleSizeChange1: function handleSizeChange1(pageSize) {
      this.query1.pageSize = pageSize;
      this.getRecommendationDetaill();
    },

    /**@function 分页1 */
    handleCurrentChange1: function handleCurrentChange1(pageNum) {
      this.query1.pageNum = pageNum;
      this.getRecommendationDetaill();
    },

    /**@function 分页2*/
    handleSizeChange2: function handleSizeChange2(pageSize) {
      this.query2.pageSize = pageSize;
      this.getRecommendationDetaill();
    },

    /**@function 分页2 */
    handleCurrentChange2: function handleCurrentChange2(pageNum) {
      this.query2.pageNum = pageNum;
      this.getRecommendationDetaill();
    }
  },
  created: function created() {
    this.getRecommendationDetaill();
    // console.table(this.$route.params);
  }
};

/***/ }),

/***/ 458:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(459);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("0624f740", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d9f542a\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./recommendationDetail.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d9f542a\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./recommendationDetail.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 459:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-5d9f542a] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐列表")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐用户信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 3 } }, [
                _c("img", {
                  staticClass: "br",
                  attrs: {
                    src: _vm.userData.headImgUrl,
                    width: "100",
                    height: "100",
                    alt: "用户头像"
                  }
                })
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 8 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("用户名称：" + _vm._s(_vm.userData.nickName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("用户编号：" + _vm._s(_vm.userData.channelUserCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "用户性别：" + _vm._s(_vm._f("changeSex")(_vm.userData.sex))
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "用户所在地：" +
                      _vm._s(_vm.userData.province || "") +
                      _vm._s(_vm.userData.city || "")
                  )
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 8 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("用户手机号：" + _vm._s(_vm.userData.phone))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "所属机构：" + _vm._s(_vm.userData.channelOrganizationName)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "机构编号：" + _vm._s(_vm.userData.channelOrganizationCode)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("佣金等级：" + _vm._s(_vm.userData.levelName))
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐商品信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品名称：" + _vm._s(_vm.goodsData.goodsName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品编号：" + _vm._s(_vm.goodsData.goodsCode))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("所属店铺：" + _vm._s(_vm.goodsData.storeName))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v("商品品牌：" + _vm._s(_vm.goodsData.goodsBrandCnName))
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "商品分类：" + _vm._s(_vm.goodsData.goodsCategoryNames)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "商品价格（元）：" + _vm._s(_vm.goodsData.sellingPrice)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "佣金比例：" +
                      _vm._s(
                        _vm.goodsData.commisionRate
                          ? _vm.goodsData.commisionRate + "%"
                          : ""
                      )
                  )
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("推荐信息")]),
          _vm._v(" "),
          _c(
            "el-row",
            { attrs: { gutter: 20 } },
            [
              _c("el-col", { attrs: { span: 11 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "推荐渠道：" +
                      _vm._s(
                        _vm.recommendationData.modeType
                          ? _vm.recommendationData.modeType +
                            "  " +
                            _vm.recommendationData.modeValue
                          : ""
                      )
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "点击次数：" +
                      _vm._s(_vm.recommendationData.goodsClickCount)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "付款笔数：" + _vm._s(_vm.recommendationData.grossDealCount)
                  )
                ])
              ]),
              _vm._v(" "),
              _c("el-col", { attrs: { span: 13 } }, [
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "成交预估收入（元）：" +
                      _vm._s(_vm.recommendationData.preDealAmount)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "结算预估收入（元）：" +
                      _vm._s(_vm.recommendationData.preSettleAmount)
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "p10" }, [
                  _vm._v(
                    "推荐时间：" + _vm._s(_vm.recommendationData.createTime)
                  )
                ])
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("点击用户")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData1, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号", width: "55" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "nickName", label: "用户昵称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sex", label: "用户性别" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n            " +
                            _vm._s(_vm._f("changeSex")(scope.row.sex)) +
                            "\n          "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "所在地" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n            " +
                            _vm._s(scope.row.province + scope.row.city) +
                            "\n          "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "createTime", label: "点击时间" }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt20" },
            [
              _c("el-button", { attrs: { type: "text" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "fr" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.query1.pageNum,
                      "page-sizes": [20, 50, 100, 200],
                      "page-size": _vm.query1.pageSize,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total1
                    },
                    on: {
                      "size-change": _vm.handleSizeChange1,
                      "current-change": _vm.handleCurrentChange1
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content  mb20" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("付款用户")]),
          _vm._v(" "),
          _c(
            "el-table",
            {
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData2, "tooltip-effect": "dark" }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号", width: "55" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "nickName", label: "用户昵称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "channelUserCode", label: "用户编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "buyInfo", label: "购买信息" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "tv1", label: "付款金额（元）" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "tv1", label: "成交预估收入（元）" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "orderSn", label: "订单编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "orderStatus", label: "订单状态" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "createTime", label: "订单创建时间" }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt20" },
            [
              _c("el-button", { attrs: { type: "text" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "fr" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.query2.pageNum,
                      "page-sizes": [20, 50, 100, 200],
                      "page-size": _vm.query2.pageSize,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total2
                    },
                    on: {
                      "size-change": _vm.handleSizeChange2,
                      "current-change": _vm.handleCurrentChange2
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.goBack } },
        [_vm._v("返 回")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5d9f542a", esExports)
  }
}

/***/ })

});