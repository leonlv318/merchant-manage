webpackJsonp([5],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6cec75c3_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddStaff_vue__ = __webpack_require__(448);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(446)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6cec75c3"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddStaff_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6cec75c3_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddStaff_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\shop\\AddStaff.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6cec75c3", Component.options)
  } else {
    hotAPI.reload("data-v-6cec75c3", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      imgApi: this.$imgApi, //图片上传api
      val1: "",
      val2: "",
      options1: [{
        value: "1",
        label: "启用"
      }, {
        value: "2",
        label: "停用"
      }],
      options2: [{
        value: "0",
        label: "女"
      }, {
        value: "1",
        label: "男"
      }],
      upImgs: [],
      ruleForm: {
        userName: "",
        userPassword: "",
        employeeName: "",
        position: "",
        phone: "",
        headImage: "",
        remark: "",
        userStatus: "",
        sex: ""
      },
      rules: {
        // name: [
        //   { required: true, message: "请输入内容", trigger: "blur" }
        // { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'blur' }
        // ],
        // user: [{ required: true, message: "请输入内容", trigger: "blur" }],
        // password: [{ required: true, message: "请输入内容", trigger: "blur" }],
        // remark: [{ required: true, message: "请输入内容", trigger: "blur" }],
        // sort: [{ required: true, message: "请选择内容", trigger: "change" }],
        // status: [{ required: true, message: "请输入内容", trigger: "blur" }],
        // job: [{ required: true, message: "请输入内容", trigger: "blur" }]
      },
      dialogImageUrl: "",
      dialogVisible: false
    };
  },

  methods: {
    /** @function 保存 */
    save: function save() {
      var _this = this;

      console.log(this.ruleForm);
      this.$http.post(this.$api.addStoreEmployee, this.ruleForm).then(function (rsp) {
        _this.$isot(rsp); //登录超时处理函数
        if (rsp.data.flag) {
          console.log(rsp.data);
          _this.$message.success("保存成功！");
          //  保存成功后跳转
          _this.$router.go(-1);
        } else {
          _this.$message.error("数据未填写完毕或出错，请重新检查！");
        }
      });
    },

    /**@function 取消 */
    cancel: function cancel() {
      var _this2 = this;

      this.$confirm("取消后将放弃保存，返回上一页面，请确认是否取消？", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        _this2.$message({
          type: "success",
          message: "取消成功!"
        });
        _this2.$router.go(-1);
      }).catch(function () {
        _this2.$message({
          type: "info",
          message: "放弃 [取消] 操作!"
        });
      });
    },

    /**@function 图片 */
    handleRemove: function handleRemove(file, fileList) {
      console.log(file, fileList);
    },

    /**@function 图片上传成功事件 */
    uploadImgHandler: function uploadImgHandler(data) {
      console.log("图片上传成功事件");
      // console.log(data);
      this.ruleForm.headImage = data.data[0].fileUrl;
      console.log(this.ruleForm.headImage);
    },
    selectVal1: function selectVal1() {
      this.ruleForm.userStatus = this.val1;
    },
    selectVal2: function selectVal2() {
      this.ruleForm.sex = this.val2;
    }
  }
};

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(447);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("44e61b4c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6cec75c3\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddStaff.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6cec75c3\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddStaff.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-6cec75c3] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n.content1[data-v-6cec75c3] {\n  margin-bottom: 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("填写职员信息")]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "ruleForm",
              attrs: {
                model: _vm.ruleForm,
                rules: _vm.rules,
                "label-width": "100px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员姓名", prop: "employeeName" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.employeeName,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "employeeName", $$v)
                              },
                              expression: "ruleForm.employeeName"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 8, offset: 4 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员账号", prop: "userName" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.userName,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "userName", $$v)
                              },
                              expression: "ruleForm.userName"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "联系方式", prop: "phone" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.phone,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "phone", $$v)
                              },
                              expression: "ruleForm.phone"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 8, offset: 4 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "登录密码", prop: "userPassword" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.userPassword,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "userPassword", $$v)
                              },
                              expression: "ruleForm.userPassword"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员职位", prop: "position" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.ruleForm.position,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "position", $$v)
                              },
                              expression: "ruleForm.position"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 8, offset: 4 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员备注", prop: "remark" } },
                        [
                          _c("el-input", {
                            attrs: {
                              type: "textarea",
                              rows: 2,
                              placeholder: "请输入内容"
                            },
                            model: {
                              value: _vm.ruleForm.remark,
                              callback: function($$v) {
                                _vm.$set(_vm.ruleForm, "remark", $$v)
                              },
                              expression: "ruleForm.remark"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 8 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员头像" } },
                        [
                          _c(
                            "el-upload",
                            {
                              attrs: {
                                action: _vm.imgApi,
                                "list-type": "picture-card",
                                "on-success": _vm.uploadImgHandler,
                                "file-list": _vm.upImgs,
                                "on-remove": _vm.handleRemove,
                                limit: 1
                              }
                            },
                            [
                              _c("i", { staticClass: "el-icon-plus" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "el-upload__tip orange-txt",
                                  attrs: { slot: "tip" },
                                  slot: "tip"
                                },
                                [
                                  _vm._v(
                                    "**图片尺寸：200*200px；格式:jpg、png；大小：100KB以内"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 8, offset: 4 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "账号状态", prop: "userStatus" } },
                        [
                          _c(
                            "el-select",
                            {
                              attrs: {
                                clearable: "",
                                filterable: "",
                                placeholder: "选择账号状态"
                              },
                              on: { change: _vm.selectVal1 },
                              model: {
                                value: _vm.val1,
                                callback: function($$v) {
                                  _vm.val1 = $$v
                                },
                                expression: "val1"
                              }
                            },
                            _vm._l(_vm.options1, function(item) {
                              return _c("el-option", {
                                key: item.value,
                                staticClass: "search-select",
                                attrs: { label: item.label, value: item.value }
                              })
                            })
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 8, offset: 4 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "职员性别", prop: "sex" } },
                        [
                          _c(
                            "el-select",
                            {
                              attrs: {
                                clearable: "",
                                filterable: "",
                                placeholder: "选择职员性别"
                              },
                              on: { change: _vm.selectVal2 },
                              model: {
                                value: _vm.val2,
                                callback: function($$v) {
                                  _vm.val2 = $$v
                                },
                                expression: "val2"
                              }
                            },
                            _vm._l(_vm.options2, function(item) {
                              return _c("el-option", {
                                key: item.value,
                                staticClass: "search-select",
                                attrs: { label: item.label, value: item.value }
                              })
                            })
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        {
          staticClass: "purple-bg",
          staticStyle: { "margin-top": "20px" },
          on: { click: _vm.save }
        },
        [_vm._v("保存")]
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.cancel } }, [
        _vm._v("取消")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6cec75c3", esExports)
  }
}

/***/ })

});