webpackJsonp([14],{

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_00133894_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addGoods_vue__ = __webpack_require__(472);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(470)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-00133894"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addGoods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_00133894_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addGoods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\addGoods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-00133894", Component.options)
  } else {
    hotAPI.reload("data-v-00133894", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    },
    changeHasAdd: function changeHasAdd(value) {
      if (value == 1) {
        return "已添加";
      } else {
        return "未添加";
      }
    },
    changeGoodsAudit: function changeGoodsAudit(value) {
      if (value == 2) {
        return "通过";
      } else if (value == 3) {
        return "驳回";
      } else {
        return "未审核";
      }
    }
  },
  data: function data() {
    return {
      showDialog: false,
      val1: "", //搜索商品名称/编号
      val2: [], //选择分类
      val3: "", //选择品牌
      val4: "", //选择店铺
      options2: [
        //分类选项
      ],
      options3: [
        //品牌选项
      ],
      options4: [
      //添加状态选项
      {
        value: "0",
        label: "未添加"
      }, {
        value: "1",
        label: "已添加"
      }],
      multipleSelection: [],
      total: 0,
      pages: 0,
      query: {
        pageNum: 1,
        pageSize: 20,
        name: "",
        goodsCategoryId: "",
        isUp: "",
        goodsAudit: "",
        hasAddChannel: ""
      },
      tableData: []
    };
  },

  methods: {
    /**@function 获取商品列表 */
    getGoodsData: function getGoodsData() {
      var _this = this;

      this.$http.get(this.$api.findGoodsListPage, { params: this.query }).then(function (rsp) {
        console.log("获取商品列表");
        console.log(rsp);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findGoodsCategoryList).then(function (rsp) {
        var orData = rsp.data.data;
        var toget = function toget(orData) {
          var data = orData;
          var arrl = orData.length;
          var arr = [];
          for (var i = 0; i < arrl; i++) {
            //一级
            var obj1 = {};
            obj1.value = data[i].goodsCategoryId;
            obj1.label = data[i].goodsCategoryName;
            if (data[i].childCategoryList) {
              //是否有2级
              obj1.childrentemp = data[i].childCategoryList;
              obj1.children = [];
              for (var j = 0; j < obj1.childrentemp.length; j++) {
                var obj2 = {};
                obj2.children = [];
                obj2.value = obj1.childrentemp[j].goodsCategoryId;
                obj2.label = obj1.childrentemp[j].goodsCategoryName;
                obj1.children.push(obj2);
                // obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                if (obj1.childrentemp[j].childCategoryList) {
                  //是否有3级
                  obj2.childrentemp = obj1.childrentemp[j].childCategoryList;
                  for (var k = 0; k < obj2.childrentemp.length; k++) {
                    var obj3 = {};
                    obj3.value = obj2.childrentemp[k].goodsCategoryId;
                    obj3.label = obj2.childrentemp[k].goodsCategoryName;
                    obj2.children.push(obj3);
                  }
                }
              }
              arr[i] = obj1;
            } else {
              arr[i] = obj1;
            }
          }
          return arr;
        };
        var arr = toget(orData);
        console.log("生成分类数据");
        console.log(arr);
        _this2.options2 = arr;
      });
    },

    /**@function 搜索商品名称/编号/品牌*/
    searchVal1: function searchVal1() {
      this.query.name = this.val1;
      this.getGoodsData();
    },

    /**@function 选择分类 */
    selectVal2: function selectVal2() {
      var l = this.val2.length;
      this.query.goodsCategoryId = this.val2[l - 1];
      this.getGoodsData();
    },

    // /**@function 选择品牌 */
    // selectVal3() {
    //   this.query.isUp = this.val3;
    //   this.getGoodsData();
    // },
    /**@function 选择添加状态 */
    selectVal4: function selectVal4() {
      this.query.hasAddChannel = this.val4;
      this.getGoodsData();
    },


    /**@function 查看*/
    check: function check(row, $index) {
      console.log(row);
      console.log("跳转查看，id ↓");
      //   console.log(row.goodsId);
      //   console.log(row.storeId);
      this.$router.push({
        name: "gc",
        params: { id: row.goodsId, query: this.query }
      });
    },

    /**@function 编辑*/
    edit: function edit(row, $index) {
      console.log(row);
      if (row.hasAddChannel == 0) {
        this.$router.push({
          name: "raa",
          params: { id: row.goodsId, query: this.query }
        });
      } else if (row.hasAddChannel == 1) {
        this.$router.push({
          name: "rae",
          params: { id: row.goodsId, query: this.query }
        });
      } else {
        this.$messege.error("商品出问题啦~暂时无法编辑推荐信息~");
      }
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.go(-1);
    }
  },
  created: function created() {
    this.getGoodsData();
    this.getCategoryData();
  }
};

/***/ }),

/***/ 470:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(471);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("d0062fe6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-00133894\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addGoods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-00133894\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addGoods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 471:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-00133894],\n.search-group[data-v-00133894] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-00133894] {\n  float: right;\n}\n.up-btn[data-v-00133894] {\n  position: relative;\n}\n.file-upload[data-v-00133894] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ 472:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content" },
        [
          _c(
            "div",
            { staticClass: "search-group" },
            [
              _c("el-input", {
                staticClass: "search-input mb10",
                attrs: {
                  clearable: "",
                  placeholder: "搜索商品名称/编号/品牌",
                  "prefix-icon": "el-icon-search"
                },
                on: { blur: _vm.searchVal1 },
                nativeOn: {
                  keyup: function($event) {
                    if (
                      !("button" in $event) &&
                      _vm._k($event.keyCode, "enter", 13, $event.key)
                    ) {
                      return null
                    }
                    _vm.searchVal1($event)
                  }
                },
                model: {
                  value: _vm.val1,
                  callback: function($$v) {
                    _vm.val1 = $$v
                  },
                  expression: "val1"
                }
              }),
              _vm._v(" "),
              _c("el-cascader", {
                staticClass: "mb10",
                attrs: {
                  clearable: "",
                  placeholder: "选择分类",
                  "expand-trigger": "click",
                  options: _vm.options2,
                  "change-on-select": false
                },
                on: { change: _vm.selectVal2 },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              }),
              _vm._v(" "),
              _c(
                "el-select",
                {
                  staticClass: "mb10",
                  attrs: {
                    clearable: "",
                    filterable: "",
                    placeholder: "选择添加状态"
                  },
                  on: { change: _vm.selectVal4 },
                  model: {
                    value: _vm.val4,
                    callback: function($$v) {
                      _vm.val4 = $$v
                    },
                    expression: "val4"
                  }
                },
                _vm._l(_vm.options4, function(item) {
                  return _c("el-option", {
                    key: item.value,
                    attrs: { label: item.label, value: item.value }
                  })
                })
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "multipleTable",
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, "tooltip-effect": "dark" },
              on: { "selection-change": _vm.handleSelectionChange }
            },
            [
              _c("el-table-column", {
                attrs: { type: "index", label: "序号", width: "55" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsName", label: "商品名称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsCode", label: "编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsBrandName", label: "品牌" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "categoryName", label: "分类" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "isUp", label: "销售状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                            "\n        "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "hasAddChannel", label: "添加状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(
                              _vm._f("changeHasAdd")(scope.row.hasAddChannel)
                            ) +
                            "\n        "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsAudit", label: "审核状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n          " +
                            _vm._s(
                              _vm._f("changeGoodsAudit")(scope.row.goodsAudit)
                            ) +
                            "\n        "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作", width: "140" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "purple-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.check(scope.row, scope.$index)
                              }
                            }
                          },
                          [_vm._v("查看")]
                        ),
                        _vm._v(" "),
                        scope.row.hasAddChannel == 0 && scope.row.isUp == 1
                          ? _c(
                              "el-button",
                              {
                                staticClass: "purple-txt",
                                attrs: { type: "text", size: "small" },
                                on: {
                                  click: function($event) {
                                    _vm.edit(scope.row, scope.$index)
                                  }
                                }
                              },
                              [_vm._v("编辑推荐信息")]
                            )
                          : _vm._e()
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt20" },
            [
              _c("el-button", { attrs: { type: "text", size: "small" } }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "pagination" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.query.pageNum,
                      "page-sizes": [20, 50, 100, 200],
                      "page-size": _vm.query.pageSize,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total
                    },
                    on: {
                      "size-change": _vm.handleSizeChange,
                      "current-change": _vm.handleCurrentChange
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.goBack } },
        [_vm._v("返回")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-00133894", esExports)
  }
}

/***/ })

});