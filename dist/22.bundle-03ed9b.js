webpackJsonp([22],{

/***/ 249:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue__ = __webpack_require__(302);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c8bdbcf8_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_manageOfAssort_vue__ = __webpack_require__(367);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(365)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c8bdbcf8"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_manageOfAssort_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c8bdbcf8_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_manageOfAssort_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\manageOfAssort.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c8bdbcf8", Component.options)
  } else {
    hotAPI.reload("data-v-c8bdbcf8", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "显示";
      } else {
        return "隐藏";
      }
    }
  },
  data: function data() {
    return {
      shop_id: '',
      Num: "",
      asortName: "",
      showState: "",
      sortNum: "",
      val1: "",
      val2: "",
      options1: [],
      options2: [{
        label: "显示",
        value: 1
      }, {
        label: "隐藏",
        value: 0
      }],
      tableData: [],
      query: {
        pageNum: 1,
        pageSize: 20
      },
      total: 0,
      currentPage: 1
    };
  },

  methods: {
    // 获取分类列表
    getCategoryListPage: function getCategoryListPage() {
      var _this = this;

      this.$http.get(this.$api.findCategoryListPage, {
        params: { shop_id: this.shop_id }
      }).then(function (rsp) {
        console.log("获取分类列表");
        console.log(rsp);
        _this.tableData = rsp.data.data.category_list;
      });
    },

    /**@function 搜索商户分类id */
    selectVal1: function selectVal1() {
      this.query.storeCategoryId = this.val1;
      this.getStoreCategoryListPage();
    },

    /**@function 选择是否上线 */
    selectVal2: function selectVal2() {
      console.log(this.val2);
      this.query.isUp = this.val2;
      this.getStoreCategoryListPage();
    },

    /**@function 选择行 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(pageSize) {
      this.query.pageSize = pageSize;
      this.getGoodsData();
    },

    /**@function 分页 */
    handleCurrentChange: function handleCurrentChange(pageNum) {
      this.query.pageNum = pageNum;
      this.getGoodsData();
    },


    // 新增分类
    addCategory: function addCategory() {
      this.$router.push({ name: 'aa' });
    },

    // 修改分类
    editCategory: function editCategory(row) {
      console.log(row);
      this.$router.push({
        name: "aa",
        params: {
          category_id: row.category_id
        }
      });
    },


    // 删除分类
    deleteCategory: function deleteCategory(row) {
      var _this2 = this;

      console.log(row);
      this.$confirm('确认删除此分类?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(function () {
        var postData = _this2.$qs.stringify({
          c_id: row.category_id,
          shop_id: _this2.shop_id
        });
        _this2.$http.post(_this2.$api.deleteCategory, postData).then(function (rsp) {
          console.log('删除分类');
          console.log(rsp);
          if (rsp.data.result == 0) {
            _this2.$message.success('删除成功');
            _this2.getCategoryListPage();
          } else {
            _this2.$message.error('删除失败');
          }
        });
      }).catch(function () {
        _this2.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    }
  },
  created: function created() {
    console.log('shop_id');
    console.log(window.sessionStorage.getItem('shop_id'));
    var shop_id = window.sessionStorage.getItem('shop_id');
    this.shop_id = shop_id;
    this.getCategoryListPage();
  }
};

/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(366);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("7a667304", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c8bdbcf8\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./manageOfAssort.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c8bdbcf8\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./manageOfAssort.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 366:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-c8bdbcf8],\n.search-group[data-v-c8bdbcf8] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-c8bdbcf8] {\n  float: right;\n}\n.up-btn[data-v-c8bdbcf8] {\n  position: relative;\n}\n.file-upload[data-v-c8bdbcf8] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n.select_width .el-cascader[data-v-c8bdbcf8] {\n  width: 240px;\n}\n", ""]);

// exports


/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("商品管理")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content" },
      [
        _c(
          "div",
          { staticClass: "btn-group" },
          [
            _c(
              "el-button",
              {
                staticClass: "purple-bg",
                attrs: { icon: "el-icon-plus" },
                on: { click: _vm.addCategory }
              },
              [_vm._v("新增分类")]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "search-group select_width" },
          [
            _c(
              "el-select",
              {
                staticClass: "search-select mb10",
                attrs: {
                  clearable: "",
                  filterable: "",
                  placeholder: "选择显示状态"
                },
                on: { change: _vm.selectVal2 },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = $$v
                  },
                  expression: "val2"
                }
              },
              _vm._l(_vm.options2, function(item) {
                return _c("el-option", {
                  key: item.value,
                  attrs: { label: item.label, value: item.value }
                })
              })
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            ref: "multipleTable",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
          },
          [
            _c("el-table-column", {
              attrs: { type: "index", label: "序号", width: "200" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "name", label: "分类名称" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "image", label: "分类图标" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("img", {
                        attrs: { src: scope.row.image, alt: "", width: "100px" }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "index", sortable: "", label: "排序号" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c("el-input", {
                        staticStyle: { width: "80px" },
                        attrs: { disabled: "", placeholder: "999" },
                        on: {
                          blur: function($event) {
                            _vm.changeSort(scope.row)
                          }
                        },
                        nativeOn: {
                          keyup: function($event) {
                            if (
                              !("button" in $event) &&
                              _vm._k($event.keyCode, "enter", 13, $event.key)
                            ) {
                              return null
                            }
                            _vm.changeSort(scope.row)
                          }
                        },
                        model: {
                          value: scope.row.index,
                          callback: function($$v) {
                            _vm.$set(scope.row, "index", $$v)
                          },
                          expression: "scope.row.index"
                        }
                      })
                    ]
                  }
                }
              ])
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { label: "操作", width: "140" },
              scopedSlots: _vm._u([
                {
                  key: "default",
                  fn: function(scope) {
                    return [
                      _c(
                        "el-button",
                        {
                          staticClass: "purple-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.editCategory(scope.row)
                            }
                          }
                        },
                        [_vm._v("修改")]
                      ),
                      _vm._v(" "),
                      _c(
                        "el-button",
                        {
                          staticClass: "orange-txt",
                          attrs: { type: "text", size: "small" },
                          on: {
                            click: function($event) {
                              _vm.deleteCategory(scope.row)
                            }
                          }
                        },
                        [_vm._v("删除")]
                      )
                    ]
                  }
                }
              ])
            })
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c8bdbcf8", esExports)
  }
}

/***/ })

});