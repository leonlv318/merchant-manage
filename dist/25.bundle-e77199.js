webpackJsonp([25],{

/***/ 253:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue__ = __webpack_require__(306);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_14b4faaf_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addSortGoods_vue__ = __webpack_require__(379);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(377)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-14b4faaf"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSortGoods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_14b4faaf_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addSortGoods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\addSortGoods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-14b4faaf", Component.options)
  } else {
    hotAPI.reload("data-v-14b4faaf", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changehasAdd: function changehasAdd(value) {
      if (value == 1) {
        return "已添加";
      } else {
        return "未添加";
      }
    }
  },
  data: function data() {
    return {
      query: {
        pageNum: 1,
        pageSize: 10
      },
      total: 0,
      val0: "",
      val1: "",
      val2: "",
      val3: [],
      val4: "",
      options0: [],
      options2: [],
      options3: [],
      options4: [{
        value: "1",
        label: "已添加"
      }, {
        value: "0",
        label: "未添加"
      }],
      currentPage: 1,
      tableData: [],
      addStr: ""
    };
  },

  methods: {
    // 获取分类商品添加商品列表
    getStoreCategoryGoodsAddListPage: function getStoreCategoryGoodsAddListPage() {
      var _this = this;

      this.$http.get(this.$api.findStoreCategoryGoodsAddListPage, {
        params: this.query
      }).then(function (rsp) {
        console.log("获取分类商品添加商品列表");
        console.log(rsp);
        _this.tableData = rsp.data.data.list;
        _this.total = rsp.data.data.total;
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findStoreCategoryList, {
        params: this.query
      }).then(function (rsp) {
        console.log("获取分类下拉列表");
        console.log(rsp);
        console.log(rsp.data.data);
        var data = rsp.data.data;
        var list = [];
        for (var i in data) {
          list.push({
            label: data[i].storeCategoryName,
            value: data[i].storeCategoryId
          });
        }
        _this2.options3 = list;
      });
    },

    // 获取选择新增商品所属分类
    getAllChannelCategory: function getAllChannelCategory() {
      var _this3 = this;

      this.$http.get(this.$api.findStoreCategoryList, {
        params: this.query
      }).then(function (rsp) {
        console.log("获取选择新增商品所属分类");
        console.log(rsp);
        console.log(rsp.data.data);
        var data = rsp.data.data;
        var list = [];
        for (var i in data) {
          list.push({
            label: data[i].storeCategoryName,
            value: data[i].storeCategoryId
          });
        }
        _this3.options0 = list;
      });
    },

    // 选择选择新增商品所属分类
    selectVal0: function selectVal0() {
      console.log(this.val0);
    },

    /**@function 搜索名称/编号/品牌 */
    searchVal1: function searchVal1() {
      console.log(this.val1);
      this.query.searchName = this.val1;
      this.getStoreCategoryGoodsAddListPage();
    },

    // 搜索分类
    selectVal3: function selectVal3() {
      this.query.goodsCategoryId = this.val3;
      this.getStoreCategoryGoodsAddListPage();
    },

    /**@function 选择添加状态 */
    selectVal4: function selectVal4() {
      console.log(this.val4);
      this.query.hasAdd = this.val4;
      this.getStoreCategoryGoodsAddListPage();
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
      console.log(this.multipleSelection);
      var str = "";
      for (var i in this.multipleSelection) {
        str = str + this.multipleSelection[i].goodsId + ",";
      }
      str = str.substring(0, str.length - 1);
      console.log(str);
      this.addStr = str;
    },
    handleSizeChange: function handleSizeChange(val) {
      console.log("\u6BCF\u9875 " + val + " \u6761");
      this.query.pageSize = val;
      this.getStoreCategoryGoodsAddListPage();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      console.log("\u5F53\u524D\u9875: " + val);
      this.query.pageNum = val;
      this.getStoreCategoryGoodsAddListPage();
    },

    // 查看详情
    checkOrganization: function checkOrganization(row) {
      this.$router.push({
        name: "aagd",
        params: {
          goodsId: row.goodsId,
          storeCategoryId: row.storeCategoryId,
          storeGoodsCategoryId: row.storeGoodsCategoryId
        }
      });
    },

    // 添加
    add: function add(row, index) {
      var _this4 = this;

      this.$confirm("确认添加?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        if (_this4.val0) {
          _this4.$http.get(_this4.$api.insertStoreGoodsCategoryGoods, {
            params: {
              storeCategoryId: _this4.val0,
              goodsIds: row.goodsId
            }
          }).then(function (rsp) {
            console.log("获取添加状态");
            console.log(rsp);
            if (rsp.data.flag == true) {
              _this4.$message({
                type: "success",
                message: "添加成功。"
              });
              _this4.getStoreCategoryGoodsAddListPage();
            } else {
              _this4.$message({
                type: "error",
                message: "添加失败。"
              });
            }
          });
        } else {
          _this4.$message({
            type: "error",
            message: "选择添加分类"
          });
        }
      }).catch(function () {
        _this4.$message({
          type: "info",
          message: "已取消添加"
        });
      });
    },

    // 批量添加
    AddSelect: function AddSelect() {
      var _this5 = this;

      this.$confirm("确认添加?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning"
      }).then(function () {
        if (_this5.val0) {
          _this5.$http.get(_this5.$api.insertStoreGoodsCategoryGoods, {
            params: {
              storeCategoryId: _this5.val0,
              goodsIds: _this5.addStr
            }
          }).then(function (rsp) {
            console.log("获取添加状态");
            console.log(rsp);
            if (rsp.data.flag == true) {
              _this5.$message({
                type: "success",
                message: "添加成功。"
              });

              _this5.getStoreCategoryGoodsAddListPage();
            } else {
              _this5.$message({
                type: "error",
                message: "添加失败。"
              });
            }
          });
        } else {
          _this5.$message({
            type: "error",
            message: "选择添加分类"
          });
        }
      }).catch(function () {
        _this5.$message({
          type: "info",
          message: "已取消添加"
        });
      });
    },
    goBack: function goBack() {
      this.$router.push("/assort/goods");
    }
  },
  created: function created() {
    this.getStoreCategoryGoodsAddListPage();
    this.getCategoryData();
    this.getAllChannelCategory();
  }
};

/***/ }),

/***/ 377:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(378);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("4a547084", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14b4faaf\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addSortGoods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14b4faaf\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addSortGoods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-14b4faaf],\n.search-group[data-v-14b4faaf] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-14b4faaf] {\n  float: right;\n}\n.content .content-title[data-v-14b4faaf] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1 addcarousel" },
        [
          _c("div", { staticClass: "content-title" }, [
            _vm._v("选择新增商品所属分类")
          ]),
          _vm._v(" "),
          _c(
            "el-select",
            {
              attrs: {
                filterable: "",
                placeholder: "选择新增商品所属分类",
                clearable: ""
              },
              on: { change: _vm.selectVal0 },
              model: {
                value: _vm.val0,
                callback: function($$v) {
                  _vm.val0 = $$v
                },
                expression: "val0"
              }
            },
            _vm._l(_vm.options0, function(item) {
              return _c("el-option", {
                key: item.value,
                staticClass: "search-select",
                attrs: { label: item.label, value: item.value }
              })
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1 addcarousel" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("选择商品范围")]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "search-group" },
            [
              _c("el-input", {
                staticClass: "search-input",
                attrs: {
                  placeholder: "搜索商品名称 / 编号 / 品牌",
                  "prefix-icon": "el-icon-search",
                  clearable: ""
                },
                on: { blur: _vm.searchVal1 },
                nativeOn: {
                  keyup: function($event) {
                    if (
                      !("button" in $event) &&
                      _vm._k($event.keyCode, "enter", 13, $event.key)
                    ) {
                      return null
                    }
                    _vm.searchVal1($event)
                  }
                },
                model: {
                  value: _vm.val1,
                  callback: function($$v) {
                    _vm.val1 = typeof $$v === "string" ? $$v.trim() : $$v
                  },
                  expression: "val1"
                }
              }),
              _vm._v(" "),
              _c(
                "el-select",
                {
                  staticClass: "search-select mb10",
                  attrs: {
                    clearable: "",
                    filterable: "",
                    placeholder: "选择店铺分类"
                  },
                  on: { change: _vm.selectVal3 },
                  model: {
                    value: _vm.val3,
                    callback: function($$v) {
                      _vm.val3 = $$v
                    },
                    expression: "val3"
                  }
                },
                _vm._l(_vm.options3, function(item) {
                  return _c("el-option", {
                    key: item.value,
                    attrs: { label: item.label, value: item.value }
                  })
                })
              ),
              _vm._v(" "),
              _c(
                "el-select",
                {
                  attrs: {
                    filterable: "",
                    placeholder: "选择添加状态",
                    clearable: ""
                  },
                  on: { change: _vm.selectVal4 },
                  model: {
                    value: _vm.val4,
                    callback: function($$v) {
                      _vm.val4 = $$v
                    },
                    expression: "val4"
                  }
                },
                _vm._l(_vm.options4, function(item) {
                  return _c("el-option", {
                    key: item.value,
                    staticClass: "search-select",
                    attrs: { label: item.label, value: item.value }
                  })
                })
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "singleTable",
              staticStyle: { width: "100%" },
              attrs: {
                data: _vm.tableData,
                "tooltip-effect": "dark",
                "default-sort": { prop: "sort_no", order: "descending" }
              },
              on: { "selection-change": _vm.handleSelectionChange }
            },
            [
              _c("el-table-column", {
                attrs: { type: "selection", width: "55" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsName", label: "商品名称", width: "180px" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsImageUrl", label: "图片", width: "180px" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          attrs: { src: scope.row.goodsImageUrl, alt: "" }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsBrandName", label: "品牌", width: "180px" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "categoryName", label: "分类", width: "180px" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: {
                  prop: "originalPrice",
                  label: "原价（元）",
                  width: "180px"
                }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: {
                  prop: "sellingPrice",
                  label: "销售价（元）",
                  width: "180px"
                }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: {
                  prop: "inventory",
                  label: "库存量（件）",
                  width: "180px"
                }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "hasAdd", label: "添加状态", width: "150px" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n            " +
                            _vm._s(_vm._f("changehasAdd")(scope.row.hasAdd)) +
                            "\n        "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "purple-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.checkOrganization(scope.row)
                              }
                            }
                          },
                          [_vm._v("查看")]
                        ),
                        _vm._v(" "),
                        scope.row.hasAdd == 0
                          ? _c(
                              "el-button",
                              {
                                staticClass: "purple-txt",
                                attrs: { type: "text", size: "small" },
                                on: {
                                  click: function($event) {
                                    _vm.add(scope.row, scope.$index)
                                  }
                                }
                              },
                              [_vm._v("添加")]
                            )
                          : _vm._e()
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { "margin-top": "20px" } },
            [
              _c(
                "el-button",
                { staticClass: "purple-bg", on: { click: _vm.AddSelect } },
                [_vm._v("批量添加")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "pagination" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "current-page": _vm.currentPage,
                      "page-sizes": [10, 20, 50, 100],
                      "page-size": 10,
                      layout: "total, sizes, prev, pager, next, jumper",
                      total: _vm.total
                    },
                    on: {
                      "size-change": _vm.handleSizeChange,
                      "current-change": _vm.handleCurrentChange
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.goBack } }, [
        _vm._v("返回")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-14b4faaf", esExports)
  }
}

/***/ })

});