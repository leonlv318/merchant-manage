webpackJsonp([30],{

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_e62830c2_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_multipleAddCarouselGoods_vue__ = __webpack_require__(415);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(413)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-e62830c2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_multipleAddCarouselGoods_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_e62830c2_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_multipleAddCarouselGoods_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\carousel\\multipleAddCarouselGoods.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e62830c2", Component.options)
  } else {
    hotAPI.reload("data-v-e62830c2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "上架";
      } else {
        return "下架";
      }
    }
  },
  data: function data() {
    return {
      query: {
        pageNum: 1,
        pageSize: 20,
        hasAdd: 0
      },
      total: 0,
      val1: "",
      val2: "",
      val3: "",
      val4: [],
      val5: "",
      val6: 0,
      multipleSelection: [],
      options3: [],
      options4: [],
      options5: [{
        value: 1,
        label: "上架"
      }, {
        value: 0,
        label: "下架"
      }],
      options6: [{
        value: 1,
        label: "已添加"
      }, {
        value: 0,
        label: "未添加"
      }],
      adImageId: "",
      tableData: [],
      currentPage: 1
    };
  },

  methods: {
    /**@function 添加商品 */
    addGoods: function addGoods(row) {
      var _this = this;

      console.log(row);
      this.$http.get(this.$api.addStoreAdImageOneGoods, {
        params: { goodsIds: row.goodsId, storeAdImageId: this.adImageId }
      }).then(function (rsp) {
        console.log("添加轮播图商品");
        console.log(rsp);
        if (rsp.data.flag) {
          _this.$message({
            type: "success",
            message: "添加商品成功"
          });
          _this.getAdImageGoodsList();
        } else {
          _this.$message({
            type: "error",
            message: rsp.data.msg
          });
        }
      });
    },

    /**@function 获取分类下拉列表 */
    getCategoryData: function getCategoryData() {
      var _this2 = this;

      this.$http.get(this.$api.findGoodsCategoryList).then(function (rsp) {
        console.log("获取分类下拉列表");
        console.log(rsp.data);
        var orData = rsp.data.data;

        var toget = function toget(orData) {
          var data = orData;
          var arrl = orData.length;
          var arr = [];
          for (var i = 0; i < arrl; i++) {
            //一级
            var obj1 = {};
            obj1.value = data[i].id;
            obj1.label = data[i].name;
            if (data[i].children != null) {
              //有2级
              obj1.childrentemp = data[i].children;
              obj1.children = [];
              for (var j = 0; j < obj1.childrentemp.length; j++) {
                var obj2 = {};
                obj2.value = obj1.childrentemp[j].id;
                obj2.label = obj1.childrentemp[j].name;
                obj1.children.push(obj2);
                if (obj1.childrentemp[j].children != null) {
                  //有3级
                  obj2.children = [];
                  obj2.childrentemp = obj1.childrentemp[j].children;
                  for (var k = 0; k < obj2.childrentemp.length; k++) {
                    var obj3 = {};
                    obj3.value = obj2.childrentemp[k].id;
                    obj3.label = obj2.childrentemp[k].name;
                    obj2.children.push(obj3);
                  }
                } else {
                  //没有3级
                }
              }
              arr[i] = obj1;
            } else {
              //没有2级
              arr[i] = obj1;
            }
          }
          return arr;
        };
        var arr = toget(orData);
        console.log("生成分类数据");
        console.log(arr);
        _this2.options4 = arr;
      });
    },

    /**@function 搜索商品名称/编号/品牌 */
    searchVal2: function searchVal2() {
      console.log(this.val2);
      this.query.name = this.val2;
      this.getAdImageGoodsList();
    },

    /**@function 选择店铺 */
    selectVal3: function selectVal3() {
      this.query.storeId = this.val3;
      this.getAdImageGoodsList();
    },

    /**@function 选择分类 */
    selectVal4: function selectVal4() {
      console.log(this.val4);
      var l = this.val4.length;
      this.query.goodsCategoryId = this.val4[l - 1];
      this.getAdImageGoodsList();
    },

    /**@function 选择销售状态 */
    selectVal5: function selectVal5() {
      console.log(this.val5);
      this.query.isUp = this.val5;
      this.getAdImageGoodsList();
    },

    // 选择添加状态
    // selectVal6() {
    //     console.log(this.val6);
    //     this.query.hasAdd = this.val6;
    //     this.getHotGoodsList();
    // },

    /**@function 批量添加 */
    addSelect: function addSelect() {
      var _this3 = this;

      if (this.multipleSelection.length != 0) {
        this.$confirm("是否添加选中的商品？", "提示", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function () {
          var goodsIds = "";
          _this3.multipleSelection.forEach(function (o, i) {
            goodsIds = o.goodsId + "," + goodsIds;
          });
          _this3.$http.get(_this3.$api.addStoreAdImageOneGoods, {
            params: { goodsIds: goodsIds, storeAdImageId: _this3.adImageId }
          }).then(function (rsp) {
            console.log("添加轮播图商品");
            console.log(rsp);
            if (rsp.data.flag) {
              _this3.$message({
                type: "success",
                message: "添加商品成功!"
              });
              _this3.getAdImageGoodsList();
            } else {
              _this3.$message({
                type: "error",
                message: rsp.data.msg
              });
            }
          });
        }).catch(function () {
          _this3.$message({
            type: "info",
            message: "已取消"
          });
        });
      } else {
        this.$message({
          type: "error",
          message: "当前未选中任何行！"
        });
      }
    },

    /**@function 查看商品 */
    checkgoods: function checkgoods(row, $index) {
      console.log(row);
      console.log($index);
      this.$router.push({
        name: "gc",
        params: {
          id: row.goodsId
        }
      });
    },

    /**@function 选择赋值 */
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
    },

    /**@function 分页 */
    handleSizeChange: function handleSizeChange(val) {
      console.log("\u6BCF\u9875 " + val + " \u6761");
      this.query.pageSize = val;
      this.getAdImageGoodsList();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      console.log("\u5F53\u524D\u9875: " + val);
      this.query.pageNum = val;
      this.getAdImageGoodsList();
    },

    /**@function 返回 */
    goBack: function goBack() {
      this.$router.push({
        name: "cck",
        params: { id: this.adImageId }
      });
    },

    //  获取轮播图商品列表
    getAdImageGoodsList: function getAdImageGoodsList() {
      var _this4 = this;

      var self = this;
      this.$http.get(this.$api.findAdImageGoodsPageList, { params: this.query }).then(function (rsp) {
        console.log("轮播图商品列表");
        console.log(rsp);

        _this4.tableData = rsp.data.data.list;
        _this4.total = rsp.data.data.total;
      });
    }
  },
  created: function created() {
    var adImageId = this.$route.params.adImageId;
    console.log("轮播图ID");
    console.log(adImageId);
    this.query.adImageId = adImageId;
    this.adImageId = adImageId;
    this.getAdImageGoodsList();
    this.getCategoryData();
  }
};

/***/ }),

/***/ 413:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(414);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("58ba9e7e", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e62830c2\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./multipleAddCarouselGoods.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e62830c2\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./multipleAddCarouselGoods.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 414:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-e62830c2],\n.search-group[data-v-e62830c2] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-e62830c2] {\n  float: right;\n}\n.up-btn[data-v-e62830c2] {\n  position: relative;\n}\n.file-upload[data-v-e62830c2] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ 415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("小程序管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content" },
        [
          _c(
            "div",
            { staticClass: "search-group" },
            [
              _c("el-input", {
                staticClass: "search-input",
                attrs: {
                  clearable: "",
                  placeholder: "搜索商品名称/编号/品牌",
                  "prefix-icon": "el-icon-search"
                },
                on: { blur: _vm.searchVal2 },
                nativeOn: {
                  keyup: function($event) {
                    if (
                      !("button" in $event) &&
                      _vm._k($event.keyCode, "enter", 13, $event.key)
                    ) {
                      return null
                    }
                    _vm.searchVal2($event)
                  }
                },
                model: {
                  value: _vm.val2,
                  callback: function($$v) {
                    _vm.val2 = typeof $$v === "string" ? $$v.trim() : $$v
                  },
                  expression: "val2"
                }
              }),
              _vm._v(" "),
              _c("el-cascader", {
                staticClass: "mb10",
                attrs: {
                  clearable: "",
                  placeholder: "选择分类",
                  "expand-trigger": "click",
                  options: _vm.options4,
                  "change-on-select": ""
                },
                on: { change: _vm.selectVal4 },
                model: {
                  value: _vm.val4,
                  callback: function($$v) {
                    _vm.val4 = $$v
                  },
                  expression: "val4"
                }
              }),
              _vm._v(" "),
              _c(
                "el-select",
                {
                  attrs: {
                    clearable: "",
                    filterable: "",
                    placeholder: "选择销售状态"
                  },
                  on: { change: _vm.selectVal5 },
                  model: {
                    value: _vm.val5,
                    callback: function($$v) {
                      _vm.val5 = $$v
                    },
                    expression: "val5"
                  }
                },
                _vm._l(_vm.options5, function(item, index) {
                  return _c("el-option", {
                    key: index,
                    staticClass: "search-select",
                    attrs: { label: item.label, value: item.value }
                  })
                })
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-table",
            {
              ref: "multipleTable",
              staticStyle: { width: "100%" },
              attrs: { data: _vm.tableData, "tooltip-effect": "dark" },
              on: { "selection-change": _vm.handleSelectionChange }
            },
            [
              _c("el-table-column", { attrs: { type: "selection" } }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsName", label: "名称" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "imageUrl", label: "图片", width: "70" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("img", {
                          attrs: {
                            src: scope.row.imageUrl,
                            width: "52",
                            height: "52"
                          }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsCode", label: "编号" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "storeName", label: "店铺" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsBrandName", label: "品牌" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "categoryName", label: "分类" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "originalPrice", label: "原价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "sellingPrice", label: "销售价(元)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "inventory", label: "库存量(件)" }
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { prop: "goodsResource.isUp", label: "销售状态" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _vm._v(
                          "\n                    " +
                            _vm._s(_vm._f("changeIsUp")(scope.row.isUp)) +
                            "\n                "
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { label: "操作" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c(
                          "el-button",
                          {
                            staticClass: "purple-txt",
                            attrs: { type: "text", size: "small" },
                            on: {
                              click: function($event) {
                                _vm.checkgoods(scope.row, scope.$index)
                              }
                            }
                          },
                          [_vm._v("查看")]
                        ),
                        _vm._v(" "),
                        scope.row.isStoreAdGoods == 0
                          ? _c(
                              "el-button",
                              {
                                staticClass: "orange-txt",
                                attrs: { type: "text", size: "small" },
                                on: {
                                  click: function($event) {
                                    _vm.addGoods(scope.row)
                                  }
                                }
                              },
                              [_vm._v("添加")]
                            )
                          : _vm._e()
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticStyle: { "margin-top": "20px" } },
            [
              _c(
                "el-button",
                { staticClass: "orange-bg", on: { click: _vm.addSelect } },
                [_vm._v("批量添加")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "pagination" },
                [
                  _c("el-pagination", {
                    attrs: {
                      "page-sizes": [20, 30, 50, 100],
                      layout: "total, sizes, prev, pager, next, jumper",
                      "page-size": _vm.query.pageSize,
                      "current-page": _vm.query.pageNum,
                      total: _vm.total
                    },
                    on: {
                      "size-change": _vm.handleSizeChange,
                      "current-change": _vm.handleCurrentChange
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        { staticClass: "orange-bg mt20", on: { click: _vm.goBack } },
        [_vm._v("返回")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e62830c2", esExports)
  }
}

/***/ })

});