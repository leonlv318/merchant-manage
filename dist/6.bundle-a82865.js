webpackJsonp([6],{

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0cae3c75_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_report_vue__ = __webpack_require__(466);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(464)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0cae3c75"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_report_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0cae3c75_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_report_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\recommendation\\report.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0cae3c75", Component.options)
  } else {
    hotAPI.reload("data-v-0cae3c75", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  filters: {
    changeIsUp: function changeIsUp(value) {
      if (value == 1) {
        return "启用";
      } else {
        return "停用";
      }
    }
  },
  data: function data() {
    return {
      val2: [], //选择日期
      query: {},
      tableData: []
    };
  },

  methods: {
    /**@function 获取报告 */
    getGuideData: function getGuideData() {
      var _this = this;

      this.$http.get(this.$api.findStatistics, { params: this.query }).then(function (rsp) {
        console.log("获取报告");
        console.log(rsp);
        if (rsp.data.flag) {
          _this.tableData = [];
          _this.tableData.push(rsp.data.data);
        } else {
          _this.$message.error(rsp.data.msg);
        }
      });
    },


    /**@function 选择日期 */
    selectVal2: function selectVal2() {
      if (this.val2) {
        this.query.beginDate = this.val2[0];
        this.query.endDate = this.val2[1].substring(0, 10) + " 23:59:59";
      } else {
        this.query.beginDate = this.query.endDate = "";
      }
      this.getGuideData();
    }
  },
  created: function created() {
    this.getGuideData();
  }
};

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(465);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("104eb7de", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0cae3c75\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./report.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0cae3c75\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./report.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 465:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.btn-group[data-v-0cae3c75],\n.search-group[data-v-0cae3c75] {\n  margin-bottom: 20px;\n}\n.pagination[data-v-0cae3c75] {\n  float: right;\n}\n.up-btn[data-v-0cae3c75] {\n  position: relative;\n}\n.file-upload[data-v-0cae3c75] {\n  position: absolute;\n  background: red;\n  color: #fff;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  opacity: 0;\n  cursor: pointer;\n}\n", ""]);

// exports


/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "breadcrumb" },
      [
        _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐列表")]),
        _vm._v(" "),
        _c(
          "el-breadcrumb",
          _vm._l(_vm.$route.meta, function(item, i) {
            return _c("el-breadcrumb-item", { key: i }, [_vm._v(_vm._s(item))])
          })
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "content h500" },
      [
        _c(
          "div",
          { staticClass: "search-group" },
          [
            _c("el-date-picker", {
              staticClass: "mb10",
              attrs: {
                clearable: "",
                type: "daterange",
                "range-separator": "至",
                "start-placeholder": "开始日期",
                "end-placeholder": "结束日期",
                "value-format": "yyyy-MM-dd HH:mm:ss"
              },
              on: { change: _vm.selectVal2 },
              model: {
                value: _vm.val2,
                callback: function($$v) {
                  _vm.val2 = $$v
                },
                expression: "val2"
              }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            staticClass: "mb40",
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
          },
          [
            _c("el-table-column", {
              attrs: { prop: "channelGoodsCount", label: "总商品量" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "goodsSettleCount", label: "商品总结算量" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: { prop: "userSettleCount", label: "商品总结算用户量" }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "el-table",
          {
            staticStyle: { width: "100%" },
            attrs: { data: _vm.tableData, "tooltip-effect": "dark" }
          },
          [
            _c("el-table-column", {
              attrs: { prop: "totalDealAmount", label: "总销售额（元）" }
            }),
            _vm._v(" "),
            _c("el-table-column", {
              attrs: {
                prop: "totalCommissionAmount",
                label: "总产生佣金额（元）"
              }
            })
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0cae3c75", esExports)
  }
}

/***/ })

});