webpackJsonp([26],{

/***/ 250:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue__ = __webpack_require__(303);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_26bcde17_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addSort_vue__ = __webpack_require__(370);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(368)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-26bcde17"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_addSort_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_26bcde17_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_addSort_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\goods\\addSort.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-26bcde17", Component.options)
  } else {
    hotAPI.reload("data-v-26bcde17", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      isEdit: 0,
      shop_id: '',
      category_id: "",
      imageUrl: [],
      uploadApi: this.$uploadApi, //图片上传api
      upload_data: { file_type: 10 },
      dialogVisible: false,
      sortData: {
        name: "",
        index: "",
        image: ''
      },
      rules: {
        storeCategoryName: [{ required: true, message: "必填", trigger: "blur" }],
        sort: [{ required: true, message: "必填", trigger: "blur" }]
      }
    };
  },

  methods: {
    // 图片
    handleRemove: function handleRemove(file, fileList) {
      console.log(file, fileList);
    },
    handlePictureCardPreview: function handlePictureCardPreview(file) {
      console.log('handlePictureCardPreview');
      this.sortData.image = file.url;
      this.dialogVisible = true;
    },

    // 上传图片前校验
    beforeAvatarUpload: function beforeAvatarUpload() {},

    // 上传图片成功
    handleAvatarSuccess: function handleAvatarSuccess(res) {
      console.log('上传图片成功');
      console.log(res);
      this.sortData.image = this.$imgApi + res.data.file_md5;
    },
    change: function change() {
      //   todo
    },
    save: function save() {
      var _this = this;

      console.log(this.sortData);
      var postData = this.$qs.stringify({
        name: this.sortData.name,
        image: this.sortData.image,
        index: this.sortData.index,
        shop_id: this.shop_id,
        c_id: this.category_id
      });
      if (this.sortData.name && this.sortData.index && this.sortData.image) {
        if (this.isEdit) {
          this.$http.post(this.$api.editCategory, postData).then(function (rsp) {
            console.log("新增分类");
            console.log(rsp);
            if (rsp.data.message == 'OK') {
              _this.$message.success("成功");
              _this.$router.go(-1);
            }
          });
        } else {
          this.$http.post(this.$api.insertCategory, postData).then(function (rsp) {
            console.log("新增分类");
            console.log(rsp);
            if (rsp.data.message == 'OK') {
              _this.$message.success("成功");
              _this.$router.go(-1);
            }
          });
        }
      } else {
        this.$message.error('请完整填写信息');
        return false;
      }
    },
    cancal: function cancal() {
      this.$router.go(-1);
    }
  },
  created: function created() {
    var _this2 = this;

    console.log('shop_id');
    console.log(window.sessionStorage.getItem('shop_id'));
    var shop_id = window.sessionStorage.getItem('shop_id');
    this.shop_id = shop_id;
    if (this.$route.params.category_id) {
      this.isEdit = 1;
      this.category_id = this.$route.params.category_id;
      this.$http.get(this.$api.findCategory, {
        params: {
          shop_id: this.shop_id,
          category_id: this.category_id
        }
      }).then(function (rsp) {
        console.log('获取单个分类信息');
        console.log(rsp);
        var upImgObj = {};
        upImgObj.name = upImgObj.url = rsp.data.data.category_info.image;
        _this2.imageUrl.push(upImgObj);
        _this2.sortData = rsp.data.data.category_info;
        console.log(_this2.sortData);
      });
    }
  }
};

/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(369);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("514605a9", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-26bcde17\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addSort.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-26bcde17\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./addSort.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 369:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-26bcde17] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 370:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("推荐管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("新增分类")]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "sortData",
              attrs: {
                model: _vm.sortData,
                rules: _vm.rules,
                "label-width": "120px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "分类名称", prop: "name" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.sortData.name,
                              callback: function($$v) {
                                _vm.$set(_vm.sortData, "name", $$v)
                              },
                              expression: "sortData.name"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "店铺logo", prop: "real_name" } },
                        [
                          _c(
                            "el-upload",
                            {
                              attrs: {
                                action: _vm.uploadApi,
                                "list-type": "picture-card",
                                "file-list": _vm.imageUrl,
                                "show-file-list": "",
                                "on-preview": _vm.handlePictureCardPreview,
                                "on-remove": _vm.handleRemove,
                                "on-success": _vm.handleAvatarSuccess,
                                data: _vm.upload_data,
                                name: "upload_file",
                                "with-credentials": "",
                                limit: 1
                              }
                            },
                            [_c("i", { staticClass: "el-icon-plus" })]
                          ),
                          _vm._v(" "),
                          _c(
                            "el-dialog",
                            {
                              attrs: { visible: _vm.dialogVisible },
                              on: {
                                "update:visible": function($event) {
                                  _vm.dialogVisible = $event
                                }
                              }
                            },
                            [
                              _c("img", {
                                attrs: {
                                  width: "100%",
                                  src: _vm.sortData.image,
                                  alt: ""
                                }
                              })
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号", prop: "index" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.sortData.index,
                              callback: function($$v) {
                                _vm.$set(_vm.sortData, "index", $$v)
                              },
                              expression: "sortData.index"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "purple-bg", on: { click: _vm.save } }, [
        _vm._v("保存")
      ]),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.cancal } }, [
        _vm._v("取消")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-26bcde17", esExports)
  }
}

/***/ })

});