webpackJsonp([40],{

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue__);
/* harmony namespace reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_39079715_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddActivity_vue__ = __webpack_require__(433);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(431)
}
var normalizeComponent = __webpack_require__(8)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-39079715"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_AddActivity_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_39079715_hasScoped_true_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_AddActivity_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src\\component\\admin\\activity\\AddActivity.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-39079715", Component.options)
  } else {
    hotAPI.reload("data-v-39079715", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      shop_id: "",
      formData: {
        name: "",
        sort: "",
        image: ""
      },
      rules: {
        storeCategoryName: [{ required: true, message: "必填", trigger: "blur" }],
        sort: [{ required: true, message: "必填", trigger: "blur" }]
      }
    };
  },

  methods: {
    change: function change() {
      //   todo
    },
    save: function save(formName) {
      var _this = this;

      this.$refs[formName].validate(function (valid) {
        if (valid) {
          _this.$http.get(_this.$api.insertStoreCategory, {
            params: {
              storeCategoryName: _this.formData.storeCategoryName,
              sort: _this.formData.sort
            }
          }).then(function (rsp) {
            console.log("获取分类修改信息");
            console.log(rsp);
            _this.$message("成功");
            _this.$router.go(-1);
          });
        } else {
          _this.$message("添加失败");
          return false;
        }
      });
    },
    cancal: function cancal() {
      this.$router.go(-1);
    }
  },
  created: function created() {}
};

/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(432);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(9)("0abdc515", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-39079715\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddActivity.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-39079715\",\"scoped\":true,\"hasInlineConfig\":false}!../../../../node_modules/less-loader/dist/cjs.js!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddActivity.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 432:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(4)(false);
// imports


// module
exports.push([module.i, "\n.content .content-title[data-v-39079715] {\n  font-size: 18px;\n  font-family: MicrosoftYaHei-Bold;\n  color: #606266;\n  font-weight: bold;\n  margin: 0 10px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "breadcrumb" },
        [
          _c("div", { staticClass: "breadcrumb-head" }, [_vm._v("活动管理")]),
          _vm._v(" "),
          _c(
            "el-breadcrumb",
            _vm._l(_vm.$route.meta, function(item, i) {
              return _c("el-breadcrumb-item", { key: i }, [
                _vm._v(_vm._s(item))
              ])
            })
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content content1" },
        [
          _c("div", { staticClass: "content-title" }, [_vm._v("新增活动")]),
          _vm._v(" "),
          _c(
            "el-form",
            {
              ref: "formData",
              attrs: {
                model: _vm.formData,
                rules: _vm.rules,
                "label-width": "120px"
              }
            },
            [
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _c(
                        "el-form-item",
                        {
                          attrs: {
                            label: "活动名称",
                            prop: "storeCategoryName"
                          }
                        },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.formData.storeCategoryName,
                              callback: function($$v) {
                                _vm.$set(_vm.formData, "storeCategoryName", $$v)
                              },
                              expression: "formData.storeCategoryName"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                {
                  staticClass: "addcarousel-pic-input",
                  staticStyle: { height: "200px" }
                },
                [
                  _c(
                    "el-col",
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "活动图片" } },
                        [
                          _c(
                            "el-upload",
                            {
                              attrs: {
                                action: _vm.imgApi,
                                "list-type": "picture-card",
                                "before-upload": _vm.beforeAvatarUpload,
                                "on-success": _vm.uploadImgHandler,
                                "file-list": _vm.upImgs,
                                "on-remove": _vm.handleRemove,
                                limit: 1
                              }
                            },
                            [
                              _c("i", { staticClass: "el-icon-plus" }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "el-upload__tip orange-txt",
                                  attrs: { slot: "tip" },
                                  slot: "tip"
                                },
                                [
                                  _vm._v(
                                    "**图片尺寸：750*320px；格式:jpg、png；大小：200KB以内"
                                  )
                                ]
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { span: 6 } },
                    [
                      _c(
                        "el-form-item",
                        { attrs: { label: "排序号", prop: "sort" } },
                        [
                          _c("el-input", {
                            model: {
                              value: _vm.formData.sort,
                              callback: function($$v) {
                                _vm.$set(_vm.formData, "sort", $$v)
                              },
                              expression: "formData.sort"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-button",
        {
          staticClass: "purple-bg",
          on: {
            click: function($event) {
              _vm.save("formData")
            }
          }
        },
        [_vm._v("保存")]
      ),
      _vm._v(" "),
      _c("el-button", { staticClass: "orange-bg", on: { click: _vm.cancal } }, [
        _vm._v("取消")
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-39079715", esExports)
  }
}

/***/ })

});