﻿// 导入第三方库
import Vue from 'vue';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'normalize.css';
import './less/index.less';
import qs from 'qs'


//导入富文本编辑器
// import '../static/UE/ueditor.config.js'
// import '../static/UE/ueditor.all.js'
// import '../static/UE/lang/zh-cn/zh-cn.js'
// import '../static/UE/ueditor.parse.js'


// 启用库
Vue.use(VueRouter);
Vue.use(ElementUI);

import AppComponent from './component/App.vue';

// 导入路由配置
import RouterConfig from './router/index.js';

// 3. 导入配置后的axios与api, 注入到Vue的原型当中, 使可以通过this调用
import axios from './js/axios_config.js';
import api, {
    domain
} from './js/api_config.js';
Vue.prototype.$http = axios;
Vue.prototype.$api = api;
Vue.prototype.$qs = qs;

// Vue.prototype.$imgApi = "http://upload.weyou.shop/oss/txyun/uploadsImages";//图片上传api  =============== 正式
Vue.prototype.$uploadApi = "https://www.flyphant.cn/mk_mall/file/upload/";//图片上传api ============  测试
Vue.prototype.$imgApi='http://okmqf080n.bkt.clouddn.com/';
Vue.prototype.$apiDomain = domain;
Vue.prototype.$shop_id=""
// 导入路由守卫函数, 创建路由实例, 配置守卫做登陆校验

// import routerGuard from './router/guard.js';
const vueRouter = new VueRouter(RouterConfig);
// vueRouter.beforeEach(routerGuard);

new Vue({
    el: '#app',
    render: c => c(AppComponent),
    router: vueRouter
});


/**@function 判断登录超时处理函数 */
Vue.prototype.$isot = function isOvertime(rsp) {
    // console.log(`判断登录超时处理函数,目前登录状态:`+rsp.data.msg)
    // if (rsp.data.stateCode == "E100011") {
    //     this.$alert('登录超时，请重新登录！', '提示：', {
    //         confirmButtonText: '确定',
    //         callback: action => {
    //             this.$router.push('/login')
    //         }
    //     });
    //     return false;
    // }
};
