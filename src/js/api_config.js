﻿// 接口域名
// export const domain = 'http://merchant.weyou.shop/api';//============正式
export const domain = 'https://www.flyphant.cn/mk_mall'; //=============测试

// 导出接口api
export default {
  // 账号管理
  login: `/admin/login/`, // 登陆
  register: '/admin/reg_user/',
  registerStore: '/shop/apply_shop/',	// 注册店铺
  logout: `/admin/userlogin/logout`, // 登出
  findUserInfo: `/admin/user_info/`,  // 获取用户信息

  //商户资料
  findStoreInfo: `/admin/storeInfo/findStoreInfo`, //商户基本资料详情
  updateStoreFreight: `/admin/storeInfo/updateStoreFreight`, //修改商户基础运费及包邮  POST

  // 分类管理
  findCategoryListPage: '/category/show_category/', // 分类管理 - 分类列表(分页)
  insertCategory: '/category/add_category/', //分类新增
  findCategory: '/category/get_category_info/', //获取分类信息
  editCategory: '/category/edit_category/', //分类编辑
  deleteCategory: '/category/del_category/', // 删除分类
  getCategoryGoods:'/category/get_category_goods/', //通过分类ID获取绑定商品
  bindCategoryGoods: '/category/category_bound_goods/', //绑定分类商品

  // 活动管理
  findActivityList: '/activity/get_activity_list/',
  insertActivity: '/activity/add_activity/', // 新增活动
  findActivityInfo:'/activity/get_activity_info/',// 获取单个活动信息
  editActivity: '/activity/edit_activity/',
  deleteActivity: '/activity/del_activity/',

  //职员管理
  findStoreEmployeeListPage: `/admin/storeEmployee/findStoreEmployeeListPage`, //职员管理列表
  findStoreEmployee: `/admin/storeEmployee/findStoreEmployee`, //查看职员信息
  updateStoreEmployeeStatus: `/admin/storeEmployee/updateStoreEmployeeStatus`, //启用及禁用员工
  delStoreEmployee: `/admin/storeEmployee/delStoreEmployee`, //删除员工
  addStoreEmployee: `/admin/storeEmployee/addStoreEmployee`, //添加商户职员

  // 物流管理
  findStoreLogisticsListPage: `/admin/storeLogistics/findStoreLogisticsListPage`, //商户物流列表
  addStoreLogistics: `/admin/storeLogistics/addStoreLogistics`, //商户启用物流
  delStoreLogistics: `/admin/storeLogistics/delStoreLogistics`, //商户删除物流信息
  findStoreLogisticsList: `/admin/storeLogistics/findStoreLogisticsList`, //商户物流列表(不分页)


  // 轮播图管理
  findAdImageListPage: `/slide_show/get_slide_show/`, //商户轮播图列表
  findAdImage: `/slide_show/get_slide_info/`, //获取轮播图详情
  addAdImage: `/slide_show/add_slide_show/`,  //新增轮播图
  delAdImage:`/slide_show/del_slide_show/`, // 删除轮播图

  // 商品列表
  findGoodsListPage: `/goods/goods_list/`, //商户商品列表
  findGoods: `/goods/get_goods_msg/`, //查询商户商品详情
  addGoods: `/goods/add_goods/`, //添加商品
  updateGoods: `/admin/goods/updateGoods`, // 商户编辑商品
  updateGoodsIsUp: `/admin/goods/updateGoodsIsUp`, // 商户编辑商品

  //订单列表
  findOrdersListPage: `/ordering/ordering_list/`, // 商户订单列表
  findOrders: `/admin/orders/findOrders`, // 商户订单详情
  exportOrderList: `/admin/orders/exportOrderList`, // 商户订单导出
  deliverGoods: `/admin/orders/deliverGoods`, // 订单发货

  // 商品管理

  findGoodsBrandList: `/admin/goodsBrand/findGoodsBrandList`, // 商品品牌列表(不分页)
  delGoods: `/admin/goods/delGoods`, // 删除商品
  findGoodsCategoryList: `/admin/goodsCategory/findGoodsCategoryList`, // 商品营销分类列表(不分页)
  findGoodsAttributeCategoryList: `/admin/goodsAttributeCategory/findGoodsAttributeCategoryList`, //商品属性列表(不分页)
  findActivityGoodsListPage: `/admin/storespecialsalesgoods/findActivityGoodsListPage`, // 特卖活动商品列表
  findActivityGoodsDetails: `/admin/storespecialsalesgoods/findActivityGoodsDetails`, // 特卖活动商品详情
  findActivityDetails: `/admin/activity/findActivityDetails`, //立即报名
  findActivitySpecialSalesGoodsListPage: `/admin/activity/findActivitySpecialSalesGoodsListPage`, // 特卖商品列表（立即报名）
  insertActivitySpecialSales: `/admin/storespecialsalesgoods/insertActivitySpecialSales`, // 添加特卖商品
  deleteActivitySpecialSales: `/admin/storespecialsalesgoods/deleteActivitySpecialSales`, // 特卖商品删除
  findGoodsByGoodsCode: `/admin/goods/findGoodsByGoodsCode`, //根据商品编号获取商品ID
  findStoreActivityGoodsPageList: '/admin/storespecialsalesgoods/findStoreActivityGoodsPageList', //特卖添加商品列表(新)

  // 优惠券管理
  findCouponList: `/coupon/show_coupon/`,
  addCoupon:  `/coupon/add_coupon/`,



  //推荐管理
  findChannelRecordList: `/admin/channelRecord/findChannelRecordList`, //推荐列表
  findChannelRecordDto: `/admin/channelRecord/findChannelRecordDto`, //推荐列表 查看1
  findChannelUserRelationByChannelRecordId: `/admin/channelUserRelation/findChannelUserRelationByChannelRecordId`, //推荐列表 查看2
  findChannelRecordOrdersRelationList: `/admin/channelRecordOrdersRelation/findChannelRecordOrdersRelationList`, //推荐列表 查看3
  updateChannelGoods: `/admin/channelGoods/updateChannelGoods`, //编辑渠道分享商品
  addChannelGoods: `/admin/channelGoods/addChannelGoods`, //新增渠道分享商品
  findChannelGoods: `/admin/channelGoods/findChannelGoods`, //渠道分享商品详情
  findChannelGoodsListPage: `/admin/channelGoods/findChannelGoodsListPage`, //推荐商品列表
  delChannelGoods: `/admin/channelGoods/delChannelGoods`, //删除渠道商品
  updateChannelGoodsIsUp: `/admin/channelGoods/updateChannelGoodsIsUp`, //修改渠道商品上下线状态(可批量上下线)
  findChannelCommissionRateConfList: `/admin/channelCommissionRateConf/findChannelCommissionRateConfList`, //获取赏金等级
  findAllChannelOrganization: `/admin/channelOrganization/findAllChannelOrganization`, //获取机构
  findStatistics: `/admin/channelStatistics/findStatistics`, //统计报表

  findChannelGoodsDocumentListPage: `/admin/channelGoodsDocument/findChannelGoodsDocumentListPage`, //渠道商品文案列表
  findChannelGoodsDocumente: `/admin/channelGoodsDocument/findChannelGoodsDocumente`, //渠道商品文案详情
  addChannelGoodsDocument: `/admin/channelGoodsDocument/addChannelGoodsDocument`, //添加渠道商品文案详情
  updateChannelGoodsDocument: `/admin/channelGoodsDocument/updateChannelGoodsDocument`, //编辑渠道商品文案详情
  updateChannelGoodsDocumentSort: `/admin/channelGoodsDocument/updateChannelGoodsDocumentSort`, //编辑渠道商品文案排序号
  findChannelOrderDetailsList: 'admin/channelRecord/findChannelOrderDetailsList' //购买信息

}
