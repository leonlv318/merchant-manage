export default [
  // 导入商品模块相关路由配置
  {
    name: 'gg',
    path: 'goods/goods',
    component: resolve => require(['../component/admin/goods/Goods.vue'], resolve),
    meta: ['商品列表']
  },
  {
    name: 'ga',
    path: 'goods/addgoods',
    component: resolve => require(['../component/admin/goods/AddGoods.vue'], resolve),
    meta: ['商品列表', '新增商品']
  },
  {
    name: 'ge',
    path: 'goods/editgoods',
    component: resolve => require(['../component/admin/goods/EditGoods.vue'], resolve),
    meta: ['商品列表', '编辑商品']
  },
  {
    name: 'gc',
    path: 'goods/checkgoods',
    component: resolve => require(['../component/admin/goods/CheckGoods.vue'], resolve),
    meta: ['商品列表', '查看商品']
  },
  {
    name: 'am',
    path: 'assort/manage',
    component: resolve => require(['../component/admin/goods/manageOfAssort.vue'], resolve),
    meta: ['分类管理']
  },
  {
    name: 'aa',
    path: 'assort/addAssort',
    component: resolve => require(['../component/admin/goods/addSort.vue'], resolve),
    meta: ['分类管理', '新增分类']
  },
  {
    name: 'ama',
    path: 'assort/modifyAssort',
    component: resolve => require(['../component/admin/goods/modifyAsort.vue'], resolve),
    meta: ['分类管理', '修改分类']
  },
  {
    name: 'ag',
    path: 'assort/goods',
    component: resolve => require(['../component/admin/goods/goodsOfSort.vue'], resolve),
    meta: ['分类商品']
  },
  {
    name: 'aag',
    path: 'assort/addAssortGoods',
    component: resolve => require(['../component/admin/goods/addSortGoods.vue'], resolve),
    meta: ['分类商品', '添加商品']
  },
  {
    name: 'agd',
    path: 'assort/asortGoodsDetail',
    component: resolve => require(['../component/admin/goods/GoodsDetail.vue'], resolve),
    meta: ['分类商品', '商品详情']
  },
  {
    name: 'aagd',
    path: 'assort/asortGoodsDetail',
    component: resolve => require(['../component/admin/goods/asortGoodsDetail.vue'], resolve),
    meta: ['分类商品', '新增商品详情']
  },

  // 导入订单模块相关路由配置
  {
    name: 'ot',
    path: 'oders/total',
    component: resolve => require(['../component/admin/orders/Total.vue'], resolve),
    meta: ['全部订单']
  },
  {
    name: 'otbs',
    path: 'oders/tobeshipped',
    component: resolve => require(['../component/admin/orders/ToBeShipped.vue'], resolve),
    meta: ['待发货订单']
  },
  {
    name: 'otbr',
    path: 'oders/tobereceived',
    component: resolve => require(['../component/admin/orders/ToBeReceived.vue'], resolve),
    meta: ['待收货订单']
  },
  {
    name: 'ocp',
    path: 'oders/completed',
    component: resolve => require(['../component/admin/orders/CompletedOrder.vue'], resolve),
    meta: ['已完成订单']
  },
  {
    name: 'occ',
    path: 'oders/cancel',
    component: resolve => require(['../component/admin/orders/CancelOrder.vue'], resolve),
    meta: ['取消订单']
  },
  {
    name: 'oo',
    path: 'oders/orderdetail',
    component: resolve => require(['../component/admin/orders/OrderDetail.vue'], resolve),
    meta: ['订单详情']
  },

  // 导入轮播图模块相关路由配置
  {
    name: 'cc',
    path: 'carousel/carousel',
    component: resolve => require(['../component/admin/carousel/Carousel.vue'], resolve),
    meta: ['轮播图管理']
  },
  {
    name: 'ca',
    path: 'carousel/addcarousel',
    component: resolve => require(['../component/admin/carousel/AddCarousel.vue'], resolve),
    meta: ['轮播图管理', '新增轮播图']
  },
  {
    name: 'cck',
    path: 'carousel/checkcarousel',
    component: resolve => require(['../component/admin/carousel/CheckCarousel.vue'], resolve),
    meta: ['轮播图管理', '查看轮播图']
  },
  {
    name: 'cmad',
    path: 'carousel/multipleAddCarouselGoods',
    component: resolve => require(['../component/admin/carousel/multipleAddCarouselGoods.vue'], resolve),
    meta: ['轮播图管理', '轮播图添加商品']
  },
  {
    name: 'cam',
    path: 'carousel/indexadimg',
    component: resolve => require(['../component/admin/carousel/indexAdImg.vue'], resolve),
    meta: ['横幅广告管理']
  },
  {
    name: 'caai',
    path: 'carousel/addindexadimg',
    component: resolve => require(['../component/admin/carousel/AddIndexAdImg.vue'], resolve),
    meta: ['横幅广告管理', '新增横幅广告']
  },
  {
    name: 'ccia',
    path: 'carousel/checkindexadimg',
    component: resolve => require(['../component/admin/carousel/CheckIndexAdImg.vue'], resolve),
    meta: ['横幅广告管理', '查看横幅广告']
  },
  // 导入活动模块相关路由配置
  {
    name: 'ama',
    path: 'activity/myactivity',
    component: resolve => require(['../component/admin/activity/MyActivity.vue'], resolve),
    meta: ['活动列表']
  },
  {
    name: 'as',
    path: 'activity/activitygoods',
    component: resolve => require(['../component/admin/activity/ActivityGoods.vue'], resolve),
    meta: ['活动商品']
  },
  {
    name: 'ac',
    path: 'activity/addactivity',
    component: resolve => require(['../component/admin/activity/AddActivity.vue'], resolve),
    meta: ['活动标题', '新增活动标题']
  },
  {
    name: 'ae',
    path: 'activity/addactivitygoods',
    component: resolve => require(['../component/admin/activity/AddActivityGoods.vue'], resolve),
    meta: ['活动商品', '新增活动商品']
  },
  {
    name: 'aea',
    path: 'activity/editactivity',
    component: resolve => require(['../component/admin/activity/EditActivity.vue'], resolve),
    meta: ['活动标题', '编辑活动标题']
  },

  // 导入商户模块相关路由配置
  {
    name: 'sm',
    path: 'shop/merchant',
    component: resolve => require(['../component/admin/shop/Merchant.vue'], resolve),
    meta: ['商户设置', '基本信息']
  },
  {
    name: 'ss',
    path: 'shop/staff',
    component: resolve => require(['../component/admin/shop/Staff.vue'], resolve),
    meta: ['商户设置', '职员管理']
  },
  {
    name: 'sa',
    path: 'shop/addstaff',
    component: resolve => require(['../component/admin/shop/AddStaff.vue'], resolve),
    meta: ['职员管理', '新增职员']
  },
  {
    name: 'sl',
    path: 'shop/logistics',
    component: resolve => require(['../component/admin/shop/Logistics.vue'], resolve),
    meta: ['商户设置', '物流管理']
  },


  // 导入 推荐管理 模块相关路由配置
  // {
  //   name: 'rr',
  //   path: 'recommendation/recommendation',
  //   component: resolve => require(['../component/admin/recommendation/recommendation.vue'], resolve),
  //   meta: ['推荐管理', '购买数据']
  // },
  // {
  //   name: 'rb',
  //   path: 'recommendation/buyInformation',
  //   component: resolve => require(['../component/admin/recommendation/buyInformation.vue'], resolve),
  //   meta: ['推荐管理', '购买信息']
  // },
  // {
  //   name: 'rrd',
  //   path: 'recommendation/recommendationDetail',
  //   component: resolve => require(['../component/admin/recommendation/recommendationDetail.vue'], resolve),
  //   meta: ['购买数据', '推荐详情']
  // },
  // {
  //   name: 'rg',
  //   path: 'recommendation/goods',
  //   component: resolve => require(['../component/admin/recommendation/goods.vue'], resolve),
  //   meta: ['推荐管理', '商品管理']
  // },
  // {
  //   name: 'rrp',
  //   path: 'recommendation/report',
  //   component: resolve => require(['../component/admin/recommendation/report.vue'], resolve),
  //   meta: ['推荐管理', '数据报表']
  // },
  // {
  //   name: 'rc',
  //   path: 'recommendation/checkGoods',
  //   component: resolve => require(['../component/admin/recommendation/checkGoods.vue'], resolve),
  //   meta: ['推荐管理', '查看商品']
  // },
  // {
  //   name: 'raa',
  //   path: 'recommendation/addGoods/addGoods',
  //   component: resolve => require(['../component/admin/recommendation/addGoods.vue'], resolve),
  //   meta: ['推荐管理', '新增商品', '新增商品管理']
  // },
  // {
  //   name: 'rae',
  //   path: 'recommendation/addGoods/editGoods',
  //   component: resolve => require(['../component/admin/recommendation/editGoodsOfAdd.vue'], resolve),
  //   meta: ['推荐管理', '新增商品', '编辑商品管理']
  // },
  // {
  //   name: 'ra',
  //   path: 'recommendation/addGoods',
  //   component: resolve => require(['../component/admin/recommendation/addGoodsOfAdd.vue'], resolve),
  //   meta: ['推荐管理', '添加商品']
  // },


  // 导入 优惠券管理 模块相关路由配置
  {
    name: 'ccou',
    path: 'coupon/couponList',
    component: resolve => require(['../component/admin/coupon/MyCoupon.vue'], resolve),
    meta: ['优惠券管理', '优惠券列表']
  },
  {
    name: 'cacou',
    path: 'coupon/AddCoupon',
    component: resolve => require(['../component/admin/coupon/AddCoupon.vue'], resolve),
    meta: ['优惠券管理', '添加优惠券']
  },
];
