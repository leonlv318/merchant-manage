import AdminComponet from '../component/admin/Admin.vue';
import AdminAsideComponet from '../component//admin/frame/Aside.vue';
import AdminHeaderComponet from '../component/admin/frame/Header.vue';


import LoginComponent from '../component/login/Login.vue';
import RegisterComponent from '../component/login/register.vue'
import RegisterStore from '../component/login/registerstore.vue'

// 导入商品路由配置
import RouterConfig from './route_config.js';
let adminChildren = [...RouterConfig];


export default {


    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        // 登录
        {
            name: "l",
            path: "/login",
            component: LoginComponent
        },
        //  注册
        {
            name: "2",
            path: "/register",
            component: RegisterComponent
        },
        // 注册店铺模块
        {       
            name: '3',
            path: '/registerstore',
            component: RegisterStore,
        },
        // 后台主页
        {
            name: "a",  
            path: "/",  
            component: AdminComponet,
            children: adminChildren
        },



    ]
}