const path = require('path');
const htmlWP = require('html-webpack-plugin'); // html插件

const uglify = require('uglifyjs-webpack-plugin'); //压缩

module.exports = {

    // 入口
    entry: path.resolve(__dirname, './src/main.js'),

    // 输出
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'bundle-[chunkhash:6].js'
        //    filename: 'bundle.js'
    },

    plugins: [

        // 自动把打包js注入到html
        new htmlWP({
            template: path.resolve(__dirname, './src/index.html'),
            filename: 'index.html'
        }),
        //压缩  打包时开启  调试时需要关闭
        // new uglify({
        //     compress: {
        //         warnings: false,
        //         drop_debugger: true,
        //         drop_console: true
        //     }
        // })
    ],

    module: {

        // js模块的处理规则
        rules: [

            // css模块
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },

            // less模块
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader']
            },
            // static模块
            {
                test: /\static/,
                use: ['style-loader', 'css-loader', 'html-loader']
            },

            // 静态资源引入模块
            {
                test: /\.(gif|png|jpg|svg|mp3|mp4|avi|ttf|woff)/,
                use: [
                    // 小于10KB的才打包
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10240
                        }
                    }
                ]
            },

            // js模块
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },

            // vue模块
            {
                test: /\.vue$/,
                use: ['vue-loader']
            }

        ]
    },

    // webpack-dev-server的配置
    devServer: {
        open: true,
        port: 8090, // 服务端口
        contentBase: 'dist'
    }

}
